#pragma once

#include "na64dp/processingInfo.hh"

#ifdef ZMQ_FOUND

#include <zmq.hpp>

namespace na64dp {

class NwPubEventProcessingInfo : public EvProcInfoDispatcher {
private:
    zmq::context_t _zCtx;
    zmq::socket_t _zPubSock;
    std::vector<char> _msgBuf;
protected:
    virtual void _update_event_processing_info() override;
public:
    NwPubEventProcessingInfo( int portNo
                            , size_t nMaxEventsm
                            , const HitBanks & banks
                            , unsigned int refreshInterval=200
                            );
};

}  // namespace na64dp

# endif

