#pragma once

#include "na64event/hitInserter.hh"
#include "na64dp/processingInfo.hh"
#include "na64calib/manager.hh"
#include "na64util/YAMLAssertions.hh"
#include "na64util/vctr.hh"

#include <map>
#include <yaml-cpp/yaml.h>

namespace na64dp {

///\brief An interface for event source entities (files, sockets, etc).
class AbstractEventSource {
protected:
    /// Sources logger category
    log4cpp::Category & _log;
    /// Reference to memory allocators used to create new hits
    HitBanks & _banks;
    HitsInserter<SADCHit>    _inserterSADC;    ///< Inserter helper for SADC hits
    HitsInserter<APVHit>     _inserterAPV;     ///< Inserter helper for APV hits
    HitsInserter<StwTDCHit>  _inserterStwTDC;  ///< Inserter helper for APV hits
    // ... other inserters
    /// Online event processing stats monitor handle
    iEvProcInfo * _epi;
public:
    ///\brief Main ctr, expects a banks reference to be bound with
    AbstractEventSource(HitBanks & banks, iEvProcInfo * epi=nullptr);
    /// Shall read event from file, socket, whatever into reentrant buffer.
    /// Shall not clear event, but rather rely on event has been already
    /// re-set. Must return whether the event iteration shall be proceed.
    virtual bool read( Event & ) = 0;
    virtual ~AbstractEventSource() {}
    /// Returns logger instance affiliated to sources objects.
    log4cpp::Category & log() { return _log; }

    template<typename HitT> friend class EvFieldTraits;
};

template<> struct CtrTraits<AbstractEventSource> {
    typedef AbstractEventSource * (*Constructor)
        (HitBanks &, calib::Manager &, const YAML::Node &, const std::vector<std::string> & );
};

}  // namespace na64dp

/**\brief Registers new event source type for pipeline data processing
 * \ingroup vctr-defs
 *
 * Registers subclasses of `AbstractEventSource` base to be created with `VCtr`.
 *
 * \param clsName The name of the handler type
 * \param banks The name for "banks" argument to be used in ctr code
 * \param calibMgr The name of calibations manager instance to be used in ctr
 * \param ni The name of the config (YAML) node to be used in the ctr
 * \param ids The name for list of input source IDs to be used in ctr
 * \param desc A source type description string
 */
# define REGISTER_SOURCE( clsName, banks, calibMgr, ni, ids, desc )             \
static na64dp::AbstractEventSource * _new_ ## clsName ## _instance( na64dp::HitBanks & \
                                                          , na64dp::calib::Manager & \
                                                          , const YAML::Node &  \
                                                          , const std::vector<std::string> & );  \
static bool _regResult_ ## clsName =                                                \
    ::na64dp::VCtr::self().register_class<::na64dp::AbstractEventSource>( # clsName, _new_ ## clsName ## _instance, desc ); \
                                                                                    \
static na64dp::AbstractEventSource * _new_ ## clsName ## _instance( na64dp::HitBanks & banks \
                                                          , na64dp::calib::Manager & calibMgr \
                                                          , const YAML::Node & ni \
                                                          , const std::vector<std::string> & ids )

