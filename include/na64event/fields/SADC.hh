#pragma once

#include "na64util/numerical/SADCWaveFormFit.h"
#include "na64event/hitsMap.hh"
#include "na64event/hitTraits.hh"
#include "na64event/pools.hh"

#if defined(CapnProto_FOUND) && CapnProto_FOUND
#include "event.capnp.h"
#endif

namespace na64dp {

struct Event;  // fwd
namespace serialization {
    struct StatePacking;  // fwd
    struct StateUnpacking;  // fwd
}

/// Keeps the fitting results of SADC waveform.
struct WfFittingData {
    enum FittingType {
        moyal = 0x1,
        // ...
    } fitType;
    SADCWF_FittingFunctionParameters fitPars;
    // other fitting parameters ...

    /// $\chi^2$ of the fitting
    double chisq;
    /// RMS of the fitting
    double rms;
};

/// Represents an activated (triggered) SADC.
struct SADCHit {
    /// Waveform samples array
    double wave[32];
    /// Sum of the waveform samples array
    double sum;
    /// Energy deposition calculated wrt the waveform
    double eDep;
    /// Reconstructed time of hit
    double time;
    /// Time sigma
    double timeError;
    /// Waveform maximum position and value
    double maxSample
         , maxValue;
    /// Map of all local maxima in waveform
    std::map<int, double> maxima;
    /// Is the current cell is maximal in this calorimeter
    bool maxCell;
    /// Pedestals subtracted from the waveform
    double pedestals[2];
    PoolRef<WfFittingData> fittingData;
    /// Time hit getting to using rising edge
    double timeBegin;
    /// The angle of inclination of the start of the hit 
    double angle;
    double countPipeUp;
};

/**\brief Hit routines specification for SADC hit type
 *
 * This structure provides routines and definitions for SADC hits object.
 *
 * Uninitialized state implies that every double-typed field of the hit
 * structure will be set to NaN("0") value.
 *
 * List of getters:
 *  - `"sum"` corresponds to `SADCHit::sum`
 *  - `"eDep"` corresponds to `SADCHit::eDep`
 *  - `"maxSample"` corresponds to `SADCHit::maxSample`
 *  - `"maxValue"` corresponds to `SADCHit::maxValue`
 *  - `"pedestalOdd"` and `"pedestalEven"` corresponds to `SADCHit::pedestals`,
 *  0 and 1 correspondingly
 *  - `fitRMS` corresponds to SADCHit::fittingData::rms
 *  - `fitChiSq` corresponds to SADCHit::fittingData::chisq
 *
 * Getter that rely on fitting data will return "NaN(0)" value if there is no
 * fitting data associated with hit.
 */
template<>
struct EvFieldTraits<SADCHit> {
    typedef HitsMap<SADCHit> MapType;
    /// Returns reference to a SADC hits map from Event object
    static MapType & map( Event & e );
    /// Returns certain bank for the type
    static ObjPool<SADCHit> & bank( HitBanks & bs ) { return bs.bankSADC; }
    /// Returns a list of hits maps specific for certain SADC detector id
    static std::vector<MapType*>
        maps_for( Event & e, DetID did, const nameutils::DetectorNaming & );
    /// Erases hit entry from all the maps within an event
    static void remove_from_event( Event &
                                 , MapType::Iterator
                                 , const nameutils::DetectorNaming & );
    /// Returns an inserter instance from abstract source object, repsonsible
    /// for insertion hits of SADC type
    static HitsInserter<SADCHit> &
        inserter( AbstractEventSource & src );
    #if 0
    /// Appends substitution dictionary with various kin-specific information
    /// (cell identifier, etc)
    static void
        append_subst_dict_for( DetID did
                             , std::map<std::string, std::string> &
                             , const nameutils::DetectorNaming & );
    #endif
    /// Drops the hit to "uninitialized" state
    static void reset_hit( SADCHit & );

    /// Returns unique detector ID without variable payload; for APV detectors
    /// it means getting rid of wire number
    static DetID_t uniq_detector_id(DetID);

    /// A type of getter callback used to dynamically parameterize various
    /// histogramming objects
    typedef double (*ValueGetter)(const SADCHit &);

    static struct GetterEntry {
        ValueGetter getter;
        const char * name;
        const char * description;
    } * getters;

    #if defined(CapnProto_FOUND) && CapnProto_FOUND
    typedef capnp::na64dp::SADCHit PackedType;
    static void pack( const SADCHit &, PackedType::Builder, serialization::StatePacking & );
    static void unpack( PackedType::Reader, SADCHit &, serialization::StateUnpacking & );
    #endif
};

#if 0
struct SADCCalibration {
    double meanPedestals[2];
    // ...
};

template<>
struct CalibrationTraits<SADCCalibration> {
    const SADCCalibration & get( iCalibHandle &, DetectorID did ) const;
};
#endif

//TODO: move it somewhere?
template<>
struct EvFieldTraits<WfFittingData> {
    /// Returns certain bank for the type
    static ObjPool<WfFittingData> & bank( HitBanks & bs ) { return bs.bankSADCFitting; }
};

}

