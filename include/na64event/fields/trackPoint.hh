#pragma once

#include <memory>

#include "na64event/fields/APVCluster.hh"

#if defined(CapnProto_FOUND) && CapnProto_FOUND
#include "event.capnp.h"
#endif

#include <TrackPoint.h>

namespace genfit {  // fwd decls
class TrackPoint;
}

namespace na64dp {

namespace serialization {
    struct StatePacking;  // fwd
    struct StateUnpacking;  // fwd
}

/**\brief Particle track point representation
 *
 * TODO: this structure's content will vary to fit the conceptions of track and
 * tracking point in GenFit.
 **/
struct TrackPoint {
    /// Local track point coordinates: x, y, z. Typically only x,y are used, z
    /// is reserved for (rare) case when track point has 3rd coordinate.
    ///\todo set to NaN on reset
    double lR[3];
    /// Global track point coordinates: x, y, z.
    /// \todo set to NaN on reset
    double gR[3];
    /// A list of clusters references that defined this track point
    std::set< std::pair<DetID_t, PoolRef<APVCluster> > > clusterRefs;
    /// For track points related to a single statio (of possibly multiple
    /// planes) this will be set to common station ID.
    DetID_t station;
    /// Charge of conjugated cluster
    double charge;
    /// Ptr to the genfit track associated. Available only if GenFit was enabled
    genfit::TrackPoint genfitTrackPoint;
    
    //TrackPoint();
    //TrackPoint(const TrackPoint &) = delete;
    //~TrackPoint();
};

typedef ObjMap<DetID_t, TrackPoint, aux::Multimap> TrackPointIndex;

template<>
struct EvFieldTraits<TrackPoint> {
    typedef TrackPointIndex MapType;
    /// Returns reference to a APV hits map from Event object
    static MapType & map( Event & e );
    // Returns reference to the main track points container within an event
    // TrackPoints & map(Event & e);
    /// Returns certain bank for the type
    static ObjPool<TrackPoint> & bank( HitBanks & bs ) { return bs.bankTrackPoints; }
    /// Returns a list of maps specific for certain APV detector station id
    static std::vector<MapType*>
        maps_for( Event & e, DetID did, const nameutils::DetectorNaming & );
    /// Erases hit entry from all the maps within an event
    static void remove_from_event( Event & e
                                 , MapType::Iterator
                                 , const nameutils::DetectorNaming & );

    static void reset_hit( TrackPoint & hit );

    typedef double (*ValueGetter)(const TrackPoint &);
    static struct GetterEntry {
        ValueGetter getter;
        const char * name;
        const char * description;
    } * getters;

    #if defined(CapnProto_FOUND) && CapnProto_FOUND
    typedef capnp::na64dp::TrackPoint PackedType;
    static void pack( const TrackPoint &
                    , PackedType::Builder
                    , serialization::StatePacking & );
    static void unpack( PackedType::Reader
                      , TrackPoint &
                      , serialization::StateUnpacking & );
    #endif
};

}

