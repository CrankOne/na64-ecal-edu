#pragma once

#include "na64event/hitTraits.hh"
#include "na64event/hitsMap.hh"
#include "na64util/mm-layout.h"  // for NA64DP_DemultiplexingMapping
#include "na64event/pools.hh"

#if defined(CapnProto_FOUND) && CapnProto_FOUND
#include "event.capnp.h"
#endif

namespace na64dp {

// define namespaced type aliases
typedef NA64DP_APVStripNo_t APVStripNo_t;
typedef NA64DP_APVWireNo_t  APVWireNo_t;
typedef NA64DP_APVNWires_t  APVNWires_t;

struct Event;  // fwd
namespace serialization {
    struct StatePacking;  // fwd
    struct StateUnpacking;  // fwd
}


/// Keeps APV raw data
struct APVRawData {
    /// Triggered wire number
    APVWireNo_t wireNo;
    /// Measured amplitude samples on a waire
    uint32_t samples[3];
};

/// Raw data of single APV hit
struct APVHit {
    /// Hit raw data (DDD digit)
    APVRawData rawData;
    /// Physical wire number (topological order)
    /// \todo remove?
    APVStripNo_t channelNo;
    /// Hit max charge
    double maxCharge;
    /// Hit average time
    double averageT;
    // TEMP
    double timeRise;
    /// Amplitude ratios
    double a02, a12;
    /// Time ratios of hit
    double t02, t12;
    /// Error of time ratio
    double t02sigma, t12sigma;
    /// Hit time 
    double time;
    /// Hit Error
    double timeError;
    
};

/**\brief Hit routines specification for APV hit type
 *
 * This structure provides routines and definitions for APV hits object.
 *
 * \todo add description of "unitialized" state of APV hits
 */
template<>
struct EvFieldTraits<APVHit> {
    typedef HitsMap<APVHit> MapType;

    /// Returns reference to a APV hits map from Event object
    static MapType & map( Event & e );
    /// Returns certain bank for the type
    static ObjPool<APVHit> & bank( HitBanks & bs ) { return bs.bankAPV; }
    /// Returns a list of hits maps specific for certain APV detector id
    static std::vector<MapType*>
        maps_for( Event & e, DetID did, const nameutils::DetectorNaming & );
    /// Erases hit entry from all the maps within an event
    static void remove_from_event( Event &
                                 , MapType::Iterator
                                 , const nameutils::DetectorNaming & );
    /// Returns an inserter instance from abstract source object, repsonsible
    /// for insertion hits of APV type
    static HitsInserter<APVHit> & inserter( AbstractEventSource & src );
    #if 0  // XXX
    /// Appends substitution dictionary with various kin-specific information
    /// (wire number, etc)
    static void
        append_subst_dict_for( DetID did
                             , std::map<std::string, std::string> &
                             , const nameutils::DetectorNaming & );
    #endif
    /// Drops the hit to "uninitialized" state
    static void reset_hit( APVHit & );

    /// Returns unique detector ID without variable payload; for APV detectors
    /// it means getting rid of wire number
    static DetID_t uniq_detector_id(DetID);

    /// A type of getter callback used to dynamically parameterize various
    /// histogramming objects
    typedef double (*ValueGetter)(const APVHit &);

    static struct GetterEntry {
        ValueGetter getter;
        const char * name;
        const char * description;
    } * getters;

    #if defined(CapnProto_FOUND) && CapnProto_FOUND
    typedef capnp::na64dp::APVHit PackedType;
    static void pack( const APVHit &, PackedType::Builder, serialization::StatePacking & );
    static void unpack( PackedType::Reader, APVHit &, serialization::StateUnpacking & );
    #endif
};


}
