#pragma once

#include "na64event/hitsMap.hh"
#include "na64event/hitTraits.hh"
#include "na64event/pools.hh"

#if defined(CapnProto_FOUND) && CapnProto_FOUND
#include "event.capnp.h"
#endif

namespace na64dp {

struct Event;  // fwd
namespace serialization {
    struct StatePacking;  // fwd
    struct StateUnpacking;  // fwd
}

/// Keeps APV raw data
struct StwTDCRawData {
    /// Triggered wire number
    uint16_t wireNo;
    /// Measured amplitude samples on a waire
    uint32_t time;
};

/// Represents an activated (triggered) StwTDC.
struct StwTDCHit {
	/// Hit raw data (DDD digit)
	StwTDCRawData rawData;
    /// Waveform samples array
    double position;
   
};

/**\brief Hit routines specification for StwTDC hit type
 */
template<>
struct EvFieldTraits<StwTDCHit> {
    typedef HitsMap<StwTDCHit> MapType;
    /// Returns reference to a SADC hits map from Event object
    static MapType & map( Event & e );
    /// Returns certain bank for the type
    static ObjPool<StwTDCHit> & bank( HitBanks & bs ) { return bs.bankStwTDC; }
    /// Returns a list of hits maps specific for certain SADC detector id
    static std::vector<MapType*>
        maps_for( Event & e, DetID did, const nameutils::DetectorNaming & );
    /// Erases hit entry from all the maps within an event
    static void remove_from_event( Event &
                                 , MapType::Iterator
                                 , const nameutils::DetectorNaming & );
    /// Returns an inserter instance from abstract source object, repsonsible
    /// for insertion hits of StwTDCHit type
    static HitsInserter<StwTDCHit> &
        inserter( AbstractEventSource & src );
    #if 0
    /// Appends substitution dictionary with various kin-specific information
    /// (cell identifier, etc)
    static void
        append_subst_dict_for( DetID did
                             , std::map<std::string, std::string> &
                             , const nameutils::DetectorNaming & );
    #endif
    /// Drops the hit to "uninitialized" state
    static void reset_hit( StwTDCHit & );

    /// Returns unique detector ID without variable payload; for APV detectors
    /// it means getting rid of wire number
    static DetID_t uniq_detector_id(DetID);

    /// A type of getter callback used to dynamically parameterize various
    /// histogramming objects
    typedef double (*ValueGetter)(const StwTDCHit &);

    static struct GetterEntry {
        ValueGetter getter;
        const char * name;
        const char * description;
    } * getters;

    #if defined(CapnProto_FOUND) && CapnProto_FOUND
    typedef capnp::na64dp::StwTDCHit PackedType;
    static void pack( const StwTDCHit &, PackedType::Builder, serialization::StatePacking & );
    static void unpack( PackedType::Reader, StwTDCHit &, serialization::StateUnpacking & );
    #endif
};
}
