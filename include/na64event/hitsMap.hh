#pragma once

#include "na64event/objRef.hh"
#include "na64detID/detectorID.hh"
#include "na64util/str-fmt.hh"

#include <cassert>
#include <map>

namespace na64dp {

template<typename HitT> class HitsInserter;  // fwd

namespace errors {

/* \brief An error thrown if hit refers to more than one detector entity in
 *        hits map where it is prohibited.
 * 
 * Has to be understood as a mistake in the name mappings or hit insertion
 * procedures.
 *
 * \note Does not arise in multimap hit maps, like map of APV clusters
 */ 

class HitKeyIsNotUniqe : public std::runtime_error {
public:
    HitKeyIsNotUniqe( const char * desc )
        : std::runtime_error(desc) {}
};

}  // namespace na64dp::errors

namespace aux {
template<typename KeyT, typename ValueT> using Map = std::map<KeyT, ValueT>;
template<typename KeyT, typename ValueT> using Multimap = std::multimap<KeyT, ValueT>;

template< template<typename, typename> class MapT
        , class KeyT
        , class ValueT
        > struct Registrar;

template< typename KeyT
        , typename ValueT>
struct Registrar<Map, KeyT, ValueT> {
    static typename Map<KeyT, ValueT>::iterator put(Map<KeyT, ValueT> & m, KeyT k, ValueT v) {
        auto ir = m.emplace(k, v);
        if( ! ir.second ) {
			
			/// \todo resolve issue with multiple Straw entries
			throw errors::HitKeyIsNotUniqe("Key exists.");
		}
        return ir.first;
    }
};

template< typename KeyT
        , typename ValueT>
struct Registrar<Multimap, KeyT, ValueT> {
    static typename Multimap<KeyT, ValueT>::iterator put(Multimap<KeyT, ValueT> & m, KeyT k, ValueT v) {
        return m.emplace(k, v);
    }
};

}

/** \brief Template type alias for objects of certain pool mapped with key.
 *
 * Indexes entries over a certain object pool instance by certain key type.
 * Parameterised with key, object type and container type. Expected to be
 * strictly bound to certain bank (pool allocator).
 *
 * \note on iterators: currently, an own iterator has redundant information on
 * the pool the object belongs to. Most frequently used (multi)map's iterator
 * have this reference, but this class is not restricted to (multi)map. Proper
 * usage implies involving a traits, but this seems to be an overkill currently.
 *
 * \todo custom exception for hit being not found.
 * \todo elaborate error reports
 * \todo rename to PoolMap
 */
template< typename KeyT
        , typename ObjT
        , template <typename, typename> class ContainerT=aux::Map>
class ObjMap : protected ContainerT<KeyT, size_t> {
private:
    /// Reference of pool this map is bound to
    ObjPool<ObjT> & _poolRef;
public:
    typedef ContainerT<KeyT, size_t> Parent;

    /// Re-defined iterator class
    struct Iterator {
        ObjPool<ObjT> & poolRef;
        typename Parent::iterator offsetIt;

        Iterator( ObjPool<ObjT> & p, typename Parent::iterator it )
                    : poolRef(p), offsetIt(it) {}
        std::pair<KeyT, PoolRef<ObjT>> operator*() {
            return std::pair<KeyT, PoolRef<ObjT>>( offsetIt->first
                                                 , PoolRef<ObjT>( poolRef, offsetIt->second ));
        }
        #if 0
        std::pair<KeyT, PoolRef<ObjT>> * operator->() {
            return std::pair<KeyT, PoolRef<ObjT>>( offsetIt->first
                    , PoolRef<ObjT>( poolRef, offsetIt->second ));
        }
        #endif

        bool operator!=( const Iterator & it ) const { return it.offsetIt != offsetIt; }
        bool operator==( const Iterator & it ) const { return it.offsetIt == offsetIt; }
        Iterator operator++(int) {
            return Iterator(poolRef, ++offsetIt);
        }
        Iterator operator++() {
            return Iterator(poolRef, ++offsetIt);
        }
    };

    /// Re-defined const iterator class
    struct ConstIterator {
        const ObjPool<ObjT> & poolRef;
        typename Parent::const_iterator offsetIt;

        ConstIterator( const ObjPool<ObjT> & p, typename Parent::const_iterator it )
                    : poolRef(p), offsetIt(it) {}

        std::pair<KeyT, ConstPoolRef<ObjT>> operator*() {
            return std::pair<KeyT, ConstPoolRef<ObjT>>( offsetIt->first
                                                   , ConstPoolRef<ObjT>( poolRef, offsetIt->second ));
        }

        #if 0
        std::pair<KeyT, PoolRef<ObjT>> * operator->() {
            return std::pair<KeyT, PoolRef<ObjT>>( offsetIt->first
                                                   , PoolRef<ObjT>( poolRef, offsetIt->second ));
        }
        #endif

        bool operator!=( const ConstIterator & it ) const { return it.offsetIt != offsetIt; }
        bool operator==( const ConstIterator & it ) const { return it.offsetIt == offsetIt; }
        ConstIterator operator++(int n) {
            return ConstIterator(poolRef, ++offsetIt);
        }
        ConstIterator operator++() {
            return ConstIterator(poolRef, ++offsetIt);
        }
    };
protected:
    //using Parent::emplace;
    Iterator register_hit( DetID_t did, size_t offset) {
        return Iterator(_poolRef, aux::Registrar<ContainerT, KeyT, size_t>::put(*this, did, offset));
    }
public:
    /// Default ctr; needs a bank instance to be bound with.
    ObjMap( ObjPool<ObjT> & bankRef ) : _poolRef( bankRef) {}

    /// Returns reference to associated memory bank
    ObjPool<ObjT> & pool() { return _poolRef; }
    /// Returns reference to associated memory bank (const)
    const ObjPool<ObjT> & pool() const { return _poolRef; }

    Iterator begin() noexcept { return Iterator(_poolRef, Parent::begin()); }
    Iterator end() { return Iterator(_poolRef, Parent::end()); }
    Iterator find( KeyT k ) { return Iterator(_poolRef, Parent::find(k) ); }
    ConstIterator cbegin() const noexcept { return ConstIterator(_poolRef, Parent::cbegin()); }
    ConstIterator cend() const noexcept { return ConstIterator(_poolRef, Parent::cend()); }
    ConstIterator find( KeyT k ) const { return ConstIterator(_poolRef, Parent::find(k) ); }

    /// Returns reference to a hit or raises std::runtime_error
    ObjT & get(KeyT k) {
        auto it = Parent::find(k);
        if( Parent::end() == it ) {
            NA64DP_RUNTIME_ERROR("Key not found.");
        }
        return _poolRef[it->second];
    }
    /// Returns const reference to a hit or raises std::runtime_error
    const ObjT & get(KeyT k) const {
        auto it = Parent::find(k);
        if( Parent::end() == it ) {
            NA64DP_RUNTIME_ERROR("Hit not found.");
        }
        return _poolRef[it->second];
    }
    /// Returns true if hit is present
    bool has(KeyT k) const {
        return Parent::end() != Parent::find(k);
    }

    /// Provides access to the underlying container
    Parent & container() { return *this; }
    /// Provides access to the underlying container (const)
    const Parent & container() const { return *this; }

    //auto insert( const KeyT &, const ObjT & );  // ...

    using Parent::size;
    using Parent::clear;
    using Parent::empty;

    friend class HitsInserter<ObjT>;
};

template<typename HitT> using HitsMap=ObjMap<DetID_t, HitT>;

}

