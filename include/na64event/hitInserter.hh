#pragma once

#include "na64event/event.hh"
#include "na64event/hitTraits.hh"

namespace na64dp {

/**\brief A hit insertion helper
 *
 * \todo possibly, all this stuff will be re-implemented
 * */
template<typename HitT>
class HitsInserter {
private:
    ObjPool<HitT> & _bankRef;
public:
    /// Constructor referencing a particular bank.
    HitsInserter( ObjPool<HitT> & bankRef ) : _bankRef( bankRef) {}
    /// Used to impose new hit in event composition structure.
    virtual PoolRef<HitT> operator()( Event & eve
                                    , DetID did
                                    , const nameutils::DetectorNaming & nm ) {
        PoolRef<HitT> ref = _bankRef.create();
        size_t hitIdx = ref.offset_id();
        for( auto mapPtr : EvFieldTraits<HitT>::maps_for(eve, did, nm) ) {
            mapPtr->register_hit( did.id, hitIdx );
        }
        EvFieldTraits<HitT>::reset_hit( *ref );
        return ref;
    }
};

}  // namespace na64dp
