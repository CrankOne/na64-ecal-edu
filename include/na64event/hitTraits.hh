#pragma once

#include <cstring>

/**\file NA64DPHitTraits.hh
 * \brief A hit traits specification.
 *
 * Various traits for certain hit types are specified here as partial template
 * specification of class `EvFieldTraits<HitType>`.
 * */

namespace na64dp {

template<typename HitT> struct EvFieldTraits;  // generic is not defined

class AbstractEventSource;  // fwd
template<typename HitT> class HitsInserter;  // fwd
namespace nameutils { class DetectorNaming; }  // fwd

}  // namespace na64dp

