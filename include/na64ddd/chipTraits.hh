#pragma once

#include "na64sw-config.h"

#ifdef DaqDataDecoding_FOUND

#include <ChipSADC.h>
#include <ChipAPV.h>
#include <ChipNA64TDC.h>

#include "na64detID/detectorID.hh"
#include "na64event/event.hh"
#include "na64ddd/source.hh"

namespace na64dp {
namespace ddd {

/// Specification defines how to translate the TBName postfix and some
/// additional hit payload to complete detector ID.
template<typename CSDigitT>
struct CSDigitTraits;


template<>
struct CSDigitTraits<CS::ChipSADC::Digit> {
    typedef SADCHit HitType;

    static bool complete_detector_id( const DDDEventSource::NameCache &
                                    , DetID &
                                    , const CS::ChipSADC::Digit & );
    /// Copies waveform
    static void impose_hit_data( const CS::ChipSADC::Digit & sadc
                               , SADCHit & sadcHit );
};

template<>
struct CSDigitTraits<CS::ChipAPV::Digit> {
    typedef APVHit HitType;

    static bool complete_detector_id( const DDDEventSource::NameCache &
                                    , DetID &
                                    , const CS::ChipAPV::Digit & );
    /// Copies APV data
    static void impose_hit_data( const CS::ChipAPV::Digit & apv
                               , APVHit & apvHit );
};

template<>
struct CSDigitTraits<CS::ChipNA64TDC::Digit> {
    typedef StwTDCHit HitType;

    static bool complete_detector_id( const DDDEventSource::NameCache &
                                    , DetID &
                                    , const CS::ChipNA64TDC::Digit & );
    /// Copies APV data
    static void impose_hit_data( const CS::ChipNA64TDC::Digit & stwtdc
                               , StwTDCHit & stwtdcHit );
};

}  // namespace na64::ddd
}  // namespace na64

#endif

