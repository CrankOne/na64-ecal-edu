#pragma once

#include "na64util/vctr.hh"

#include <G4GDMLAuxStructType.hh>

#include <map>
#include <set>

class G4LogicalVolume;
class G4VPhysicalVolume;
class G4GDMLAuxStructType;

namespace na64dp {
namespace mc {

struct GDMLDocumentParameterization {
    G4String setupName;
    G4VPhysicalVolume * worldPtr;
    GDMLDocumentParameterization() : worldPtr(nullptr) {}
};

/// Interface for logical volume aux info processor
class iAuxInfoProcessor {
public:
    virtual void process_auxinfo( const G4GDMLAuxStructType & auxStruct
                                , G4LogicalVolume * lvPtr ) = 0;
};

/**\brief A class interfacing selection of specific aux info processor.
 *
 * Ususally used as a mixture to detector construction implementation class.
 *
 * \todo Consider messenger for creation/configuration of the processors.
 */
class AuxInfoIndex : public std::map<std::string, iAuxInfoProcessor *> {
private:
    /// Set of auxinfo processors to be silently ignored
    std::set<std::string> _auxInfoIgnore;
public:
    AuxInfoIndex();
    virtual ~AuxInfoIndex() {}
    /// Iterates over logical volume storage processing the auxiliary GDML tags.
    /// May be called with `volumePtr` being NULL, meaning that global aux info
    /// list is provided as a second argument.
    virtual void process_auxinfo( G4LogicalVolume * volumePtr
                                , const G4GDMLAuxListType & auxInfoList );
    /// Called by external code to make auxinfo invocation routine ignore
    /// certain tag types
    void ignore_auxinfo(const std::string &);
};

}  // namespace na64dp::mc

template<> struct CtrTraits<mc::iAuxInfoProcessor> {
    typedef mc::iAuxInfoProcessor * (*Constructor)
        ( const G4String &  // msgrPath
        , mc::GDMLDocumentParameterization &
        );
};

}  // namespace na64dp

# define REGISTER_GDML_AUX_INFO_TYPE( clsName, msgrPath, rs, desc ) \
static na64dp::mc::iAuxInfoProcessor * _new_ ## clsName ## _instance( const G4String & \
                                , na64dp::mc::GDMLDocumentParameterization & \
                                ); \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<na64dp::mc::iAuxInfoProcessor>( # clsName, _new_ ## clsName ## _instance, desc ); \
static na64dp::mc::iAuxInfoProcessor * _new_ ## clsName ## _instance( \
          __attribute__((unused)) const G4String & msgrPath \
        , __attribute__((unused)) na64dp::mc::GDMLDocumentParameterization & rs \
        )

