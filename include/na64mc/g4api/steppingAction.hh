#pragma once

#include "na64util/numerical/sums.h"
#include "na64mc/genericMessenger.hh"
#include "na64mc/notifications.hh"
#include "na64mc/stepDispatcher.hh"
#include "na64mc/modules.hh"

#include <G4UserSteppingAction.hh>

#include <unordered_map>
#include <fstream>

#define NA64SW_MC_DIRNAME_STEPPING "stepping"
/// A name of UI dir for stepping handlers
#define NA64SW_MC_DIRNAME_STEPPING_HANDLERS "handlers"
/// A name of UI dir for stepping filters
#define NA64SW_MC_DIRNAME_STEPPING_FILTERS "filters"
/// A name of UI dir for stepping classifiers
#define NA64SW_MC_DIRNAME_STEPPING_CLASSIFIERS "classifiers"

namespace na64dp {
namespace mc {

/**\brief An NA64SW stepping action accomplishing various diagnostic tasks
 *
 * Provided features are useful for debugging and general validity checks, can
 * be enabled in macro via NA64MC messenger.
 *
 * \note Order of the step handlers invokation is arbitrary.
 *
 * \note Avoid using this for collecting actual physics data as it will
 * decrease overall modularity. Consider `G4VSensitiveDetector` instead.
 * */
class SteppingAction : public G4UserSteppingAction
                     , public GenericG4Messenger
                     {
private:
    /// Sub-actions
    std::multimap<iStepFilter *, iStepHandler *> _destinations;
    /// Reference to step handlers dict; SA does not modify dict
    std::map<std::string, iStepHandler *> & _handlers;
    /// Reference to step filters dict; SA does not modify dict
    std::map<std::string, iStepFilter *> & _filters;
public:
    SteppingAction( const std::string & macroCmdPrefix
                  , std::map<std::string, iStepHandler *> &
                  , std::map<std::string, iStepFilter *> &
                  );

    /// `G4UserSteppingAction` interface implementation
    ///
    /// \todo optimize filter appliocation using the `equal_range`
    virtual void UserSteppingAction(const G4Step*);

    /// Binds the existing handler instance with the stepping action
    ///
    /// \todo G4-exception if not found
    static void ui_cmd_add_handler( GenericG4Messenger *, const G4String & );
    /// Binds the existing step filter and handler with the stepping action
    ///
    /// \todo G4-exception if filter or handler not found
    static void ui_cmd_add_filtered_handler( GenericG4Messenger *, const G4String & );
};


template<>
class UIModule<G4UserSteppingAction> : public GenericG4Messenger
                                     , public GenericUIModule
                                     , public StepClassifiers {
public:
    /// Step handlers dict
    std::map<std::string, iStepHandler *> handlers;
    /// Step filters dict
    std::map<std::string, iStepFilter *> filters;
private:
    /// Type name of the object in use
    G4String _inUse;
    /// Pointer to an object in use
    G4UserSteppingAction * _objPtr;
    /// Reference to a modular configuration to operate with
    ModularConfig & _cfg;
protected:
    /// Ctr; requires a base UI path to use
    UIModule( const G4String & rootPath
            , ModularConfig & cfg
            );
public:
    /// Returns pointer to object under control
    G4UserSteppingAction * get_ptr_in_use() { return _objPtr; }
    /// Returns pointer to object under control (const)
    const G4UserSteppingAction * get_ptr_in_use() const { return _objPtr; }
    /// Returns name of the object under control
    const G4String & get_name_in_use() const { return _inUse; }

    /// Instantiates a new stepping action
    static void ui_cmd_instantiate_stepping_action( GenericG4Messenger *, const G4String & );
    /// Prints list available step handlers with their short descriptions
    ///
    ///\todo Catch for `NoCtrsOfType`
    static void ui_cmd_list_step_handlers_classes(GenericG4Messenger*, const G4String &);
    /// Creates new handler instance
    static void ui_cmd_create_step_handler(GenericG4Messenger*, const G4String &);
    /// Creates new handler, bound with classifier
    static void ui_cmd_create_classifying_step_handler(GenericG4Messenger*, const G4String &);
    /// Prints list available step filters with their short descriptions
    ///
    ///\todo Catch for `NoCtrsOfType`
    static void ui_cmd_list_step_filters_classes(GenericG4Messenger*, const G4String &);
    /// Creates new step filter instance
    static void ui_cmd_create_step_filter(GenericG4Messenger*, const G4String &);
    /// Creates a new step classifier instance
    static void ui_cmd_define_step_classifier(GenericG4Messenger*, const G4String &);

    friend class ModularConfig;
};

}  // na64dp::mc
}  // na64dp

