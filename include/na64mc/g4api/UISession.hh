#pragma once

#include <G4UIsession.hh>

#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

/**\brief Integrates Geant4 UI and current NA64SW's logging system
 *
 * A shimmering session intercepting the cerr/cout logs produced by Geant4
 * system messaging protocol.
 * */
class UISession : public G4UIsession {
protected:
    /// Reference to NA64SW logging handle for native Geant4 category
    log4cpp::Category & _logCat;
public:
    UISession( const std::string & sysCat="Geant4");
    virtual G4int ReceiveG4cout( const G4String& coutString ) override;
    virtual G4int ReceiveG4cerr( const G4String& cerrString ) override;
};

}
}

