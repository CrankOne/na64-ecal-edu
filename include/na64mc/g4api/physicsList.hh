#pragma once

#include "na64util/vctr.hh"
#include "na64mc/modules.hh"

#include <G4PhysListFactory.hh>

/// A special name for runtime-assembled physics list
#define NA64SW_MC_LOCAL_PHYS_LIST_NAME "local"

class G4VPhysicsConstructor;

namespace na64dp {

#if 0
// Define traits for G4VModularPhysicsList base whose subclasses we are going
// to instantiate with VCtr
template<> struct CtrTraits<G4VModularPhysicsList> {
    typedef G4VModularPhysicsList * (*Constructor)();
};
#endif

namespace mc {

class PhysicsList : public G4VModularPhysicsList {
public:
    PhysicsList( const G4String & );
    ~PhysicsList();

    virtual void ConstructParticle() override;
    virtual void ConstructProcess() override;

    void add_physics_ctr(const G4String & name);

    void add_step_max_limiter();
private:
    //PhysicsListMessenger* fMessenger;
    G4String fEmName;
    G4VPhysicsConstructor*  fEmPhysicsList;
    G4VPhysicsConstructor*  fDecayPhysics;
    G4VPhysicsConstructor*  fHadPhysicsList;
};


template<>
class UIModule<G4VModularPhysicsList> : public GenericG4Messenger
                                      , public GenericUIModule
                                      , public G4PhysListFactory {
private:
    /// Type name of the object in use
    G4String _inUse;
    /// Pointer to an object in use
    G4VModularPhysicsList * _objPtr;
    /// Reference to a modular configuration to operate with
    ModularConfig & _cfg;
protected:
    /// Ctr; requires a base UI path to use
    UIModule( const G4String & rootPath
            , ModularConfig & cfg
            );
public:
    /// Returns pointer to object under control
    G4VModularPhysicsList * get_ptr_in_use() { return _objPtr; }
    /// Returns pointer to object under control (const)
    const G4VModularPhysicsList * get_ptr_in_use() const { return _objPtr; }
    /// Returns name of the object under control
    const G4String & get_name_in_use() const { return _inUse; }

    /// Set the object to use
    static void ui_cmd_use( GenericG4Messenger * msgr_
                          , const G4String & typeName );
    /// Prints list of available physics lists wrt native Geant4 factory
    static void ui_cmd_list( GenericG4Messenger * msgr_
                           , const G4String & typeName );
    /// Prints list of availavle E/M options wrt native Geant4 factory
    static void ui_cmd_list_em( GenericG4Messenger * msgr_
                              , const G4String & typeName );
    /// Sets verbosity level of factory
    static void ui_cmd_set_verbose( GenericG4Messenger * msgr_
                                  , const G4String & typeName );

    friend class ModularConfig;
};

}
}

