#pragma once

#include "na64util/vctr.hh"
#include "na64calib/dispatcher.hh"
#include "na64mc/notifications.hh"
#include "na64mc/partsIndex.hh"

#include <G4VSensitiveDetector.hh>

namespace na64dp {
namespace mc {

class EventInformation;
class MultipleSensitiveDetector;

/**\brief A helper sensitive detector class
 * */
class SensitiveDetector : public G4VSensitiveDetector {
public:
    SensitiveDetector( const G4String & sdName );
    /// Retrieves event information instance for current event, if available
    static EventInformation * get_einfo();
protected:
    using G4VSensitiveDetector::ProcessHits;
    friend MultipleSensitiveDetector;
};

}

template<> struct CtrTraits<mc::SensitiveDetector> {
    typedef mc::SensitiveDetector * (*Constructor)(
            const G4String &, const G4String &, calib::Dispatcher &, mc::Notifier & );
};

}

# define REGISTER_SENSITIVE_DETECTOR( clsName, name, msgrPath, calibDsp, nfr, desc )  \
    static ::na64dp::mc::SensitiveDetector * _new_ ## clsName ## _instance(     \
          const G4String & \
        , const G4String & \
        , na64dp::calib::Dispatcher & \
        , na64dp::mc::Notifier & );  \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<::na64dp::mc::SensitiveDetector>( \
        # clsName, _new_ ## clsName ## _instance, desc ); \
static ::na64dp::mc::SensitiveDetector * _new_ ## clsName ## _instance( \
            __attribute__((unused)) const G4String & name  \
          , __attribute__((unused)) const G4String & msgrPath \
          , __attribute__((unused)) na64dp::calib::Dispatcher & calibDsp \
          , __attribute__((unused)) na64dp::mc::Notifier & nfr \
          )


