#pragma once

#include <cmath>
#include <stddef.h>  // for size_t in global ns

namespace na64dp {
namespace calib {

/**\brief Normally distributed value information
 *
 * Normally distributed value representation. Useful to store misc calibration
 * data.
 *
 * \todo doubtful relevance
 * */
struct NDValue {
    double mean, stdDev;
    size_t n;

    NDValue() : mean(std::nan("0"))
              , stdDev(std::nan("0"))
              , n(0) {}

    NDValue( double mean_
           , double stdDev_
           , size_t n_ ) : mean(mean_)
                         , stdDev(stdDev_)
                         , n(n_) {}
};

}
}

