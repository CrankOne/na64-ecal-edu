#pragma once

#include "na64calib/dispatcher.hh"
#include "na64calib/loader-generic.hh"
#include "na64detID/TBName.hh"

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace calib {

class RangeOverrideRunIndex;  // fwd

class iYAMLIndex : public GenericLoader::AbstractCalibDataIndex {
public:
    virtual void append(EventID, const YAML::Node & ) = 0;
};

/// Represents calibration info entry
struct YAMLCalibInfoCtr {
    /// Internal (local to host) calibration data identifier
    Dispatcher::CIDataID ciDataID;
    /// Pointer to data index, may be null if index is not used
    iYAMLIndex * dataIndexPtr;
    /// Particular data index constructor, may be null, if `dataIndexPtr` is set
    iYAMLIndex * (*ctr)();
};

/**\brief configures calibration data manager w.r.t. YAML node.
 *
 * This function implements a synchroneous setup of generic loader data indexes
 * and run indexes described by YAML node.
 *
 * The root YAML node has to be a map with keys corresponding to entries of
 * `ciCtrs`. The behaviour is following:
 *
 *  - for each node name in root node, the corresponding entry from `ciCtrs`
 *    will be checked. If no entry found, the `"calibrations.yaml"` level
 *    warning will be printed and sub-node will be ignored
 *  - for entries being found, the `dataIndexPtr` will be checked. If it is
 *    null, the new instance will be created using `ctr` of corresponding entry
 *    and result will be assigned to `dataIndexPtr` writing info message.
 *  - within the current node, the integer field "validFromRun" has to be
 *    found. Its value is then used to compose an event ID (with
 *    spillNo=0, eventNo=0) that is then, along with the rest of the node data,
 *    forwarded to `append()` method of the `dataIndexPtr` (see
 *    `iYAMLDataIndex<T>::append()`). The info message confirming that data was
 *    appended will be printed.
 *  - the run index instance's (provided by `runIndex` argument) `add_entry()`
 *    method will be called with the event ID, the calibration data type ID and
 *    generic loader name (provided with `loaderName` arg).
 *
 * \param rootNode a root YAML node to iterate over
 * \param runIndex a run index to be appended synchroneously with the loader
 * \param loader a generic loader that is used to collect the data indexes
 * \param loaderName the name of the (generic) loader to be refered from run index
 * \param ciCtrs a dictionary of data index constructors
 *
 * \note We have to highlight that existing data indexes (i.e. when
 *       `dataIndexPtr` is set and `ctr` is not called) will not be added to
 *       the loader as in this case we assume that these objects obey other
 *       lifycycle than this function suggests.
 * */
void
configure_from_YAML( YAML::Node & rootNode
                   , RangeOverrideRunIndex * runIndex
                   , GenericLoader * loader
                   , const std::string & loaderName
                   , std::unordered_map<std::string, YAMLCalibInfoCtr> & ciCtrs
                   );

/**\brief reads naming mapping from YAML description
 *
 * Parses naming declaration provided as YAML node with "chips", "kins" and
 * other various data.
 *
 * \note For APV and SADC chips assigns the `append_completion_context()`,
 * `to_string()`, and `from_string()` static functions from traits.
 *
 * \todo Detailed description on YAML nodes format in doc.
 * */
nameutils::DetectorNaming
mappings_from_yaml_API02( const YAML::Node & root );

}
}

