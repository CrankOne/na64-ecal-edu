#pragma once

#include "na64calib/manager.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace calib {

/**\brief Common calibration data loader class.
 *
 * Maintans set of calibration data indexes, grouped by type. The purpose of
 * this class is to assist user's code with simple loaders like ones depending
 * on simple configuration YAML nodes, ASCII files, etc.
 * */
class GenericLoader : public iCalibDataLoader {
public:
    /**\brief Value setter proxy
     *
     * Assures type correctness when data index sets calibration data into
     * dispatcher.
     * */
    class DispatcherProxy {
    private:
        Dispatcher & _dispatcher;
        Dispatcher::CIDataID _type;
        bool _wasUsed;
    protected:
        DispatcherProxy( Dispatcher & d, Dispatcher::CIDataID ciid )
            : _dispatcher(d), _type(ciid), _wasUsed(false) {}
        bool was_used() const { return _wasUsed; }
    public:
        template<typename T> void set(const T & v) {
            if(Dispatcher::info_id<T>(_type.second) != _type) {
                NA64DP_RUNTIME_ERROR( "Bad type assignment" );  // TODO?
            }
            _dispatcher.set<T>(_type.second, v);
            _wasUsed = true;
        }
        template<typename T> void set(T && v) {
            if(Dispatcher::info_id<T>(_type.second) != _type) {
                NA64DP_RUNTIME_ERROR( "Bad type assignment" );  // TODO?
            }
            _dispatcher.set<T>(_type.second, std::move(v));
            _wasUsed = true;
        }
        template<typename T> DispatcherProxy & operator=(const T & v) {
            set(v);
            return *this;
        }
        friend class GenericLoader;
    };

    /**\brief Abstract data indexing interface
     *
     * Subclass may define the way to work with C++ type and be additionally
     * specialized via `name` argument.
     */
    class AbstractCalibDataIndex {
    public:
        /// Should load data referenced by event ID and name into dispatcher
        virtual void put( EventID, DispatcherProxy & ) = 0;
        /// Should return whether index has data for the event
        virtual bool has( EventID ) = 0;
        //virtual void append( EventID, const YAML::Node & ) = 0;
        virtual ~AbstractCalibDataIndex() {}
    };
private:
    /// Known calibration data type indexes
    std::unordered_map< Dispatcher::CIDataID
                      , AbstractCalibDataIndex *
                      , util::PairHash > _dataIndexes;
public:
    /// Loads calibration data into given dispatcher instance.
    virtual void load_data( EventID eventID
                          , Dispatcher::CIDataID cidID
                          , Dispatcher & d ) override;

    /// Returns whether the specified calibration is available.
    virtual bool has( EventID eventID
                    , Dispatcher::CIDataID cidID ) override;

    virtual void add_data_index( Dispatcher::CIDataID
                               , AbstractCalibDataIndex * );
};

}  // namespacen ::na64dp::calib

namespace errors {
class CalibDataTypeIsNotDefined : public GenericRuntimeError {
private:
    calib::Dispatcher::CIDataID _cdid;
    calib::GenericLoader * _loaderPtr;
public:
    CalibDataTypeIsNotDefined( calib::Dispatcher::CIDataID
                             , calib::GenericLoader * loaderPtr ) throw();
    calib::Dispatcher::CIDataID calib_data_type_id() const throw() { return _cdid; }
    calib::GenericLoader * loader_ptr() const throw() { return _loaderPtr; }
};
}  // namespace ::na64dp::error
}  // namespace na64dp

