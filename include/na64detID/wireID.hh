#include "na64detID/detectorID.hh"

#include <limits>
#include <map>

namespace na64dp {

/**\brief An on-wire hit identifier
 *
 * Define a bit structure of hit identifier related to certain wire on tracking
 * detectors. Used within the DetectorID structure at payloads for APV
 * detectors like GEMs or MuMegas.
 *
 * Stores projection axis identifier (X, Y, U, V) as well as the number of wire
 * (physical or raw). Max wire number is limited by 2^(16-3) = 8192.
 *
 * One has to distinguish "unset" and zero identifier, similar to `CellID`
 * class.
 * */
struct WireID {
    DetIDPayload_t id;

    constexpr static DetIDPayload_t WireMax = (std::numeric_limits<DetIDPayload_t>::max() >> 3) - 1;

    /// Describes projection plane of the hit.
    enum Projection {
        kUnknown = 0,
        kX = 1,     kY = 2,
        kU = 3,     kV = 4,
    };

    WireID() : id(0x0) {}
    WireID(DetIDPayload_t id_) : id(id_) {}

    /// Returns projection code conjugated to given.
    static Projection conjugated_projection_code( Projection );

    /// Returns a single character corresponding to projection enumeration
    /// value: X, Y, U, V
    static char proj_label( Projection );
    /// Returns a projection code by single character
    static Projection proj_code( char );

    /// Projection getter
    Projection proj() const {
        assert( proj_is_set() );  // LCOV_EXCL_LINE
        return (Projection) (0b111 & id);
    }
    /// Projection setter
    void proj( Projection p ) {
        assert( p < 5 );  // LCOV_EXCL_LINE
        unset_proj();
        id |= p;
    }
    /// Unsets the projection value
    void unset_proj() {
        id &= ~0b111;
    }

    /// Wire number getter
    uint16_t wire_no() const {
        return (id >> 3) - 1;
    }
    /// Returns `true` if projection is set
    bool proj_is_set() const {
        return id & 0b111;
    }

    /// Wire number setter
    void wire_no( uint16_t wn) {
        id &= ~((WireMax+1) << 3);
        id |= ((wn+1) << 3);
    }

    /// Makes wire number be not set
    void unset_wire_no() {
        id &= ~((WireMax+1) << 3);
    }

    /// Returns true if wire number is set
    bool wire_no_is_set() const {
        return id & ((WireMax+1) << 3);
    }

    /// Appends textual template completion context with `proj` and `wireNo`
    static void append_completion_context( DetID
                                         , std::map<std::string, std::string> & );
    /// Converts from WireID to string
    static void to_string( DetIDPayload_t, char *, size_t available );
    /// Converts from string to WireID 
    static DetIDPayload_t from_string( const char * );
};

}

