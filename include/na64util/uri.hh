#pragma once

#include "na64sw-config.h"

#include <string>
#include <vector>

#ifdef UriParser_FOUND
#include <uriparser/Uri.h>
#endif

namespace na64dp {
namespace util {

/// A C++ wrapper -- uses C wordexp() function to expand file names.
std::vector<std::string> expand_names( const std::string & expr );

#ifdef UriParser_FOUND
namespace uriparser {

/// A parsed URI identifier representation
class URI {
public:
    URI(std::string uri);
    ~URI();

    bool is_valid() const { return isValid_; }

    std::string scheme()   const { return from_range(uriParse_.scheme); }
    std::string host()     const { return from_range(uriParse_.hostText); }
    std::string port()     const { return from_range(uriParse_.portText); }
    std::string path()     const { return from_list(uriParse_.pathHead, "/"); }
    std::string query()    const { return from_range(uriParse_.query); }
    std::string fragment() const { return from_range(uriParse_.fragment); }

private:
    std::string uri_;
    UriUriA     uriParse_;
    bool        isValid_;

    std::string from_range(const UriTextRangeA & rng) const;
    std::string from_list(UriPathSegmentA * xs, const std::string & delim) const;
};

}
#endif

}
}
