#pragma once

#include <string>

namespace na64dp {
namespace util {

/**\brief Configures the `log4cpp` logging using YAML document
 *
 * Rationale for this function is that default properties configurator of
 * log4cpp does not use their AppenderFactories and, thus, is not extensible.
 * Following original log4cpp convention we split the implementation onto
 * internal class (`...Impl`) and static function.
 *
 * \todo assertions -> real checks & exceptions
 * \todo Support for filters (probably, with VCtr as factory)
 * */
class YAMLLog4cppConfigurator {
public:
    /// Reads the entire document and configures `log4cpp` entities
    static void configure(const std::string & filename);
};

}
}

