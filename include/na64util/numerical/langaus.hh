#pragma once

#include "na64sw-config.h"

#ifdef ROOT_FOUND

#include <TF1.h>
#include <TH1F.h>

namespace na64dp {
namespace util {

extern const Double_t gLandauFMax;  ///< Landau maximum location

/// A convoluted Landau and Gauss distribution functions with scaling parameter
Double_t langaus( Double_t x
                , Double_t width
                , Double_t mp
                , Double_t area
                , Double_t sigma
                , Int_t np=100
                , Double_t sc=5.0
                );

/// `langaus()` alias for ROOT's fitting routines
Double_t langausfun(Double_t *x, Double_t *par);

/// `langaus()` fit shortcut
TF1 * langausfit( TH1F * his
                , Double_t * fitrange
                , Double_t * startvalues
                , Double_t * parlimitslo
                , Double_t * parlimitshi
                , const char * fitOpts
                , Double_t * fitparams
                , Double_t * fiterrors
                , Double_t & chiSqr
                , Int_t & ndf
                );

}
}

#endif

