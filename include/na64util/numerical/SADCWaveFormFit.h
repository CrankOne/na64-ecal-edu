#pragma once

#include "na64sw-config.h"

# ifdef GSL_FOUND

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multifit_nlin.h>

# ifdef __cplusplus
extern "C" {
# endif

union SADCWF_FittingFunctionParameters {
    /// Model parameters for Moyal function (Landau PDF approximation).
    struct MoyalParameters { double p[3], err[3], chisq_dof; } moyalPars;
    /* ... */
};

typedef double (*SADCWF_FittingFunction)( union SADCWF_FittingFunctionParameters *, double );

struct SADCWF_FittingInput {
    /// Number of samples (typically = 32 for experimental event).
    int n;
    /// Samples array.
    double * samples;
    /// Reliability estimation array --- one number per sample.
    double * sigma;
};

/// Model function shortcut.
double moyal( union SADCWF_FittingFunctionParameters * gp, double x );

/// Error calculation function --- instance to be minimized.
int moyal_f( const gsl_vector * p
           , void * data_
           , gsl_vector * f);

/// Jacobian matrix.
int moyal_df( const gsl_vector * p
            , void * data_
            , gsl_matrix * J);

/// Function and matrix simultaneous calculation shortcut.
int moyal_fdf( const gsl_vector * p
             , void * data_
             , gsl_vector * f
             , gsl_matrix * J );

void print_state( FILE * logfile, size_t iter, gsl_multifit_fdfsolver * s);

int fit_SADC_samples( const struct SADCWF_FittingInput * srcData,
                      SADCWF_FittingFunction fitting_function,
                      union SADCWF_FittingFunctionParameters * uFitPars,
                      FILE * logfile );

void fill_data_for_fit(struct SADCWF_FittingInput *data, double *wave, double *sigm, int channel);

# ifdef __cplusplus
}
# endif

# endif  /* GSL_FOUND */

