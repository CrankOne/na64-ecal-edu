#pragma once

#include "na64sw-config.h"

#include <string>
#include <stdexcept>
#include <map>
#include <vector>

namespace na64dp {
namespace util {

/// A C++ bastard of printf() function
std::string format(const char *fmt, ...) throw();

/// String substitution dictionary type
typedef std::map<std::string, std::string> StrSubstDict;

extern const struct StringSubstFormat {
    char bgnMarker[4], endMarker[4];
} gDefaultStrFormat;

/**\brief Renders string template wrt given context dictionary
 *
 * This function performs substitution in string expression `pattern` using the
 * set of key+value pairs provided by `context`.
 *
 * Produces output relying on format string and a map of key-value pairs.
 * E.g. for string template (1st argument) `"Hi, {user}!"` and pair
 * `"user" -> "Joe"` the output string will be `"Hi, Joe!"`. This function is
 * used across the library to "render" various "string templates" producing
 * unique string for various detector types, messages, etc.
 *
 * If `requireCompleteness` is invokes `assert_str_has_no_fillers()` on result.
 *
 * \todo Recusrsive subst (e.g. "{kin}" -> "{kin}{statNum}") leads to
 *       infinite loop. Add exception for this.
 */
std::string str_subst( const std::string & pattern
                     , const StrSubstDict & context
                     , bool requireCompleteness=true
                     , const StringSubstFormat * fmt=&gDefaultStrFormat );

/// Returns if the string has no template fillers
bool str_has_no_fillers( const std::string &, const StringSubstFormat * fmt=&gDefaultStrFormat );

/// Raises an exception if string has template fillers
void assert_str_has_no_fillers( const std::string &, const StringSubstFormat * fmt=&gDefaultStrFormat );

/// A generic utility function splitting space-separated tokens with
/// single-leveled commas expression support.
std::vector<std::string> tokenize_quoted_expression(const std::string &);

}

namespace errors {

class GenericRuntimeError : public std::runtime_error {
public:
    GenericRuntimeError( const char * s ) throw() : std::runtime_error( s ) {}
};

class StringIncomplete : public std::runtime_error {
private:
    std::string _s;
public:
    StringIncomplete( const std::string & s ) throw()
            : std::runtime_error( util::format( "String \"%s\" has template markup"
                        " after substitution.", s.c_str() ) )
            , _s(s) {}
    const std::string & incomplete_string() const throw() {
        return _s;
    }
};

}

}

#ifndef NA64DP_RUNTIME_ERROR
#define NA64DP_RUNTIME_ERROR( fmt, ... ) {                                    \
    throw ::na64dp::errors::GenericRuntimeError(::na64dp::util::format(       \
                "at %s:%d: " fmt, __FILE__, __LINE__, ##__VA_ARGS__           \
            ).c_str() );                                                      \
}
#endif


