#pragma once

#include "na64sw-config.h"

#include "na64calib/manager.hh"
#include "na64calib/data-indexes/yaml.hh"

/// Name of environment variable containing configuration root path
#define NA64SW_CONFIG_ROOT_ENVVAR "NA64DP_CFG_ROOT"
/// Name of environment variable containing modules root path
#define NA64SW_MODULES_DIRS_ENVVAR "NA64SW_MODULES_DIRS"

namespace na64dp {
namespace util {

/// Prints list of registered runtime entities (uses `VCtr`)
void list_registered( std::ostream & os, const char * type );

/// Loads shared object files
void load_modules( const std::set<std::string> & modules_, const char * paths );

/**\brief Sets up the YAML calibration information constructors to be used by
 * `configure_from_YAML()` function.
 *
 * This function is convenient to use for somewhat "standard" initialization
 * procedure, useful for applications
 */
void
setup_default_calibration_indexes(
        std::unordered_map<std::string, na64dp::calib::YAMLCalibInfoCtr> & ctrs,
        na64dp::calib::GenericLoader & genericLoader,
        na64dp::calib::Manager & mgr );

}
}


