# Calibrations Guidelines

## SADC Pedestals Extraction

To extract pedestals data from the run, use `presets/util/sadc-pedestals.yaml`
pipeline configuration. Once done on the data chunk(s), use produced `.root`
file as an input for application `na64sw-sadcp`. It will generate a CSV file
that further may be used as a source of calibration data.

### SADC Pedestals Example: CSV File Used by YAML Config

Run the pedestals extraction procedure on some data:

    $ ./na64-pipe -D@5723 -r ../ecal-edu/presets/pipe-configs/util/sadc-pedestals.yaml \
        /data/cdr01001-002443.dat

The default `.root` output file is called `processed.root`. Use it as an input
for `na64sw-sadcp` application in default configuration as follows:

    $ utils/sadc-pedestals/na64sw-sadcp ./processed.root \
            ../ecal-edu/utils/sadc-pedestals/config.yaml

Produces `.csv` file named `pedestals-fit.csv` (name defined by config). To
test the file, do:

    $ cp pedestals-fit.csv ../ecal-edu/presets/calibrations/pedestals/run

and run:

    $ ./na64-pipe -D@5723 -r ../ecal-edu/presets/pipe-configs/sadc-pedestals-assign.yaml \
        /data/cdr01001-002443.dat



