# Getting Started

As a useful example of running the data processing pipeline let us consider the
ECAL calibration procedure.

## na64dp-pipe Command Line Interface

Having the `na64dp-pipe` application built, one is capable to run the data
processing pipelines composed of registered handler instances. The application
has a kind of command line interface to provide various configuration options
that may not be directly associated with pipeline itself but rather relate to
application runtime. One may get brief summary on these options by running

    $ ./na64dp-pipe -h

Most of the options have two forms: short (given as a single letters prefixed
with one dash `-`) and long (given as word prefixed with double dash `--`).
As it is supposed by the POSIX standard, the short form with argument may and
may not contain space between option letter and argument value, and the long
form arguments may be given with space or with equal sign `=` as standard
`getopt_long()` function is used to parse this argument. Note that `-l,--list`
has an optional argument and thus only `--list=<arg>` form is supported as
[known glib'c `getopt_long()` issue](https://stackoverflow.com/a/1052750/1734499).
Many option arguments have default values being relative to repo's dir.

To gain representative output the pipeline application must be supplied with
at least data source (provided as positional arguments) and the runtime
configuration provided via the `-r,--run` option argument.

### Note on NA64DP\_CFG\_ROOT Environment Variable

To set up some default arguments values (like calibration assets path or input
data source configuration file) consider setting the `NA64DP_CFG_ROOT` variable
in the environment where you assume to run the pipeline to refer to the
directory were this distribution resides. E.g. for standard `ecal-edu`
deployment within a container, running from `ecal-edu.build/` simply do:

    $ export NA64DP_CFG_ROOT=../ecal-edu/

Consider adding it to your `.bashrc` (in container, if you're using the
container) to not enter this command each time you log into the container.

### Listing the "Registered" Entities

Among various options note the `-l,--list` that prints structured output on the
entities being registered within the application: data sources, handlers,
various getters and so on. By default this option makes application to print
all the sections. To restrict it by certain type, use `--list=<section>` syntax
(note the `=` sign), e.g.:

    $ ./na64dp-pipe --list=detector-id-definitions

For instance, the `na64dp-pipe` application needs a data source to perform
processing on. It may acquire input from `stdin` (might be useful for shell
scripts based processing), use file or network connection. The data format is
defined by configuration file provided with `-i,--input-cfg` option while input
source identifiers are provided as positional arguments.

### Pipeline Monitoring

Ongoing pipeline evaluation may take some time (especially when multiple chunks
are used). Although the pipeline application generates some logs during
processing, it is often being polluted by some third party libbraries and stuff,
so we have decided to provide additional way to monitor on the running process,
making it especially useful when running on remote. To get this information one
may consider using the `-D,--display` option parameterised with montoring
destination argument. Use the `-` for `stdout` of curent terminal session OR
`-D@<portNo>` for local port number. When running on the container, this port
has to be exposed (`-p` argument of `docker run` command). Use `5723` port for
common case as this manual often implies it be so.

![Example of monitoring network client output](manual/monitoring-client-example.png)

In case of `stdout` some brief information will be written in current terminal,
often being polluted by some third party output (like CORAL). In the former
case of network connection, one have to run a dedicated client to get the
output. Thus, if you run the pipe in container, on your host run

    $ python bin/monitor.py tcp://172.17.0.2:5723

Correct IP address of container if you see no output/getting the connection
error.

## Example #1: SADC Studies

Let's consider the somewhat common working procedures on the example of SADC
studies devoted to ECAL calibrations. Usually, we have some raw input data
that we want to analyze with the chain of applied procedures generating some
histogram output. Some prefab pipeline configuration coposing the handlers in
a meaningful pipeline is provided with the repo by
`presets/pipe-configs/util/sadc-pedestals.yaml`. One would probably like to
study the content of this file to get the idea of how the handlers are
parameterized and stacked into the pipeline.

To run the pipeline on calibration data, choose proper run number(s), `cd` to
your build directory and run

    $ ./na64dp-pipe --run ../ecal-edu/presets/pipe-configs/util/sadc-pedestals.yaml \
        /data/ecal-calib/part-1/cdr01001-003139.dat \
        /data/ecal-calib/part-2/cdr01002-003139.dat

Here, paths to raw data chunks (like `/data/ecal-calib/part-1/cdr01001-003139.dat`)
are just for example -- you probably will have another paths as you have
provided during container creation. Modify this command with proper path
strings, and, by wish with `-D` monitoring option.

Note the common trouble at this step is the error

    DaqEventsManager::ReadEvent(): Fix the map file problem(s) first!

That indicates that CORAL maps are not being correctly set up. In this case,
check the `NA64DP_CFG_ROOT` environment variable and `CORALMapsDir` value in
input configuration file (by default `presets/default-DDD-input.yaml`) -- it
shall refer to `maps/` dir within `p348-daq` repo usually adjacent to outs.

It will take some time to finish the processing of two chunks. Oce completed,
the `processed.root` file will appear at the current directory (as specified by
default option `-o,--root-file`). You may browse through the file to see how
the histograms are sorted within the catalogues:

    $ root
    > new TBrowser

and then open the `processed.root` file and navigate through the `TDirectory`
within; double click on histograms to see them on the right `TCanvas` area.

![Example of histgram in processed file being opened with TBrowser](manual/tbrowser-example.png)

Albeit it is possible to see some data features with `TBrowser`, it is common
practice to write some summary scripts for more representative look. For example
to get an overview of corrected amplitudes on the ECAL, run

    $ root -l "$(readlink -f ../ecal-edu/utils/scripts/ecal_view_hst.C)(\"amps-pedCorrected\",\"processed.root\")"

Note the back-slashes (`\`) -- they needed to ecape quote signs (`"`) as
the entire line given after `-l` is, in fact, the C-expression fed to ROOT
interpreter.

This command will run `ecal_view_hst.C` script with eponymous function within
set up to draw `"amps-pedCorrected"` histograms for ECAL as two 6x6 figures on
data taken from `processed.root` file. One may study the script to get the
gist of building the summary plot from `processed.root` file.

![Example output of `ecal_view_hst.C` script](manual/ecal-overview.png)

Writing this kind of post-processing or summary extraction scripts are the
common way to produce readable reports over a bunch of detector histograms. For
iterative procedures involving operations on the event basis, specific or not
to some kinds of detectors, consider [writing your own handler](\ref HandlersDevGuide).

