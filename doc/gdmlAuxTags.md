# GDML Auxiliary Tags

`<auxiliary auxtype="SensitiveDetector" auxvalue="direct:/calo/ECAL"/>`

## SensitiveDetector

This kind of auxiliary tags provide interface for binding the
`SensitiveDetector` instance with volume.

Examples:
    - `direct:/some/sens/det`

