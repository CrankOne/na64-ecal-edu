# Заметки по рефакторингу хэндлеров

Настоящая заметка написана 27-29 февраля и посвящена описанию обзей схемы
рефакторинга хэндлеров, накопившихся за осенний семестр. Я переделал большую
часть библиотеки, и теперь хэндлеры нуждаются в рефакторинге в свою очередь.
Работа эта однако, с одной стороны сравнительно механическая, а с другой --
подразумевает знакомство с небольшими новшествами, так что я счёл
целесообразным попросить о ней студентов.

В качестве примера я рассмотрел рефакторинг трёх хэндлеров, от простейшего к
наиболее сложному. В заключении дан список хэндлеров нуждающихся в рефакторинге
вместе с указанием некоторых соображений, которые можно принять во внимание
приступая к рефакторингу.

## Простейший случай; декларация

Рассмотрим для начала простейший хэндлер который мы писали, не занятый работой
с каким-то конкретным типом данных в событии, а предназначенный для
дискриминации событий в целом: `PickEventsRange`.

Декларация класса выглядит как-то так:

        # pragma once
        # include "handlers/AbstractHandler.h"

        class PickEventsRange : public AbstractHandler {
        public:
            typedef unsigned long EventNumber_t;
        private:
            EventNumber_t _from, _to;
            bool _negate;
            EventNumber_t _counter;
        public:
            PickEventsRange( EventNumber_t from_
                           , EventNumber_t to_
                           , bool negate_);
            // Returns starting number of events to be left for further processing.
            EventNumber_t from() const { return _from; }
            // Returns final number of events to read.
            EventNumber_t to() const { return _to; }
            // Returns true if current event is allowed for read.
            virtual bool ProcessEvent(Event * event) override;
        };

Во-первых, изменился заголовочный файл с декларацией абстрактного хэндлера. Он
переехал в `NA64DPAbstractHandler.hh` (кстати, сам заголовочник хэндлера теперь
должен лежать в `NA64DPHandlers/data/PickEventsRange.hh` вместо
`handlers/data/PickEventsRange.h`). Тут важно отметить, что мы намерены
теперь следовать строгому неймингу файлов: все заголовочные файлы первого
уровня, а так же поддиректории `include/` приобрели префикс `NA64DP`. Это
полезно будет впоследствии, когда наш пакет будет устанавливаться в систему.

Во-вторых, все C++-декларации теперь должны находиться как минимум в namespace
`na64dp`.

В-третьих изменилась тип возвращаемого значения метода `process_event()` класса
`AbstractHandler` -- теперь этод метод возвращает одно из значений определённых
внутри класса `AbstractHandler` -- `kOk`, `kDiscriminateEvent`,
`kStopProcessing` или `kAbortProcessing`. Тип данных для этих значений
определён как `ProcRes`. С учётом всех этих замечаний, обновлённый хэндлер для
дисриминации номеров событий выгляит как:

        # pragma once
        # include "NA64DPAbstractHandler.hh"

        namespace na64dp {

        class PickEventsRange : public AbstractHandler {
        public:
            typedef unsigned long EventNumber_t;
        private:
            EventNumber_t _from, _to;
            bool _negate;
            EventNumber_t _counter;
        public:
            PickEventsRange( EventNumber_t from_
                           , EventNumber_t to_
                           , bool negate_);
            // Returns starting number of events to be left for further processing.
            EventNumber_t from() const { return _from; }
            // Returns final number of events to read.
            EventNumber_t to() const { return _to; }
            // Returns true if current event is allowed for read.
            virtual ProcRes process_event(Event * event) override;
        };

        }

Обратите внимание, что `ProcessEvent()` теперь `process_event()` согласно схеме
нейминга которую мы обсуждали. Кроме того, не мешало бы продокументировать
хэндлер более подробно, согласно разметке Doxygen. Пока не буду вносить этих
изменений, чтобы не сбивать.

## Простейший случай; реализация

Реализация которая раньше была в `src/handlers/data/PickEventsRange.cc` теперь
должна находиться в `src/NA64DPHandlers/data/PickEventsRange.cc`. Выглядела она
вот так:

    #include "handlers/data/PickEventsRange.h"

    PickEventsRange::PickEventsRange( EventNumber_t from_
                                    , EventNumber_t to_
                                    , bool negate_=false
                                    ) : _from(from_)
                              , _to(to_)
                              , _negate(negate_)
                              , _counter(0) {
        std::cout << "initialized " << this
                  << ", range [" << _from << ", " << _to << "], negate="
                  << (_negate ? "true" : "false") << std::endl;
    }

    bool
    PickEventsRange::ProcessEvent( Event * ) {
        ++_counter;
        return _negate != (_counter > _from && _counter < _to);
    }

    REGISTER_HANDLER( PickEventsRange, yamlNode
                    , "Performs event discrimantion by internal counter." ){
        return new PickEventsRange( yamlNode["from"] ? yamlNode["from"].as<unsigned long>()
                                                 : 0
                              , yamlNode["to"] ? yamlNode["to"].as<unsigned long>()
                                                 : ULONG_MAX
                              , yamlNode["negate"] ? yamlNode["negate"].as<bool>()
                                                   : false );
    }

Понятно, что теперь нужно поменять путь до файла в `#include` на новый,
обернуть весь код в namespace, и привести в соответствие реализацию
`process_event()`. Однако, есть ещё одно изменение, которое коснулось уже
макроса `REGISTER_HANDLER()`. К сожалению, я не смог его обойти, и теперь нужно
добавить в список аргументов макроса ещё два параметра: имя переменных
ссылающихся на банки хитов и на хэндл калибровок. Эти два объекта несколько
эзотерические по смыслу, однако они обеспечивают нам
важные возможности для быстрого выделения дополнительной информации к хиту
(быстрее чем на куче) и способ запрашивать с калибровочные данные
соответственно. Хотя мы и не используем эти переменные в рассматриваемом
хэндлере, логика макроса такова, что какие-то имена вы должны этим переменным
дать (хоть `banks` и `ch`, хоть `_` и `__` -- выбирайте по вкусу):

        #include "NA64DPHandlers/data/PickEventsRange.hh"

        namespace na64dp {

        PickEventsRange::PickEventsRange( EventNumber_t from_
                                        , EventNumber_t to_
                                        , bool negate_=false
                                        ) :  _from(from_)
                                  , _to(to_)
                                  , _negate(negate_)
                                  , _counter(0) {
            std::cout << "initialized " << this
                      << ", range [" << _from << ", " << _to << "], negate="
                      << (_negate ? "true" : "false") << std::endl;
        }

        AbstractHandler::ProcRes
        PickEventsRange::process_event( Event * ) {
            ++_counter;
            return _negate != (_counter > _from && _counter < _to) ? kOk : kStopProcessing;
        }

        REGISTER_HANDLER( PickEventsRange, banks, ch, yamlNode
                        , "Performs event discrimantion by internal counter." ){
            return new PickEventsRange( yamlNode["from"] ? yamlNode["from"].as<unsigned long>()
                                                     : 0
                                  , yamlNode["to"] ? yamlNode["to"].as<unsigned long>()
                                                     : ULONG_MAX
                                  , yamlNode["negate"] ? yamlNode["negate"].as<bool>()
                                                       : false );
        }

        }

Как видите, различия пока очень немногочислены.

## Более сложный случай

У нас скопилось довольно много интересных хэндлеров, которые можно использовать
не только для ECAL, но и для других SADC-детекторов (HCAL, WCAL, SRD, LYSO,
etc). В частности, довольно простой детектор `AppendPeds` с заголовочником в
`include/handlers/data/AppendPeds.h`:

        #pragma once

        #include "handlers/AbstractEcalHitHandler.h"
        #include "Event.h"

        class AppendPeds : public AbstractEcalHitHandler {
        public:
            AppendPeds(){}
            AppendPeds(const YAML::Node &){}
            ~AppendPeds(){}
            virtual bool process_hit(ECALHit * currentEcalHit);
        };

Пустой конструктор нам, в принципе, не нужен и его можно удалить.

Этот хэндлер оперирует только с SADC-данными, и делает, в общем довольно
простую вещь, складывая waveform со значениями обоих пьедестлов. При этом
никаких предположений о том что за детектор он обслуживает не делается -- это
может быть 72 шашлыка ECAL'а или единственная "лопата" (пластина)
вето-детектора -- он просто может работать с любыми данными SADC. Это, в
принципе, довольно распространённая ситуация в анализе, когда один инструмент
может (и должен) применяться ко всем детекторам с определённым чипом.

Для такого у нас есть шаблонный класс-родитель автоматически выполняющий
действия только для какого-то одного класса хитов (например SADC) -- он
объявлен в `include/NA64DPAbstractHandler.hh`. Класс-шаблон называется
`AbstractHitHandler` и параметризуется типом хита (`SADCHit` или `APVHit`),
так что полностью тип класса-родителя для `AppendPeds` теперь запишется как
`AbstractHitHandler<SADCHit>`.

У класса-родителя (`AbstractHitHandler<SADCHit>`) есть один важный обязательный
аргумент -- калибровочный хэндл. В нашей библиотеке предусмотрено, что состав
чипов, вообще говоря, может меняться со временем, и поэтому класс выполняющий
обработку какого-то определённого сорта хитов в событии должен быть надлежащим
образом оповещён при изменении калибровки. Все такие классы нуждаются в
указателе на "хэндл" с калибровками, и для этого в `REGISTER_HANDLER` появилась
новая переменная. Нам нужно будет дополнить конструктор `AppendPeds` чтобы он
мог передавать `AbstractHitHandler<SADCHit>` указатель на калибровочный хэндл.

Сигнатура метода `process_hit()` у класса `AbstractHitHandler<SADCHit>`
выглядит иначе чем у нашего старого `AbstractEcalHitHandler`:

        virtual bool process_hit( na64ee::EventNumericID eventID
                                , DetID_t detID
                                , HitT & hit ) = 0;

Добавились два аргумента перед, собственно, хитом. Первый из них идентифицирует
событие. Этот уникальный номер события позволяет нам впоследствии иметь важную
информацию о привязке данных ко времени, выделять для последующего анализа
группы событий и индивидуальные события. Второй аргумент -- идентификатор
детектора. Это новый тип-число, очень похожий на знакомый нам `CellID`, он
позволяет довольно быстро различать детекторы для которых, собственно, и задан
хит.

С учётом перечисленных различий, декларацию `AppendPeds` можно переписать как:

        #pragma once

        #include "NA64DPAbstractHitHandler.hh"
        #include "NA64DPEvent.hh"

        namespace na64dp {

        class AppendPeds : public AbstractHitHandler<SADCHit> {
        public:
            AppendPeds(iCalibHandle * ch)
                    : AbstractHitHandler<SADCHit>(ch) {}
            ~AppendPeds(){}
            virtual bool process_hit( na64ee::EventNumericID eventID
                                        , DetID_t detID
                                        , SADCHit & hit ) override;
        };

        }


Прежняя реализация:

        #include "handlers/data/AppendPeds.hh"

        bool AppendPeds::process_hit(ECALHit * currentEcalHit) 
        {
            //  Add Mean pedestal values back to the Event
            for(int i = 0; i < 32; ++i) {
                if ((i % 2) == 0) {
                    currentEcalHit->wave[i] = currentEcalHit->wave[i] + currentEcalHit->pedestals[0];
                    }
                    else {
                        currentEcalHit->wave[i] = currentEcalHit->wave[i] + currentEcalHit->pedestals[1];
                    }
            }
            return true;
        }

        REGISTER_HANDLER( AppendPeds, yamlNode
                        , "Adding pedestal values to the Event" ) {
            return new AppendPeds();
        }

изменится в соответствии с перечисленными выше соображениями следующим образом:
1. Изменится путь до заголовочного файла
2. Появится объемлющий namespace
3. Изменится сигнатура `process_hit()`, хотя этот хэндлер и не нуждается в
номере события и идентификаторе детектора для своего нехитрого дела.
4. `REGISTER_HANDLER()` получит пару дополнительных аргументов, один из которых
на этот раз должен быть передан в конструктор.

Пожалуй, довольно просто:

    #include "NA64DPHandlers/data/AppendPeds.hh"

    namespace na64dp {

    bool AppendPeds::process_hit( na64ee::EventNumericID
                                , DetID_t
                                , SADCHit & currentEcalHit) {
        //  Add Mean pedestal values back to the Event
        for(int i = 0; i < 32; ++i) {
            if ((i % 2) == 0) {
                    currentEcalHit.wave[i] = currentEcalHit.wave[i] + currentEcalHit.pedestals[0];
                }
                else {
                    currentEcalHit.wave[i] = currentEcalHit.wave[i] + currentEcalHit.pedestals[1];
                }
        }
        return true;
    }

    REGISTER_HANDLER( AppendPeds, banks, ch, yamlNode
                    , "Adding pedestal values to the Event" ) {
        return new AppendPeds(ch);
    }

    }

Нужно обратить внимание на то что вместо `ECALHit` у нас теперь `SADCHit`,
который, впрочем, отличается от `ECALHit` только тем что в нём нет поля `id`
(эту роль теперь выполняет более общий идентификатор `DetID_t`). Ну и хит стал
ссылкой вместо указателя -- тут, думаю, вам всё понятно.

## ECAL-специфичный хэндлер

Я посмотрел наши хэндлеры, и, вообще говоря, не нашёл ни одного который бы
нельзя было применить только к ECAL и он бы не мог бы хотя бы в перспективе
понадобиться для других SADC-детекторов.

Единственное место где то что мы уже сделали оказалось действительно
привязанным к ECAL, и это касается практически всех хэндлеров -- гистограммы.
Мы писали наши гистограммы в том предположении что ячейки могут быть
рассмотрены в структуре 2х6х6. Подобную организацию можно выделить в задачу
решаему отдельным классом -- у нас уже есть класс с такой рудиментарной
функциональностью -- `TDirAdapter`. Я его изменил с тем чтобы он отвечал более
общей задаче менеджмента структуры объектов `TDirectory` в общем случае.

Давайте рассмотрим, как преобразуется хэндлер `EcalAmpHists`. Его декларация:

        #pragma once

        #include "handlers/AbstractEcalHitHandler.h"
        #include "TDirAdapter.h"

        #include "DaqEventsManager.h"
        #include "TH2.h"

        #include <yaml-cpp/node/node.h>
        #include <vector>

        class EcalAmpHists : public TDirAdapter
                           , public AbstractEcalHitHandler {
            size_t _ampNBins;
            double _ampRange[2];
            TH2F * hist[6][6][2];
            TDirectory * tDir[3];
        private:
            // called within ctrs to allocate and initialize histograms
            void _initialize_histograms();
        public:
            // Creates default histograms; amp range: 0-4096, nBins = 100
            EcalAmpHists();
            EcalAmpHists( size_t nAmpBins, double ampMin, double ampMax
                        , const std::string & tDirName="ECAL-Amplitudes"
                        );
            ~EcalAmpHists();
            //bool FillHist(std::string name, int x, int y, const std::vector<CS::uint16> & wave);
            void WriteHistTfile();
            
            virtual bool process_hit(ECALHit * currentEcalHit);
            virtual void Finalize();
        };

Помимо всех тех изменений которые имели место в более простых случаях, теперь
понадобится иметь в виду, что целесообразно обобщить хэндлер для работы с
произвольным SADC-хитом. Пусть теперь всё знание о том как именно размещены в
`TFile` экземпляры `TH2F` будет предметом `TDirAdapter`, а наш текущий хэндлер
будет заниматься только, собственно, заполнением гистограмм. Для этого:

1. Вместо `TH2F* hst[6][6][2]` заведём просто ассоциативный массив указателей
гистограмм идентифицируемых по числу типа `DetID_t`. Напомню, что `DetID_t` это
номер уникальный для каждого хита, содержащий информацию о чипе, типе и номере
детектора, а так же номере ячейки. Удобно для этого использовать ассоциативный
массив: `std::map<DetID_t, TH2F*>`.
2. Теперь нет нужды хранить на этом уровне указатели на `TDirectory` -- эти
указатели легко получить по запросу родительского метода
`TDirectory * TDirAdapter::get_dir_for(DetID_t)`.
3. Конструктор принимает текстовое значение (аргумент `tDirName`) которое
должно означать директорию в которую будет вестись запись. Практика показала,
что это не очень удобно -- для нескольких хэндлеров у нас как правило
получалось столько же директорий, однако содержащих в лучшем случае 36
гистограмм просматривать которые не очень удобно. Поэтому теперь я предлагаю
в реализации `TDirAdapter` использовать схему в которой создаётся отдельная
директория для каждого детектора (в общем смысле, в узком, применительно к
ECAL -- ячейки). Мы всё ещё должны избежать случая коллизии имён, и дать автору
конфига возможность выбирать имена гистограмм, так что аргумент останется, но
поменяет смысл -- он будет обозначать гистограмму.

Последний пунк поясню на примере. Было:

    `-+ (dir) ECAL-before
      +- (TH2F) ECAL_hist_0_0_0
      +- (TH2F) ECAL_hist_0_0_1
      +- (TH2F) ECAL_hist_0_1_0
      ...
      `- (TH2F) ECAL_hist_5_5_1

Станет:

    `-+ (dir) ECAL
      +- (dir) x1
       +- (dir) y1
        +- (TH2F) amp-1-1-preshower
        `- (TH2F) amp-1-1-main
       +- (dir) y2
        +- (TH2F) amp-1-2-preshower
        `- (TH2F) amp-1-2-main
        ...
      +- (dir) x2
       +- (dir) y1
    ...

Таким образом, если раншье путь до какой-то конкретной гистограммы с амплитудами
выглядел как `ECAL-before/amp-1-1-0`, теперь это будет
`ECAL/x1/y1/amp-preshower`. Это позволит, кажется, более удобно структурировать
выходной файл, и избавит от мешающегося микроменеджмента.

Кстати сам файл и класс тоже лучше переименовать, и использовать вместо
`EcalAmpHists` название `SADCAmpHists`, отражающее более общий смысл хэндлера.

Теперь, с учётом изменений, декларация выглядит так:

        #pragma once

        #include "NA64DPAbstractHitHandler.hh"
        #include "NA64DPTDirAdapter.hh"

        #include <TH2F.h>

        #include <yaml-cpp/node/node.h>
        #include <vector>

        namespace na64dp {

        class SADCAmpHists : public TDirAdapter
                           , public AbstractHitHandler<SADCHit> {
        private:
            size_t _ampNBins;
            double _ampRange[2];
            const std::string _hstBaseName;

            std::map<DetID_t, TH2F *> _hists;
        public:
            SADCAmpHists( iCalibHandle * ch
                        , size_t nAmpBins
                        , double ampMin, double ampMax
                        , const std::string & hstBaseName="amp"
                        );
            ~SADCAmpHists();
            
            virtual bool process_hit( na64ee::EventNumericID
                                    , DetID_t
                                    , SADCHit & currentEcalHit) override;
            virtual void finalize() override;
        };

        }

Я избавился от выделенного метода для создания всех гистограмм, поскольку для
произвольного случая мы не знаем конкретный состав детекторов. Создаваться они
будут по мере появления соответствующих идентификаторов в методе
`process_hit()`. Я так же избавился от пустого конструктора (у нас нет для них
случая когда бы они понадобились). Метод `WriteHistTfile()` который мы добавили
в какой-то момент от безысходности тоже не слишком-то и нужен -- по смыслу
здесь полностью подходит `finalize()`.

Реализацию я разберу отдельно, по методам потому что исходный файл несколько
великоват чтобы его имело смысл дословно цитировать.

Прежде всего, конструктор, занятый в основном передачей аргументов родителям.
Было:

        EcalAmpHists::EcalAmpHists( size_t nAmpBins
                                  , double ampMin, double ampMax
                                  , const std::string & baseName
                                  ) : TDirAdapter( tDirName )
                                    , _ampNBins(nAmpBins)
                                    , _ampRange{ampMin, ampMax} {
            _initialize_histograms();
        }

Стало:

       SADCAmpHists::SADCAmpHists( iCalibHandle * ch
                          , size_t nAmpBins
                          , double ampMin, double ampMax
                          , const std::string & baseName
                          ) : TDirAdapter(ch)
                            , AbstractHitHandler(ch)
                            , _ampNBins(nAmpBins)
                            , _ampRange{ampMin, ampMax}
                            , _hstBaseName(baseName) {}

Обратите внимание, что конструктор `TDirAdapter` теперь параметризуется
указателем на калибровочный хэндл вместо базового имени. Само имя гистограммы
мы теперь сохраним поле класса `_hstBaseName`. Ваш хэндлер может хранить
несколько базовых имён гистограмм, используя их по мере надобности для того
чтобы получить различные пути.

Метод `SADCAmpHists::process_hit()` который раньше брал указатель на
гистограмму из трёхмерного массива:

    bool EcalAmpHists::process_hit(ECALHit * currentEcalHit){
        for(int i = 0; i < 32; ++i){
            hist[currentEcalHit->id.get_x()]
                [currentEcalHit->id.get_y()]
                [(int)( currentEcalHit->id.is_preshower() ? 0 : 1 )]
                ->Fill(i, currentEcalHit->wave[i] );
        }
        return true;
    }

теперь использует ассоциативный массив гистограмм, проиндексированных
по идентификатору детектора:

        bool
        SADCAmpHists::process_hit( na64ee::EventNumericID
                                 , DetID_t did
                                 , SADCHit & currentHit) {
            auto it = _hists.find( did );
            if( _hists.end() == it ) {
                // no histogram exists for this detector entity -- create and insert
                std::string histName = _hstBaseName;
                dir_for(did, histName)->cd();
                TH2F * newHst = new TH2F( histName.c_str()
                                        , "waveform distributions; Channels; Amplitude"
                                        , 32, 0, 32
                                        , _ampNBins, _ampRange[0], _ampRange[1] );
                it = _hists.emplace( did, newHst ).first;
            }
            // fill the histogram
            for(int i = 0; i < 32; ++i){
                it->second->Fill(i, currentHit.wave[i] );
            }
            return true;
        }

Расскажу подробнее про то что там делается:

1. Ищем в индексе существующих гистограмм текущий идентификатор отвечающий
детектору в котором рассматривается хит
2. Если он не найден, создаём такую гистограмму и помещаем её в индекс (метод
`std::map<>::emplace()` в конце тела `if`-выражения)
3. Заполняем гистограмму сэмплами как раньше.

Новое здесь пожалуй только что при создании гистограммы её имя используется не
напрямую. Вот эти три строчки реализуют не совсем тривиальное поведение:

        std::string histName = _hstBaseName;
        dir_for(did, histName)->cd();
        TH2F * newHst = new TH2F( histName.c_str(), ... );

1. Сначала создаётся переменная `histName` в которую копируется значение
`_hstBaseName`.
2. Затем вызывается метод  `TDirAdapter::dir_for()`, который создаёт
необходимую структуру директорий для конкретной гистограммы, используя в
качестве параметров: числовой идентификатор `did` детектора (потому что
структура директорий отвечает определённому детектору -- ECAL, HCAL, etc),
"базовое" имя гистограммы на основе которого будет составлено настоящее имя.
ВНИМАНИЕ! Этот вызов перезапишет содержимое `histName` (она передаётся по
ссылке) -- это имя затем нужно будет использовать в конструкторе `TH2F`.
3. Вызывается конструктор `TH2F`.

Сначала я не планировал изменять передаваемую строку (`histName`), однако
ROOT требует чтобы имена гистограмм были уникальны вне зависимости от того в
какую `TDirectory` они оказываются помещены. Кроме того,
`TDirAdapter::dir_for()` возвращает указатель на `TDirectory` в которую нужно
было бы сохранить гистограмму, однако, как я выяснил, не имеет большого
значения какая директория была текущей при создании рутовых объектов -- важно,
какая была текущей, когда вы вызвали объекту метод `Write()`.

Наконец, метод `finalize()`. Раньше он делегировал выполнение методу
`WriteHistTfile()`:

        void EcalAmpHists::WriteHistTfile () {
            for(int k = 0; k < 2; ++k){
                for(int i = 0; i < 6; ++i){
                    for(int j = 0; j < 6; ++j){
                        get_tdirs()[1+k]->WriteObject( hist[i][j][k]
                                                     , hist[i][j][k]->GetName() );
                    }
                }
            }
        }

        void
        EcalAmpHists::Finalize() {
            WriteHistTfile();
        }

Тут странно, что Илья выбрал именно `WriteObject()`. Согласно документации
`WriteObject()` должен применяться для подклассов `TObject` которые не
реализуют рутового стримера. Впрочем, это не так важно, -- теперь вы можете
использовать геттер `TDirAdapter::tdirectories()` чтобы получить индекс
созданных при помощи `TDirAdapter::dir_for()` директорий, и вот такой
конструкцией записать всё куда надо:

    void
    SADCAmpHists::finalize() {
        for( auto idHstPair : _hists ) {
            tdirectories()[idHstPair.first]->cd();
            idHstPair.second->Write();
        }
    }

# Список оставшихся хэндлеров

Большинство из них лежит в `[src|include]/NA64DPHandlers/[data|plotting]`.

* Везде где возможно, хэндлер должен быть обобщён на случай произвольного
SADC-детектора; по-моему, это вообще все хэндлеры. Обратное было бы, если бы мы,
к примеру, считали в каком-то хэндлере, скажем, параметры э/м ливня на основе
показаний ячеек. Этот ливень может развиваться только в ECAL, и он строго
регулируется параметрами детектора.
* Хэндлеры нужно переименовывать в том случае, если они а) включают слово `ECAL`
(`Ecal`, `ecal`, etc), b) предназначены для работы с SADC-детектором -- в этом
случае полезно вставить где-то слово `SADC`. Так, например, `DynSubtractPeds`
можно переименовать в `DynSubtrSADCPeds`.
* По мере рефакторинга, нужно добавлять файлы в `CMakeLists.txt`. Строка ~73
содержит перечисление всех исходников собираемых в библиотеку которую затем
использует `pipe`. Просто дописывайте их где-то в районе строки 100. Пока
там отсутствуют все непереработанные хэндлеры.

Все заголовочные файлы на C++ должны быть `.hh` а файлы на (чистом) C -- `.h`.

Список подлежащих рефакторингу:

- `data/APVCollectDetNames` -- этим займусь я, он дл APV-детекторов. Скорее всего
просто удалю, теперь он не нужен.
- `data/DynSubtrPeds` -- один из наших первых и один из интереснейших хэндлеров, тут
всё достаточно просто должно быть
- `data/ECALPmtWfFitMoyalHist` -- не ожидаю с ним подводных камней, должен быть
просто обобщён для всех SADC-дететкоров
- `data/EventWrite` -- этим займусь я, он делал сериализацию, теперь у меня есть
более совершенные средства для него.
- `data/JenksClassify` -- он использует коллбэк `ValueExtractor`, которая временно
отсутствует. Думаю, своими силами вы его не почините.
- `data/LinearSum` -- вообще смешной маленький изян
- `data/SubtractPeds` -- прост, но подразумевает, что нужно переделать скрипты Ивана
по извлечению, это гораздо больше работы
- `data/rmsDescriminate` -- не выглядит сложно, однако он может быть объединён
с `ChiDescriminate` с использованием пока отключенной техники `ValueExtractor`.

Все хэнлеры в `plotting/` обещают быть достаточно простыми. Более того, кажется
что с использованием `ValueExtractor` почти все их можно было бы вообще
упростить до единственного общего хэндлера, где переменная для
гистограммирования выбиралась бы параметрически.
