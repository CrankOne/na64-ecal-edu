# Obtaining the Source Code

For our task we typically need two repositories: this one and official NA64
reconstruction sources.

First, fetch this repository itself. It can be cloned with:

    $ git clone https://bitbucket.org/CrankOne/na64-ecal-edu.git ecal-edu

For raw data reading of the NA64, the `DaqDataDecoding` library is an essential
piece. It is a part of COMPASS (NA58, CERN, SPS) experiment software. The
initial bundle managed by NA64 team can be obtained with:

    $ git clone https://gitlab.cern.ch/P348/p348-daq.git

For the sake of simplicity it is reasonable to keep this two repos adjacent to
each other (in same dir). None of them have well-managed `install` target.
The `p348-daq` bundle contains few hardcoded paths to ROOT executables,
while container does not have ROOT installed by system paths. It's convenient
to provide the `.bashrc` of the following form (note that for non-container
environment, paths may differ):

        export ROOTSYS=/usr/lib/root/6.22
        export PATH="$PATH:/usr/lib/root/6.12/bin"
        export ROOT_INCLUDE_PATH=/usr/include/genfit

in the directory where both projects reside.

## Containerized Build

Currently, the primarily maintained way is to run this software within the
container based on
[`crankone/hepfarm-amd64-dbg`](https://hub.docker.com/repository/docker/crankone/hepfarm-amd64-dbg)
Docker image. User pre-sets are exposed in `presets/docker/Dockerfile`, so to
get the functional image do:

    $ cd presets/docker
    $ docker build -t hepfarm4ecal-edu .

This will take some time to fetch necessary packages and leave you with
docker image named `hepfarm4ecal-edu:latest`.

## Building the Libs and Running the Environment

Assuming your data directory to be substituted instead of
`<your-local-data-dir>` and `p348-daq` with `ecal-edu` projects dir instead
of `<your-local-repo-dir>`, run:

    $ docker run --rm --volume <your-local-data-dir>:/data \
                      --volume <your-local-repo-dir>:/var/src \
                      -ti hepfarm4ecal-edu /bin/bash

This command will leave you with containerizes interactive prompt for running
the software. Note: if you are under SELinux, there might be need to append
`--volume ...:/var/src` with `:z` postfix to allow editing.

Additionally, one may provide container with X11 display sockets by supplying
the above invokation with
`--volume /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY`. It is
essential part if you would like to run any GUI application shipped within a
container (ROOT, Geant4 Qt manager and so on).

Within the container your command-line prompt should look like
`collector@<hex-hash> ~ $`. Build the software with:

    $ cd p348-daq
    $ git apply ../ecal-edu/utils/p348-daq.patch
    $ ./build.sh
    $ cd ../ecal-edu
    $ make

Keep the command `docker run ...` around (as a memo, note or a shell-script)
to run during your development routines.

# Running Monitored processing

The `pipe` util (located at `bin/pipe`) supports rudimentary remote monitoring
procedure. To get representative excerpt over current processing, get the
container's IP with e.g.:

    (container) $ ifconfig eth0

the IP address of running container will appear after `inet` word. Then
provide `pipe` invokation with `-D@5723` key, e.g.:

    (container) $ bin/pipe -D@5723 -c presets/common.yaml /data/cdr01002-002551.dat -M ../p348-daq/maps/

One may run monitoring script on host with (say, container is running on
IP `172.17.0.2`:

    $ bin/monitor.py tcp://172.17.0.2:5723

This will occupy current terminal session with periodically-refreshing excerpt
on ongoing processing (number of discriminated events, elapsed time per handler,
etc.).

# Runnning remote build

For shell snippet of running container for remote connections,
see `utils/ecal-edu.build.run-remote-srv.sh.example`. It gives a gist of the
container setup compatible with modern IDE.

