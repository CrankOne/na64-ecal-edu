#include "EcalCalibViewer.h"
#include <cassert>

int main (int argc, char * argv[])
{
    // initialization of the structure with parameters
    EcalCalibViewer evc; 
    // command line interface call
    int rc = evc.appCfg.set_app_config( argc, argv);
    // error occured during initial application configuration
    if( rc < 0 ) {  
        evc.appCfg.usage_info( argv[0], std::cerr);
        return EXIT_FAILURE;
    }
    if( rc > 0 ) {return EXIT_SUCCESS;}
    
    //ploting histogram in PDF
    evc.process_view_calib();

    return 0;   
}


                                
