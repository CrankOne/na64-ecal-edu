def fac(v):
    if v > 1:
        return v*fac(v - 1)
    else:
        return 1

print(fac(2))
