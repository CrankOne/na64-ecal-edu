#include <iostream>
#include <cmath>
#include <fstream>
#include <cstdlib>

// Go to proper directory
void open_dir(char* TDirName
              , char* TSubDirName) {
    TDirectory::Cd(TDirName);
    if (!gDirectory) {
      std::cerr << "Error2";
    }
    TDirectory::Cd(TSubDirName);
    if (!gDirectory) {
      std::cerr << "Error2";
    }
}

// Exit from past directories
void close_dir() {
    TDirectory::Cd("../");
    if (!gDirectory) {
    std::cerr << "Error2";
    }
    TDirectory::Cd("../");
    if (!gDirectory) {
    std::cerr << "Error2";
    }
}

// Generate name of TH1F
void name_gen(char* hist_Name
              , char* TDirName) {
    // %s_%s - to compose to strings (TDirName and "2_3_1"
    // and to put "_" between them
    sprintf (hist_Name, "%s_%s", TDirName, "2_3_1");
}

// Generate the parameter distribution according to root histogram
// by Neiman's method
void gen_value (int * bins_Number
               , int * max_Value
               , char* hist_Name
               , TH1F* hist
               , std::ofstream & ofs) {
    float r1; // The genering parameter value
    int r2; // The genering parameter frequency value
    int bin_Value; // Parameter's frequency from histogram
    int i = 0;
    float bin_Width; // Variable we need for considering the scale
    float lower_Edge; // Subtracting value for getting a correct value 
                      // of parameter, without an offset
    
    // Getting the number of bins along X axis
    *bins_Number = hist->GetNbinsX();
    
    // Getting the maximum of histogram
    *max_Value = hist->GetBinContent(hist->GetMaximumBin());
    
    // Getting the lower edge of distribution
    lower_Edge = hist->GetBinLowEdge(1);
        
    // Process of generation and filtration of parameter
    while (i <= 10000) {
        r1 = (float)((rand()) % (*bins_Number) + 1);
        r2 = (rand()) % (*max_Value) +1;
        bin_Value = hist->GetBinContent(r1);
        bin_Width = hist->GetBinWidth(1); 
    
        if (r2 <= bin_Value) {
            ofs << r1*bin_Width + lower_Edge << std::endl;
            i++;
        }
    }
}

// Generate time between two events
double timegen(double lambda)  {
    //srand (time(NULL)); // Setting another Seed
    double step = 450;
    double e = M_E; // The e number
    //double lambda = 1000000;
    double lambdaL = lambda; // The collateral variable
    double par = pow(e,step);
    double k = 0.;  // The returnable number
    double p = 1.;
    double time;    
        
    // Generating a poisson's random number with average lambda
    // (k-1) is the distributed number
    do {
        k = k + 1;
        double u = (double)(rand())/RAND_MAX; // Generate random (double)number in range from 0 to 1
        p=p*u;
            
        while ((p < 1) && (lambdaL > 0)) {
            if (lambdaL > step) {
            p = p*par;
            lambdaL = lambdaL - step;
            }
                
            else {
                p = p*pow(e,lambdaL);
                lambdaL = 0;
            }
        }
    }
    while (p > 1.);
    
    // Calculating time
    time = 1./(k-1);
    
    return time;
} 

void par_gen() {   
    char TFileName [] = "../ecalOnly.root";
    char TDir1Name [] = "PmtFitMoyal_area";
    char TDir2Name [] = "PmtFitMoyal_max";
    char TDir3Name [] = "PmtFitMoyal_width";
    char TSubDirName [] = "main";
    int bins_Number;
    float bin_Value;
    int max_Value;
    char hist_Name [30];
    
    // Files for histogram.plot gnuplot script
    char HistFile1 [] = "../../../mydata/Poisson-generator/Hist1.txt";
    char HistFile2 [] = "../../../mydata/Poisson-generator/Hist2.txt";
    char HistFile3 [] = "../../../mydata/Poisson-generator/Hist3.txt";
    
    // Open TFile
    TFile* file = TFile::Open(TFileName);
    if (!file) {
    std::cerr << "Error1";
    }
    
    // Generating the Moyal Area parameter distribution
    std::ofstream ofs;
    ofs.open(HistFile1);
    open_dir(TDir1Name, TSubDirName);
    name_gen(hist_Name, TDir1Name);
    // Create an instance of TH1F
    // If the an instance wasn't created outputting the error message
    TH1F* hist1 = (TH1F*)gDirectory->Get(hist_Name);
    if (!hist1) {
    std::cerr << "Error3";
    }
    gen_value(&bins_Number, &max_Value, hist_Name, hist1, ofs);
    close_dir();
    ofs.close();
    
    // Generating the Moyal Maximum Position parameter distribution
    ofs.open(HistFile2);
    open_dir(TDir2Name, TSubDirName);
    name_gen(hist_Name, TDir2Name);
    TH1F* hist2 = (TH1F*)gDirectory->Get(hist_Name);
    if (!hist2) {
    std::cerr << "Error3";
    }
    gen_value(&bins_Number, &max_Value, hist_Name, hist2, ofs);
    close_dir();
    ofs.close();

    // Generating the Moyal Width parameter distribution
    ofs.open(HistFile3);
    open_dir(TDir3Name, TSubDirName);
    name_gen(hist_Name, TDir3Name);
    TH1F* hist3 = (TH1F*)gDirectory->Get(hist_Name);
    if (!hist3) {
    std::cerr << "Error3";
    }
    gen_value(&bins_Number, &max_Value, hist_Name, hist3, ofs);
    close_dir();
    ofs.close();
}
