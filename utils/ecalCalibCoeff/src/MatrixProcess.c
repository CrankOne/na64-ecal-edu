#include "MatrixProcess.h"

// Get inverse matrix from output matrix 
// and recoding inverse matrix in output matrix
float invert_matrix( gsl_matrix* InMatrix
                   , gsl_matrix* OutMatrix){
    // initialization of a temporary variable to store a permutation sign
    int sTemp = 0;
    //
    float det = 0;
    // memory allocation for permutations on col elements
    gsl_permutation* p = gsl_permutation_alloc(InMatrix->size1);
    // creating a matrix for storing LU decomposition
    gsl_matrix* LUdecomp = gsl_matrix_alloc (InMatrix->size1, InMatrix->size2);
    // copying the matrix to be decomposed
    gsl_matrix_memcpy (LUdecomp, InMatrix);
    // get LU decomposition of this matrix
    gsl_linalg_LU_decomp(LUdecomp, p, &sTemp);
    //
    det = gsl_linalg_LU_det(LUdecomp, sTemp);
    // get inverse of LU decomposition
    gsl_linalg_LU_invert(LUdecomp, p, OutMatrix);
    
    // delete GSL object
    gsl_permutation_free(p);
    gsl_matrix_free(LUdecomp);
    return det;     
}

// Fill this matrix with random from 0 to RandMax
void fill_random_matrix(gsl_matrix* InMatrix, int RandMax){
    int i, j;
    // temporary variable for storing a random value
    double random_value;
    // generating a random variable and writing 
    // it to the matrix element one at a time 
    for (i = 0; i < InMatrix->size1; ++i) {
        for (j = 0; j < InMatrix->size2; ++j) {
            // generate a random value
            random_value = rand() % RandMax;
            // set entry at i, j to random_value
            gsl_matrix_set(InMatrix, i, j, random_value);
        }
    }  
}

// Get pseudoinverse matrix 
float get_pseudoinv_matrix ( gsl_matrix* OriginalMatrix
                           , gsl_matrix* PseudoInverseMatrix){
    // space initialization for multiplication matrix
    gsl_matrix* SquMatrix = gsl_matrix_alloc (OriginalMatrix->size2
                                             , OriginalMatrix->size2);
    // Get square matrix from original matrix
    // and its transpose matrix  
    gsl_blas_dgemm (CblasTrans, CblasNoTrans
                   , 1.0, OriginalMatrix, OriginalMatrix
                   , 0.0, SquMatrix);
    // space initialization for inverse matrix
    gsl_matrix* InverseMatrix = gsl_matrix_alloc (OriginalMatrix->size2
                                                 , OriginalMatrix->size2);
    // Get inverse matrix
    int detPIM = 0; 
    detPIM = invert_matrix( SquMatrix, InverseMatrix);
    if (detPIM == 0){
        return 0;
    } else {
        // Get pseudoinverse matrix
        gsl_blas_dgemm (CblasNoTrans, CblasTrans
                   , 1.0, InverseMatrix, OriginalMatrix
                   , 0.0, PseudoInverseMatrix);
        return detPIM;
    }    
}

// sum all element matrix 
float sum_gsl_matrix_element(gsl_matrix* CheckMatrix){
    // variables for the loop for
    int i, j;
    // temporary variable for storing the sum of matrix elements
    float temp = 0;
    // temporary variable for storing the size of the main diagonal
    float temp2 = 0;
    // writing a matrix element to temporary variable
    for (i = 0; i < CheckMatrix->size1; i++){
        for (j = 0; j< CheckMatrix->size2; j++){
            temp += gsl_matrix_get (CheckMatrix, i, j);
        }
    }
    // finding the size of the main diagonal
    if (CheckMatrix->size1 <= CheckMatrix->size2){
        temp2 = CheckMatrix->size1;
    } else {
        temp2 = CheckMatrix->size2;
    }
    // returning the sum of the elements of the 
    // matrix divided by the size of the main diagonal
    return (temp /= temp2);    
}

// solving the matrix equation for square matrices
void gsl_solover_matrix_eq ( gsl_matrix* OriginalMatrix
                           , gsl_vector* EnergTotl 
                           , gsl_vector* CoeffS){    
    // initialization of a temporary variable to store a permutation sign
    int sTemp = 0;
    // memory allocation for permutations on col elements
    gsl_permutation* p = gsl_permutation_alloc(OriginalMatrix->size1);
    // creating a matrix for storing LU decomposition
    gsl_matrix* LUdecomp = gsl_matrix_alloc ( OriginalMatrix->size1
                                            , OriginalMatrix->size2);
    // copying the matrix to be decomposed
    gsl_matrix_memcpy (LUdecomp, OriginalMatrix);
    // get LU decomposition of this matrix
    gsl_linalg_LU_decomp(LUdecomp, p, &sTemp);
    // get inverse of LU decomposition
    gsl_linalg_LU_solve(LUdecomp, p, EnergTotl, CoeffS);
    
    // delete GSL object
    gsl_permutation_free(p);
    gsl_matrix_free(LUdecomp);    
}
