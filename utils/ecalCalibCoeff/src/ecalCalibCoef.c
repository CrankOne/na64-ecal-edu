#include "ecalCalibCoef.h"

// vector filling with energies
void fill_vector_energ (gsl_vector* EnergTotl){
    // filling elements vector
    int i;
    for(i = 0; i < EnergTotl->size; i++ ){
        gsl_vector_set (EnergTotl, i, 80);
    }
}

// finds the mean value of the gsl vector
float mean_gsl_vector(gsl_vector* Coeff){
    // variables for the loop for
    int i;
    float temp = 0;
    for (i = 0; i < Coeff->size; ++i) {
           temp += gsl_vector_get(Coeff, i);
    }
    temp /= Coeff->size;
    return temp;
}

// Print matrix in terminal
void print_matrix(gsl_matrix* InMatrix){
    int i, j;
    // console by element printing
    for (i = 0; i < InMatrix->size1; ++i) {
         for (j = 0; j < InMatrix->size2; ++j) {
            // output of the matrix element to the console
            printf("%10.2e", gsl_matrix_get(InMatrix, i, j));
        }
        printf("\n");
    }
}

// Return runtime 
float print_time(unsigned int start_time){
    // assignment to a variable of the actual cycle of the processor
    unsigned int end_time = clock(); 
    // returning the difference in the number of cycle when starting
    // the startup program and the actual cycle divided by the time
    // of one measure
    return ( end_time - start_time )/CLOCKS_PER_SEC;
}

// print GSL vector 
void print_gsl_vector(gsl_vector* Coeff){
    // variables for the loop for
    int i;
    // variables for cell position
    int x = 0, y = 0, p = 0;
    // creation of a 12 by 6 table from a linear array
    for (i = 0; i < Coeff->size ; ++i) {
        x += 1;
        // end of a row of 6 cells
        if( (i % 6) == 0  && (i != 0) ){y += 1; x = 0; printf("\n");}
        // end of a part ECAL of 36 cells
        if( (i % 36) == 0  && (i != 0) ){p += 1; y = 0;}
        // beginning of a new series
        if ( i == 0 ){x = 0;}
        // cell position output
        printf( "(%d_%d_%d) = ", x, y, p );
        // selection of negative value for printing
        if (gsl_vector_get(Coeff, i) < 0){
            printf( "%10d  ", 0);
        } else {
            printf( "%10.2e  ", gsl_vector_get(Coeff, i) );  
        } 
    }
    printf("\n");
}

// sum all element matrix 
float chi_sq_gsl_matrix_vector(gsl_vector* EnergTotl, gsl_vector* Coeff, gsl_matrix* OriginalMatrix){
    // variables for the loop for
    int i;
    // temporary variable 
    float SumEl = 0;
    gsl_vector* EnergTotlAprx = gsl_vector_alloc (EnergTotl->size);
    gsl_blas_dgemv( CblasNoTrans, 1, OriginalMatrix 
                  , Coeff, 0, EnergTotlAprx );
    // writing a matrix element to temporary variable
    for (i = 0; i < EnergTotl->size; i++){
        float temp = 0;
        temp = gsl_vector_get(EnergTotl, i) - gsl_vector_get(EnergTotlAprx, i);
        SumEl += temp*temp;
        }
    SumEl /= EnergTotl->size;
    return sqrt(SumEl);    
}
