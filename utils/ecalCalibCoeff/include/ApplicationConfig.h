#pragma once
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

// structure with file name
struct DirParameter {
        // part to directory to file   
        char* InFileName;
        char* OutFileName;
};

// structure with logical flags
struct FlagProcess {
    bool PrintTableCoeff = false;
    bool PrintInfoEnerge = false;
    bool PrintInfoProcess = false;
    bool PrintCell = false;    
};

// cell position structure
struct PositionCell{
    int x = -1;
    int y = -1;
    int z = -1;
};

// structure with variables for the command line interface
struct ApplicationConfig 
{   
    // cell position
    PositionCell position;
    // Parameters of information about directory
    DirParameter DirParam;
    // logical flags responsible for data processing
    FlagProcess FlagProc;    
};

// Displays information about existing keys
void usage_info();

// set parametrs from command line interface
int set_app_config( ApplicationConfig* AppConf
                  , int argc, char * argv[]);  
