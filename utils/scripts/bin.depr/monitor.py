#!/usr/bin/python3
import sys
import zmq
from time import sleep

import capnp
capnp.remove_import_hook()
metainfo_capnp = capnp.load('../ecal-edu/presets/schemata/metainfo.capnp')

hostconn = sys.argv[1] #"tcp://172.17.0.2:5723"

# Socket to talk to server
context = zmq.Context()
sock = context.socket( zmq.SUB )

sock.connect( hostconn )

# Note: w.r.t. pub/sub pattern, the SHOULD be a classifying filter.
sock.setsockopt(zmq.SUBSCRIBE, b'epi:')

def print_pipe_stats(eps, header='localhost', stream=sys.stdout):
    stream.write( "\033[2J\033[1m%s\033[0m\n"%header )
    stream.write( "\033[7m  #            Processor name       Discriminated  Time frac \033[0m\n")
    timeFracSum = 0.
    for n, hs in enumerate(eps.handlers):
        timeFrac = hs.elapsedTime/eps.elapsedTime
        timeFracSum += timeFrac
        stream.write( '%3d \033[1m%25s\033[0m  (%8d / %5.2f%%)  %8.2e\n'%(
            n+1, hs.name,
            hs.eventsDiscriminated,
            100*float(hs.eventsDiscriminated)/(eps.eventsPassed or 1),
            timeFrac ) )
    stream.write( "%8d events read in %8.2e msec; rate: \033[1m%8.2e\033[0m evs/sec\n"%(
        eps.eventsPassed, eps.elapsedTime, 1e3*eps.eventsPassed/(eps.elapsedTime or 1) ) )
    stream.write( 'Perf loss due to monitoring: %.2e\n'%(1. - timeFracSum) )
    stream.write( 'Banks capacity:\n' )
    stream.write( '             SADC hits: %d\n'%eps.banks.nSADCHits )
    stream.write( '              APV hits: %d\n'%eps.banks.nAPVHits )
    stream.write( '  SADC fitting entries: %d\n'%eps.banks.nSADCFittingEntries )
    stream.write( '          APV clusters: %d\n'%eps.banks.nAPVClusters )
    stream.write( '          track points: %d\n'%eps.banks.nTrackPoints )
    stream.write( '                tracks: %d\n'%eps.banks.nTracks )


while True:
    identity, series = sock.recv_multipart()
    eps = metainfo_capnp.EventProcessingState.from_bytes(series)
    print_pipe_stats(eps, header=hostconn)

