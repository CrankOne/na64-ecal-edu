#include "gdml/sensDet.hh"

#include <gtest/gtest.h>

namespace na64dp {

#if 0
static const struct SensDetsExprExample {
    std::string expr;
    mc::SetSD::Assignment a;
} gAssignmentExamples[] = {
    // Valid examples
    { "/trigger/beamCounter", { "/trigger/beamCounter", "", "" } },
    { "/tracker/MuMega[0:#]", { "/tracker/MuMega", "0", "#" } },
    { "/calo/ECAL[2:1]", { "/calo/ECAL", "2", "1" } },
    // Invalid examples
    { "wrong@symbol", {"", "", ""} },
    { "/missed/bracket[0:#", {"", "", ""} },
    { "/wrong/index[2:foo]", {"", "", ""} },
    { "", {"", "", ""} }
};

// Test basic matching
TEST(MCSDAssignment, correctParsing) {
    std::smatch m;
    for( const SensDetsExprExample * sample = gAssignmentExamples
       ; ! sample->expr.empty()
       ; ++sample ) {
        auto matchR = std::regex_match( sample->expr, m, mc::SetSD::rxSensitiveDetector );
        EXPECT_EQ( (bool) matchR, ! sample->a.sdPath.empty() );
        if( ! matchR ) continue;
        EXPECT_EQ( m[1], sample->a.sdPath );
        EXPECT_EQ( m[3], sample->a.indexPath );
        EXPECT_EQ( m[4], sample->a.indexNumber );
    }
}
#endif

}
