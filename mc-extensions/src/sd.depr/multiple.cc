#include "sd/multiple.hh"

#include <G4SDManager.hh>

namespace na64dp {
namespace mc {

MultipleSensitiveDetector::MultipleSensitiveDetector( const std::string & ownName
                                                    , const std::string & msgrPath )
        : SensitiveDetector(ownName)
        , geode::GenericG4Messenger(msgrPath) {
}

void
MultipleSensitiveDetector::Initialize(G4HCofThisEvent* hcof) {
    for( auto sdPtr : *static_cast<std::vector<SensitiveDetector *> *>(this) ) {
        sdPtr->Initialize(hcof);
    }
}

void
MultipleSensitiveDetector::EndOfEvent(G4HCofThisEvent* hcof) {
    for( auto sdPtr : *static_cast<std::vector<SensitiveDetector *> *>(this) ) {
        sdPtr->EndOfEvent(hcof);
    }
}

G4bool
MultipleSensitiveDetector::ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist) {
    for( auto sdPtr : *static_cast<std::vector<SensitiveDetector *> *>(this) ) {
        sdPtr->ProcessHits(aStep, ROhist);
    }
    return false;
}

void
MultipleSensitiveDetector::ui_cmd_add_sd( geode::GenericG4Messenger* msgr_
                                        , const G4String & strVal ) {
    MultipleSensitiveDetector & msgr = *static_cast<MultipleSensitiveDetector*>(msgr_);
    // Get the instance from global registry
    auto sdPtr_ = G4SDManager::GetSDMpointer()->FindSensitiveDetector( strVal );
    if( ! sdPtr_ ) {
        // TODO: is this redundant? The FindSensitiveDetector seems to
        // throw its own exception
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Unable to add non-existing SD \"%s\" to multiple SD \"%s\"."
                , strVal.c_str()
                , msgr.GetName().c_str()
                );
        G4Exception( __FUNCTION__, "NA64SW011", JustWarning, errbf);
        return;
    }
    SensitiveDetector * sdPtr = dynamic_cast<SensitiveDetector*>(sdPtr_);
    if( ! sdPtr ) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Failed to downcast SD instance \"%s\" to NA64SW's SD class"
                  " to be added into \"%s\" multiple SD."
                , sdPtr_->GetName().c_str()
                , msgr.GetName().c_str()
                );
        G4Exception( __FUNCTION__, "NA64SW012", JustWarning, errbf);
        return;
    }
    // Add to a vector
    msgr.std::vector<SensitiveDetector *>::push_back(sdPtr);
}

}

REGISTER_SENSITIVE_DETECTOR( Multiple
                           , name, msgrPath, dsp, nfr
                           , "A set of sensitive detector instances"
                           ) {
    return new na64dp::mc::MultipleSensitiveDetector( name, msgrPath );
}

}

