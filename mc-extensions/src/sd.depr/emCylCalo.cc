#include "sd/emCylCalo.hh"

#include <TH1F.h>

#include <G4UnitsTable.hh>

namespace na64dp {
namespace mc {

EmCylCaloTestSD::Cell::Cell(Index idx) {
    char namebf[64];
    char descbf[128];
    int rhoIdx = idx.get(0)
      , phiIdx = idx.get(1)
      , zIdx = idx.get(2)
      ;
    snprintf( namebf, sizeof(namebf), "edep%dx%dx%d"
            , rhoIdx, phiIdx, zIdx );
    snprintf( descbf, sizeof(namebf), "Energy deposition at cell %dx%dx%d ; MeV"
            , rhoIdx, phiIdx, zIdx );
    edep = new TH1F( namebf, descbf
                   , 100, 0, 10 /*CLHEP::MeV*/
                   );
}

EmCylCaloTestSD::Cell::~Cell() {
}

void
EmCylCaloTestSD::process_hits_for( const Index_t & idx, G4Step * aStep ) {
    auto it = _scorers.find(idx);
    if( _scorers.end() == it ) {
        na64dp_KleinScorer_t scorer;
        na64dp_sum_klein_init(&scorer);
        it = _scorers.emplace( idx, scorer ).first;
    }
    G4double edep = aStep->GetTotalEnergyDeposit()*aStep->GetTrack()->GetWeight() / CLHEP::MeV;
    na64dp_sum_klein_add( &(it->second), edep );
    //Cell & cell = it->second;
    //cell.edep->Fill( aStep->GetTotalEnergyDeposit() / CLHEP::MeV
    //               , aStep->GetTrack()->GetWeight() );
}

void
EmCylCaloTestSD::EndOfEvent(G4HCofThisEvent * hcofe) {
    SegmentedSensitiveDetector::EndOfEvent(hcofe);
    double sum = 0;
    for( const auto & scorerEntry : _scorers ) {
        auto cellIt = _cells.find(scorerEntry.first);
        if( _cells.end() == cellIt ) {
            cellIt = _cells.emplace( scorerEntry.first
                    , Cell(scorerEntry.first) ).first;
        }
        Cell & cell = cellIt->second;
        double edepInCell = na64dp_sum_klein_result(&scorerEntry.second);
        sum += edepInCell;
        cell.edep->Fill( edepInCell );
    }
    log4cpp::Category::getInstance("na64mc.sd.EmCylCaloTestSD")
        << log4cpp::Priority::INFO
        << "E_dep in \"" << GetName() << "\" SD of type EmCylCaloTestSD: "
        << G4BestUnit( sum, "Energy" )
        << " overall, in " << _scorers.size()
        << " cells.";
    _scorers.clear();
}

}

REGISTER_SENSITIVE_DETECTOR( EmCylCaloTest
                           , name, msgrPath, dsp, nfr
                           , "Sensitive detector for cylindrical EM calorimeter testing fixture" ) {
    return new na64dp::mc::EmCylCaloTestSD( name );
}

}

