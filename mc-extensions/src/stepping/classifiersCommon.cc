#include "stepping/classifiersCommon.hh"

#include <G4Step.hh>
#include <G4ParticleTable.hh>
#include <G4VProcess.hh>

namespace na64dp {
namespace mc {

EncodedStepFeature
ByPDGCode::get_feature(const G4Step & aStep) {
    return { .code = (std::size_t) aStep.GetTrack()
        ->GetParticleDefinition()
        ->GetPDGEncoding()
        };
}

void
ByPDGCode::key_to_str( EncodedStepFeature id
                     , char * buf
                     , std::size_t bufLen
                     ) {
    G4ParticleDefinition * pDef = G4ParticleTable::GetParticleTable()->FindParticle(id.code);
    if( ! pDef ) {
        snprintf( buf, bufLen, "PDG%zu", id.code );
    } else {
        strncpy(buf, pDef->GetParticleName().c_str(), bufLen);
    }

}


EncodedStepFeature
ByCharge::get_feature(const G4Step & aStep) {
    return { .code = (std::size_t) aStep.GetTrack()
                                        ->GetParticleDefinition()
                                        ->GetPDGCharge() };
}

void
ByCharge::key_to_str( EncodedStepFeature id
                    , char * buf
                    , size_t bufLen
                    ) {
    G4ParticleDefinition * pDef = G4ParticleTable::GetParticleTable()->FindParticle(id.code);
    if( ! pDef ) {
        snprintf( buf, bufLen, "%zuq", id.code );
    } else {
        // todo: find a nicer solution
        snprintf( buf, bufLen, "%ee", pDef->GetPDGCharge() );
    }
}


EncodedStepFeature
ByProductionProcess::get_feature(const G4Step & aStep) {
    const G4VProcess * procPtr = aStep.GetTrack()
        ->GetCreatorProcess()
        ;
    if( !procPtr ) return {.pointer = nullptr};
    return EncodedStepFeature{.pointer = procPtr};
}

void
ByProductionProcess::key_to_str( EncodedStepFeature id
                    , char * buf
                    , size_t bufLen
                    ) {
    if( id.pointer ) {
        auto procPtr = reinterpret_cast<const G4VProcess *>(id.pointer);
        // Seems that process name isn't guaranteed to be unique...
        //strncpy(buf, procPtr->GetProcessName().c_str(), bufLen);
        snprintf(buf, bufLen, "%s_%d_%p"
                    , procPtr->GetProcessName().c_str()
                    , procPtr->GetProcessSubType()
                    , procPtr
                    );
    } else {
        strncpy(buf, "null", bufLen);
    }
}

}
}

REGISTER_MC_STEP_FEATURE( ParticleType, name, path
        , "A particle type MC step feature" ) {
    return new na64dp::mc::ByPDGCode();
}

REGISTER_MC_STEP_FEATURE( ParticleCharge, name, path
        , "A particle charge MC step feature" ) {
    return new na64dp::mc::ByCharge();
}

REGISTER_MC_STEP_FEATURE( CreatorProcess, name, path
        , "A particle creator process step feature" ) {
    return new na64dp::mc::ByProductionProcess();
}

