#include "stepping/handlers/dump.hh"

// TODO
#if 0
#include <G4Step.hh>

#include <log4cpp/Category.hh>

#include <fstream>

namespace na64dp {
namespace mc {

StepsDumpHandler::StepsDumpHandler( const size_t & flags
                                  , const std::string & filePath )
        : _flags(flags)
        , _os(filePath) {}

void
StepsDumpHandler::handle_step(const G4Step * aStep) {
    // get pre-steps/post-steps
    auto preSP = aStep->GetPreStepPoint()
       , postSP = aStep->GetPostStepPoint()
       ;
    // get material
    auto matPtr = preSP
        ->GetTouchableHandle()
        ->GetVolume()
        ->GetLogicalVolume()
        ->GetMaterial()
        ;

    if( (0x1 << kParticlePDGCode) & _flags ) {  // write the particle PDG code
        _os << std::setw(8)
            << aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding();
    }
    if( (0x1 << kTotalEnergy_MeV) & _flags ) {  // write the total energy deposit
        _os << std::setw(20) << postSP->GetTotalEnergy() / CLHEP::MeV;
    }
    if( (0x1 << kPreStepPoint_mm) & _flags ) {  // write the 3 column of step start
        _os << std::setw(20) << preSP->GetPosition().x() / CLHEP::mm
            << std::setw(20) << preSP->GetPosition().y() / CLHEP::mm
            << std::setw(20) << preSP->GetPosition().z() / CLHEP::mm;
    }
    if( (0x1 << kPostStepPoint_mm) & _flags ) {  // write the 3 column of step start
        _os << std::setw(20) << postSP->GetPosition().x() / CLHEP::mm
            << std::setw(20) << postSP->GetPosition().y() / CLHEP::mm
            << std::setw(20) << postSP->GetPosition().z() / CLHEP::mm;
    }
    if( (0x1 << kMomentumDir) & _flags ) {  // write the momentum direction
        G4ThreeVector m = preSP->GetMomentumDirection();
        _os << std::setw(20) << m.x()
            << std::setw(20) << m.y()
            << std::setw(20) << m.z();
    }
    if( (0x1 << kGlobalTime_nsec) & _flags ) {  // write the global time
        _os << std::setw(20) << aStep->GetTrack()->GetGlobalTime()  / CLHEP::nanosecond;
    }
    if( (0x1 << kMaterialName) & _flags ) {  // write the material of original volume
        _os << std::setw(30) << (matPtr ? matPtr->GetName() : "null")
            << std::endl;
    }
}

//
// Non-classifying dump
//////////////////////

StepsDump::StepsDump( const G4String & name
                    , const G4String & path )
        : geode::GenericG4Messenger(path)
        , _flags(StepsDumpHandler::kAllColumns)  // enable all by default
        , _destination(nullptr) {
    dir(name, "Commands for steps dump handler.")
        .cmd<G4String>( "open", "Open file.", "filename", "Name of the file to open", ui_cmd_open_file )
        .cmd_nopar( "close", "Close opened file.", ui_cmd_close_file, anyAppState )
    .end(name);
}

void
StepsDump::handle_step(const G4Step * aStep) {
    if( ! _destination ) return;  // do nothing for file not being opened
    _destination->handle_step(aStep);
};

void
StepsDump::ui_cmd_open_file( geode::GenericG4Messenger * msgr_
                           , const G4String & strExpr
                           ) {
    StepsDump & me = dynamic_cast<StepsDump&>(*msgr_);
    if( me._destination ) {
        // TODO: warn on closing previously opened file here
        delete me._destination;
    }
    me._destination = new StepsDumpHandler(me._flags, strExpr);
}

void
StepsDump::ui_cmd_close_file( geode::GenericG4Messenger * msgr_
                            , const G4String &
                            ) {
    StepsDump & me = dynamic_cast<StepsDump&>(*msgr_);
    if( me._destination ) {
        delete me._destination;
    }
    me._destination = nullptr;
}

//
// Classifying dump
//////////////////

ClassifyingStepsDump::ClassifyingStepsDump( const G4String & name
                                          , const G4String & path
                                          , StepClassifier & kc
                                          )
            : AbstractRuntimeStepDispatcher( kc )
            , GenericG4Messenger(path)
            , _flags(StepsDumpHandler::kAllColumns)  // enable all by default (todo: more elegant)
            {
    dir(name, "Commands related to eponymous classifying MC steps dump handler.")
        .cmd<G4String>( "setOutputFilePattern", "Sets the output filename"
                " pattern for classified files. Format placeholders must obey"
                " the formatting rules for NA64SW/MC strings."
                , "fnamePattern"
                , "Filename/path pattern."
                , ui_cmd_set_filename_pattern
                )
        .cmd_nopar( "closeAll", "Finalizes writing of the dump files, closes"
                " them and frees descriptors. Without this call the output"
                " may be incomplete."
                , ui_cmd_close_all
                , anyAppState )
        //.cmd<G4String>( "enableField" )
        //.cmd<G4String>( "disableField" )
        // ...
    .end(name);
}

iStepHandler *
ClassifyingStepsDump::new_handler( const StepHandlerKey & key
                                 , const G4Step * //aStep
                                 ) {
    std::string fileName = step_classifier()
                        .format_string( _filenamePat, key );
    log4cpp::Category::getInstance("na64mc.scoring.ClassifyingStepsDump")
        << log4cpp::Priority::INFO
        << "Dump instance \"" << fileName << "\" created.";
    return new StepsDumpHandler( _flags, fileName );
}

void
ClassifyingStepsDump::clear_destinations() {
    auto & self = *static_cast<std::unordered_map<
                    StepHandlerKey, iStepHandler *, StepFeaturesHash>*>(this);
    for(auto destPair : self) {
        delete destPair.second;
    }
    self.clear();
}

void
ClassifyingStepsDump::ui_cmd_set_filename_pattern(
        geode::GenericG4Messenger * msgr_,
        const G4String & strExpr ) {
    ClassifyingStepsDump & self = dynamic_cast<ClassifyingStepsDump&>(*msgr_);
    self._filenamePat = strExpr;
}

void
ClassifyingStepsDump::ui_cmd_close_all(
        geode::GenericG4Messenger * msgr_,
        const G4String & ) {
    ClassifyingStepsDump & self = dynamic_cast<ClassifyingStepsDump&>(*msgr_);
    self.clear_destinations();
}

}
}

REGISTER_MC_STEP_HANDLER( Dump, name, path, nfr,
        "Writes multicolumn ASCII file containing dump of steps"
        " in a form: [particlePDG:int] [total-energy:double]"
        " [position-start:double[3]] [position-end:double[3]]"
        " [momentum-direction:double[3]]"
        " [time-global:double]. Length in mm, time in ns, energy in MeV."
        ) {
    return new na64dp::mc::StepsDump(name, path);
}

REGISTER_CLASSIFYING_MC_STEP_HANDLER( ClassifyingDump, name, path, nfr, kc,
        "Writes multicolumn ASCII files sorted wrt some classifier. Files"
        " contain dump of steps"
        " in a form: [particlePDG:int] [total-energy:double]"
        " [position-start:double[3]] [position-end:double[3]]"
        " [momentum-direction:double[3]]"
        " [time-global:double]. Length in mm, time in ns, energy in MeV."
        ) {
    return new na64dp::mc::ClassifyingStepsDump( name, path, kc );
}

#endif
