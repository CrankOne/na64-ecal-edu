#pragma once

namespace na64dp {
namespace mc {

/**\brief A generic handler accumulating energy depositions sum
 *
 * This class is actually a stub made because of unclear superset of possible
 * use-cases.
 * The purpose is to gain clear understanding of the intersection between 
 * `SegmentedSensitiveDetector` and `SteppingAction` classes.
 *
 * \todo Probably will be substituted in a favor of more generic one.
 * */
class IntegralEDepStepHandler : public iStepHandler {
protected:
    numerical::KleinScorer fromThisEvent  ///< energy deposition from current event
                         , overall  ///< overall edenergy deposition sum
                         , sqOverall;  ///< sum of squared energy depositions per event
public:
    /// Retrives total energy deposition from the step and adds it to the sums
    virtual void handle_step(const G4String * aStep) override;
    /// ...
};

}
}

