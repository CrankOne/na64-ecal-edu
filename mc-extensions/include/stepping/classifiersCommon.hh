#pragma once

#include "na64mc/stepClassifier.hh"

/**\file
 * \brief Common step classifiers
 *
 * Contains a set of common MC step classifiers: PDG code, charge, etc.
 * */

namespace na64dp {
namespace mc {

///\brief An MC track classifier using the particle type feature
///
/// Stateless classifier extracting the PDG encoding from step particle
/// definition. Useful for categorizing particles by type.
class ByPDGCode : public StepClassifier::iStepFeatureExtractor {
public:
    /// Creates no UI commands
    ByPDGCode() {}
    /// Returns particle PDG code
    virtual EncodedStepFeature get_feature(const G4Step &) override;
    /// Writes particle name wrt PDG definition
    virtual void key_to_str(EncodedStepFeature, char *, std::size_t) override;
};

///\brief An MC track classifier by particle charge
///
/// Stateless classifier that uses particle charge as an ID.
/// Useful for aposteriori ionizing calculus.
///
/// \todo Charge is double, the value must be (signed) int
class ByCharge : public StepClassifier::iStepFeatureExtractor {
public:
    /// Returns particle PDG code
    virtual EncodedStepFeature get_feature(const G4Step &) override;
    /// Writes particle charge + "e" meaning the charge of electron as a unit
    virtual void key_to_str(EncodedStepFeature, char *, std::size_t) override;
};

///\brief An MC step classifier by the creator process
///
/// Stateless classifier that retrieves particle process ID/name from current
/// physics list.
///
/// \todo try to use process type + subtype identifiers instead of ptr as it
///     leaves non-reproducible file names
class ByProductionProcess : public StepClassifier::iStepFeatureExtractor {
public:
    /// ...
    virtual EncodedStepFeature get_feature(const G4Step &) override;
    /// ...
    virtual void key_to_str(EncodedStepFeature, char *, std::size_t) override;
};

// ...

}
}

