#pragma once

#include "na64mc/g4api/sensitiveDetector.hh"
#include "na64mc/geode/genericMessenger.hh"

namespace na64dp {
namespace mc {

/**\brief An extensible sensitive detector class
 *
 * This is a container for multiple sensitive detector instances that performs
 * dispatching of step information among multiple bound instances.
 * 
 * User code must use the default `vector<>` methods to add/delete/replace
 * the instances.
 *
 * \todo unused, unfinished, untested
 * */
class MultipleSensitiveDetector : public SensitiveDetector
                                , public std::vector<SensitiveDetector *>
                                , protected geode::GenericG4Messenger {
public:
    MultipleSensitiveDetector( const std::string & ownName
                             , const std::string & msgrPath );
    /// Dispatches invokation of eponymous method among all the associated
    /// instances
    virtual void Initialize(G4HCofThisEvent*) override;
    /// Dispatches invokation of eponymous method among all the associated
    /// instances
    virtual void EndOfEvent(G4HCofThisEvent*) override;
protected:
    /// Dispatches invokation of eponymous method among all the associated
    /// instances
    virtual G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist) override;
public:
    /// Adds existing sensitive detector to the composition
    static void ui_cmd_add_sd(geode::GenericG4Messenger*, const G4String &);
};



}
}

