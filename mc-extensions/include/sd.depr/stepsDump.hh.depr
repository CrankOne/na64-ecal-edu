#pragma once

#include "na64mc/g4api/sensitiveDetector.hh"
#include "na64mc/partsIndex.hh"

#include "na64util/numerical/sums.h"  // XXX

#include <G4THitsCollection.hh>

#include <typeinfo>
#include <typeindex>

namespace na64dp {
namespace mc {

/**\brief A composite sensitive detector class consisting of multiple objects
 *
 * This sensitive detector subtype provides caching routines for faster pick-up
 * of various sub-volumes within a complex hierarchy.
 * */
class StepsDump : public SensitiveDetector {
protected:
    na64dp_KleinScorer_t _fullScorer;
    size_t _nInvokations;
protected:
    virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory*) override;
public:
    StepsDump( const G4String & sdName )
             : SensitiveDetector(sdName)
             , _nInvokations(0) {
        na64dp_sum_klein_init(&_fullScorer);
    }

    //virtual void Initialize(G4HCofThisEvent *) override;
    virtual void EndOfEvent(G4HCofThisEvent *) override;
};

}
}
