#pragma once

#include "sd/segmented.hh"

#include "na64util/numerical/sums.h"  // for scorer
#include "na64detID/TBName.hh"

namespace na64dp {
namespace mc {

/**\brief An MC sensitive detector class for ECAL calorimeter
 *
 */
class ECALSD : public SegmentedSensitiveDetector
             , public util::Observable<nameutils::DetectorNaming>::iObserver {
public:
    // Base calorimeter hit class, accumulates deposited energy
    class CaloHit : public na64dp_KleinScorer_t {
    public:
        CaloHit() { na64dp_sum_klein_init(this); }
        CaloHit & operator+=(double val) {
            na64dp_sum_klein_add(this, val);
            return *this;
        }
        operator double () const { return na64dp_sum_klein_result(this); }
    };
private:
    /// ECAL ID wrt current naming, to be completed with cell #
    DetID _ecalID;
    /// By-cell summators cache
    std::unordered_map<Index_t, CaloHit> _hits;

    const nameutils::DetectorNaming * _namingPtr;  //XXX
    double _totalSum;
protected:
    /// Shall update naming cache
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

    /// Create a scorer or uses existing one to fill up the energy deposition
    virtual void process_hits_for( const Index_t & idx, G4Step * aStep ) override;
public:
    ECALSD( const G4String & sdName, calib::Dispatcher & );

    // Create the hits in event and drops the caches
    virtual void EndOfEvent(G4HCofThisEvent *) override;
};

}
}

