#pragma once

#include "na64mc/gdml/processor.hh"
#include "na64mc/genericMessenger.hh"
#include "na64mc/notifications.hh"

#include <regex>

class G4VSensitiveDetector;

namespace na64dp {

namespace calib {
class Dispatcher;
}

namespace mc {

/**\brief Associates Geant4 volume with sensitive detector instance from GDML
 *
 * Implements `iAuxInfoProcessor` interface to accomplish `<auxinfo/>` GDML
 * tag processing within the `DetectorConstruction` procedures. Instances of
 * sensitive detector must be created at the pre-init state.
 *
 * \todo get rid of "index number"
 * */
class SetSD : public iAuxInfoProcessor
            , public GenericG4Messenger {
private:
    /// Externally-managed calibration dispatcher instance to bound
    /// the new sensitive detectors with
    calib::Dispatcher & _calibDispatcherRef;
    /// MC stages notification reference
    Notifier & _mcNotifier;
public:
    SetSD( const G4String rootPath
         , calib::Dispatcher & calibDspRef
         , Notifier & nfr );
    /// Associates the `SensitiveDetector` instance with volume, called by
    /// `DetectorConstruction` at the GDML processing stage
    virtual void process_auxinfo( const G4GDMLAuxStructType & auxStruct
                                , G4LogicalVolume * lvPtr ) override;

public:
    static void ui_cmd_create_sd( GenericG4Messenger *, const G4String & );
};

}
}

