#include <unordered_map>
#include <vector>
#include <iostream>
#include <cassert>
#include <random>

struct Key : public std::vector<int> {
    Key(size_t l) : std::vector<int>(l, 0x0) {}

    bool operator==(const Key & k) const {
        assert( k.size() == size() );  // assume all key of same length
        auto it1 = begin(), it2 = k.begin();
        while( it1 != end() ) {
            if( *it1 != *it2 ) return false;
            ++it1;
            ++it2;
        };
        return true;
    }
};

struct Hash {
    size_t operator()(const Key & k) const {
        int i = 0;
        int h = 0x0;
        for( auto v : k ) {
            //v <<= (i++)%16;
            //if( i%2 ) {
            //    v <<= 1;
            //} else {
            //    v >>= 1;
            //}
            h ^= v;
        }
        return h;
    }
};

int
main(int argc, char * argv[]) {
    const size_t keyLength = 23
               , nSamples = 1e6;

    #if 1
    std::random_device rd;
    std::mt19937 gen(rd());
    // if particles decay once per second on average,
    // how much time, in seconds, until the next one?
    std::exponential_distribution<> d(1);
    #endif

    std::unordered_map<Key, int, Hash> m;
    Hash h;
    for(size_t n = 0; n < nSamples; ++n) {
        Key k(keyLength);
        for(size_t i = 0; i < keyLength; ++i) {
            #if 0
            k[i] = rand();
            #else
            k[i] = d(gen)*RAND_MAX;
            #endif
        }
        auto it = m.find(k);
        if(m.end() == it){
            it = m.emplace(k, 0).first;
        }
        assert(it != m.end());
        ++(it->second);
    }
}

// Performance results:
// $ g++ dynamicKey.cc -O3
// $ time ./a.out 
//real	0m0,536s
//user	0m0,472s
//sys	0m0,064s
// $ g++ virtualHash.cc -g -ggdb
// $ time ./a.out 
//
//real	0m1,298s
//user	0m1,248s
//sys	0m0,048s
