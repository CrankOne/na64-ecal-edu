#include "tracking/trackFindSimple.hh"

#ifdef GenFit_FOUND

// Track representatives
#include <AbsTrackRep.h>
#include <RKTrackRep.h>
#include <MeasuredStateOnPlane.h>

namespace na64dp {
namespace handlers {

TrackFindSimple::TrackFindSimple( calib::Dispatcher & cdsp
								, const std::string & only
								, ObjPool<Track> & bank
								, double minResidual
								, int minCandSize
								, double momentum )
		: AbstractHitHandler<TrackPoint>(cdsp, only)
		, _tracksBank(bank)
		, _minResidual(minResidual)
		, _minCandSize(minCandSize)
		, _momentum(momentum) {}

bool
TrackFindSimple::process_hit( EventID
					        , DetID_t did
                            , TrackPoint & tp ) {
	
	//Create vector of all TrackPoints in event sorted by their placement
	DPlacement placement = tp.gR[2];
	
	auto idxIt = _trackPointsMap.find( placement );
	if( _trackPointsMap.end() == idxIt ) {
		auto ir = _trackPointsMap.emplace( placement, Hits() );
		assert( ir.second );
		idxIt = ir.first;
	}
		
	Hits & tps = idxIt->second;
	
	tps.emplace( did
	           , _current_event().trackPoints.pool().get_ref_of(tp) );
							
	return true;
}

void
TrackFindSimple::_sort_hits_by_planes( TrPoints & trackPoints ) {
	
	// Get all points in vector for each plane
	for ( auto it : _trackPointsMap ) {
		std::vector<PoolRef<TrackPoint>> temp;
		
		for ( auto ir : it.second ) {
			temp.push_back( ir.second );
		}
		trackPoints.push_back( temp );
		temp.clear();
	}
}

void
TrackFindSimple::_create_seed( TrPoints & trackPoints
				             , Seed & seeds ) {
	
	// Check points in upstream part of setup and define initial
	// angular hypothesis
	for ( auto & it : trackPoints ) {
		for ( auto & ir : it ) {
			if ( (*ir).gR[2] < -1000 ) {
				
				TVector3 vec( (*ir).gR[0]
				            , (*ir).gR[1]
				            , (*ir).gR[2] );
				
				seeds.push_back( vec );
			}
		}
	}
}

bool
TrackFindSimple::_process_seed( TVector3 & seed
					          , const TrPoints & trackPoints
					          , int PDG
					          , double eDep ) {			
		
	// First point is beamoutlet position	
	double x1(0), y1(0), z1(-2100);
	double x2(seed.X()), y2(seed.Y()), z2(seed.Z());
		
	// create vectors and calculate incline between trackpoints
	double x(0), y(0), z(0), tanX(0), tanY(0);
		
	x = ( x2 - x1 );
	y = ( y2 - y1 );
	z = ( z2 - z1 );
		
	tanX = atan2(x, z);
	tanY = atan2(y, z);
		
	// Start of Genfit routine
		
	// True initial position. Associate it with a beamoutlet
	TVector3 pos(x1, y1, z1);
	TVector3 mom(0, 0, 1);
		
	//mom.RotateX( tanX );
	//mom.RotateY( tanY );
	
	// Ecal + Hcal energy deposition
	mom.SetMag(eDep);

	// approximate covariance
	TMatrixDSym covM(6);
	double resolution = 0.1;
	for (int i = 0; i < 3; ++i) {
		covM(i,i) = resolution*resolution;
	}
	for (int i = 3; i < 6; ++i) {
		covM(i,i) = pow(resolution / 10 / sqrt(3), 2);
	}
		
	// Start from electron hypothesis. Maybe from ecal/hcal data
	genfit::AbsTrackRep * rep = new genfit::RKTrackRep(PDG);

	// start state
	genfit::MeasuredStateOnPlane state(rep);
	rep->setPosMomCov(state, pos, mom, covM);
	
	// Temporary vectors
	TVectorD curPos;
	TMatrixDSym curCov;
	
	rep->get6DStateCov(state, curPos, curCov);
	
	/* Iterate over all planes and hits, try to find hits with residual
	 * lower than certain cut. TODO: define cut!
	 */
	
	std::vector<PoolRef<TrackPoint>> tps;
	
	for ( unsigned int i = 0; i < trackPoints.size(); ++i ) {
		
		PoolRef<TrackPoint> smallRef;
		
		double smResX(1000), smResY(1000);
		
		for ( unsigned int j = 0; j < trackPoints[i].size(); ++j ) {

			rep->extrapolateBy(state, std::fabs( (*trackPoints[i][j]).gR[2] - curPos[2] ));
			rep->get6DStateCov(state, curPos, curCov);
			
			double cX((*trackPoints[i][j]).gR[0]);
			double cY((*trackPoints[i][j]).gR[1]);
			double cZ((*trackPoints[i][j]).gR[2]);
				
			double resX(0), resY(0), resZ(0);
				
			TVector3 curPoint(cX, cY, cZ);
				
			// get "residuals"
			resX = curPos[0]-curPoint[0];
			resY = curPos[1]-curPoint[1];
			resZ = curPos[2]-curPoint[2];
			
			if ( std::fabs(resX) < smResX && std::fabs(resY) < smResY) {
				smResX = resX;
				smResY = resY;
				smallRef = trackPoints[i][j];
			}
		}
		
		if ( std::fabs(smResX) < _minResidual 
		  && std::fabs(smResY) < _minResidual ) {
			tps.push_back(smallRef);
		}
	}
	
	if ( tps.size() > _minCandSize ) {

		TrackCand newCand;
		newCand.seed = seed;
		newCand.mom = mom;
		newCand.pdg = PDG;
		newCand.points = tps;
		
		_trackCands.push_back( newCand );
	}
	
	tps.clear();
	
	delete rep;
	rep = nullptr;
	
	return true;
}

TrackFindSimple::ProcRes
TrackFindSimple::process_event(Event * evPtr) {
    
    AbstractHitHandler<TrackPoint>::process_event( evPtr );
    
    TrPoints trackPoints;
	
	Seed seeds;
    	
	_sort_hits_by_planes( trackPoints );
	
	_create_seed( trackPoints, seeds);
		
	// Process track hypothesis with seed
	for ( auto seed : seeds ) {
		_process_seed( seed, trackPoints, -11,  _momentum );
		_process_seed( seed, trackPoints, -13,  _momentum );
		_process_seed( seed, trackPoints, -321,  _momentum );
		_process_seed( seed, trackPoints, -211,  _momentum );
	}
		
	// Create track candidates
	for ( auto & trackCand : _trackCands ) {
		PoolRef<Track> trackRef = _tracksBank.create();
		trackRef->seed[0] = trackCand.seed.X();
		trackRef->seed[1] = trackCand.seed.Y();
		trackRef->seed[2] = trackCand.seed.Z();
		trackRef->mom[0] = trackCand.mom.X();
		trackRef->mom[1] = trackCand.mom.Y();
		trackRef->mom[2] = trackCand.mom.Z();
		trackRef->pdg = trackCand.pdg;
		trackRef->momentum = _momentum;
		trackRef->trackPoints = trackCand.points;
		trackRef->genfitTrackRepr = nullptr;
		EvFieldTraits<Track>::map(*evPtr).emplace(EvFieldTraits<Track>::map(*evPtr).size()
												 , trackRef);
	}

	seeds.clear();
	trackPoints.clear();
	_trackCands.clear();
	_trackPointsMap.clear();

    return kOk;
}

REGISTER_HANDLER( TrackFindSimple, banks, ch, cfg
                , "Simple track following pattern recognition"
                  " algorithm based on particle momentum hypothesis" ) {
    
    return new TrackFindSimple( ch
							  , aux::retrieve_det_selection(cfg)
							  , banks.of<Track>()
                              , cfg["minResidual"].as<double>()
                              , cfg["minTrackSize"].as<int>()
                              , cfg["momentum"].as<double>() );
}
}
}
#endif  // defined(GenFit_FOUND)
