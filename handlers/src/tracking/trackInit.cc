#include "tracking/trackInit.hh"

#ifdef GenFit_FOUND

#include <EventDisplay.h>

// Geometry, material and magnetic field headers
#include "ConstFieldBox.h" // TODO: include ConstFieldBox in main-pipe
#include "ConstFieldBox.cc"
#include <FieldManager.h>
#include <MaterialEffects.h>
#include <TGeoMaterialInterface.h>
#include <TGeoManager.h>

namespace na64dp {
namespace handlers {

TrackInit::TrackInit( calib::Dispatcher & cdsp
                    , const std::string & only
                    , bool eventDisplay
                    , bool matInteraction
                    , const std::string & geoFilePath
                    , double magFieldUp
                    , double magFieldDown
                    , double magValue)
        : AbstractHandler()
        , _eventDisplay(eventDisplay)
        , _matInteraction(matInteraction)
        , _magFieldUp(magFieldUp)
        , _magFieldDown(magFieldDown)
        , _magValue(magValue) {
            
    // Include root geometry and its interfaces
    new TGeoManager( "Geometry", "NA64 geometry");

    TGeoManager::Import( geoFilePath.c_str() );
    log().debug( "Geometry read from file \"%s\" into instance %p."
               , geoFilePath.c_str()
               , gGeoManager );
    assert(gGeoManager);
    
    // Create simple magnetic field. TODO: use inhomogenous field
    genfit::ConstFieldBox * magBox = 
			new genfit::ConstFieldBox( 0., magValue, 0.
			                         , -500., 500.
			                         , -500., 500.
			                         , magFieldUp, magFieldDown
			                         );
    
    genfit::FieldManager::getInstance()->init(magBox);
    
    
    // Set if there is interaction with materials defined in root geo
    genfit::TGeoMaterialInterface * matInter = 
			new genfit::TGeoMaterialInterface();
    
    genfit::MaterialEffects::getInstance()->init(matInter);
    
    if ( !_matInteraction ) {
		genfit::MaterialEffects::getInstance()->setNoEffects();
	}
    
	if ( _eventDisplay ) {
        genfit::EventDisplay::getInstance();
    }
}

TrackInit::ProcRes
TrackInit::process_event(Event * evPtr) {
	return kOk;
}

REGISTER_HANDLER( TrackInit, banks, ch, cfg
                , "Handler to set magnetic field, material interaction and"
                  " initiate tracking routine" ) {
    
    return new TrackInit( ch
                        , aux::retrieve_det_selection(cfg)
                        , cfg["display"] ? cfg["display"].as<bool>() : false
                        , cfg["matInter"] ? cfg["matInter"].as<bool>() : false
                        , cfg["geometry"].as<std::string>()
                        , cfg["magnetPos"][0].as<double>()
						, cfg["magnetPos"][1].as<double>()
						, cfg["fieldValue"].as<double>()
                        );
}
}
}
#endif  // defined(GenFit_FOUND)
