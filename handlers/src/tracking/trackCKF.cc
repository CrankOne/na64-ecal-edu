#include "tracking/trackCKF.hh"

#ifdef GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>
#include <AbsTrackRep.h>
#include <RKTrackRep.h>

#include <AbsFitterInfo.h>
#include <AbsKalmanFitter.h>
#include <KalmanFitter.h>
#include <KalmanFitterInfo.h>
#include <KalmanFittedStateOnPlane.h>
#include <KalmanFitterRefTrack.h>
#include <DAF.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <StateOnPlane.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>

#include "ConstFieldBox.h" // both have to included in somewhere in root of na64 analysis tool
#include <FieldManager.h>
#include <MaterialEffects.h>
#include <TGeoMaterialInterface.h>
#include <TGeoManager.h>

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH1D.h>
#include <TH2D.h>

#include <EventDisplay.h>


namespace na64dp {
namespace handlers {

TrackCKF::TrackCKF( calib::Dispatcher & cdsp
                  , const std::string & only
                  , ObjPool<Track> & bank
                  , const std::string & geoFilePath )
		: AbstractHitHandler<TrackPoint>(cdsp, only)
		, TDirAdapter(cdsp)
		, _tracksBank(bank) {
			
    // init geometry and mag. field
    new TGeoManager( "Geometry", "NA64 geometry");

    TGeoManager::Import( geoFilePath.c_str() );
    // ^^^ TODO: how to check whether the geometry was, indeed, imported?
    log().debug( "Geometry read from file \"%s\" into instance %p.", geoFilePath.c_str(), gGeoManager );
    assert(gGeoManager);
    
    //Init_Geometry_2017
    genfit::FieldManager::getInstance()->init(new genfit::ConstFieldBox( 0., 18.0, 0.0, -500, 500, -500, 500, -1796.0, -1366.0));
    //genfit::FieldManager::getInstance()->init(new genfit::ConstFieldBox( 0., 17.2, 0.0, -500, 500, -500, 500, -1746.6, -1346.6));  
    
    genfit::MaterialEffects::getInstance()->init( new genfit::TGeoMaterialInterface() );
    //genfit::MaterialEffects::getInstance()->setNoEffects();
    
    hMom = new TH1D("hResMom" ,"momRes",60, 0, 150);
    
    hMomVsECal = new TH2D("MomentumEcal" ,"momVsECal", 100, 0, 150, 100, 0, 150);
    
    hMomVsChi = new TH1D("Chi/Ndf" ,"chiSq2", 100, 0, 10);
    
    //genfit::EventDisplay::getInstance();
    
}

TrackCKF::~TrackCKF() {
	for(auto p : _histograms) {
		delete p.second;
    }
}

bool
TrackCKF::process_hit( EventID
					 , DetID_t did
                     , TrackPoint & tp ) {
	
	DPlace place = tp.gR[2];
	
	auto idxIt = _tpMap.find( place );
	if( _tpMap.end() == idxIt ) {
		auto ir = _tpMap.emplace( place, Hits() );
		assert( ir.second );
		idxIt = ir.first;
	}
		
	Hits & tps = idxIt->second;
	
	tps.emplace( did
	           , _current_event().trackPoints.pool().get_ref_of(tp) );
							
	return true;
}

void
TrackCKF::_create_seed( TrPoints & trackPoints
				      , Seed & seeds ) {
	
	for ( auto & it : trackPoints ) {
		for ( auto & ir : it ) {
			if ( (*ir).gR[2] < -1000 ) {
				
				TVector3 vec( (*ir).gR[0]
				            , (*ir).gR[1]
				            , (*ir).gR[2] );
				
				seeds.push_back( vec );
			}
		}
	}
}

void
TrackCKF::_sort_hits_by_planes( TrPoints & trackPoints ) {
	
	// Get all points in vector for each plane
	for ( auto it : _tpMap ) {
		std::vector<PoolRef<TrackPoint>> temp;
		
		for ( auto ir : it.second ) {
			temp.push_back( ir.second );
		}
		trackPoints.push_back( temp );
		temp.clear();
	}
}

bool
TrackCKF::_process_seed( TVector3 & seed
					   , const TrPoints & trackPoints
					   , int PDG
					   , double eDep ) {
	
	if ( eDep < 10 || eDep > 200 ) {
		return false;
	}
				
	#if 0
	std::cout << "===== NEW TRACK CANDIDATE =====" << std::endl;
	std::cout << "Seed = x: " << seed.X() << ", "
	          << "y: " << seed.Y() << ", "
	          << "z: " << seed.Z() << std::endl;

	#endif
	
	// First point is beamoutlet position	
	double x1(0), y1(0), z1(-2100);
	double x2(seed.X()), y2(seed.Y()), z2(seed.Z());
		
	// create vectors and calculate incline between trackpoints
	double x(0), y(0), z(0), tanX(0), tanY(0);
		
	x = ( x2 - x1 );
	y = ( y2 - y1 );
	z = ( z2 - z1 );
		
	tanX = atan2(x, z);
	tanY = atan2(y, z);
		
	#if 0
	std::cout << "Tan X is " << tanX  * 180 / M_PI << ", "
	          << "Tan Y is " << tanY  * 180 / M_PI << ", " << std::endl;
	#endif
		
	// Start of Genfit routine
		
	// True initial position. Associate it with a first hit
	TVector3 pos(x1, y1, z1);
	//TVector3 mom(tanX, tanY, 0);
	TVector3 mom(0, 0, 1);
		
	mom.RotateX( tanX * M_PI / 180 );
	mom.RotateY( tanY * M_PI / 180);
	
	// Ecal energy deposition
	mom.SetMag(eDep);

	// approximate covariance
	TMatrixDSym covM(6);
	double resolution = 0.1;
	for (int i = 0; i < 3; ++i) {
		covM(i,i) = resolution*resolution;
	}
	for (int i = 3; i < 6; ++i) {
		covM(i,i) = pow(resolution / 10 / sqrt(3), 2);
	}
		
	// Start from electron hypothesis. Maybe from ecal/hcal data
	genfit::AbsTrackRep * rep = new genfit::RKTrackRep(PDG);

	// start state
	genfit::MeasuredStateOnPlane state(rep);
	rep->setPosMomCov(state, pos, mom, covM);
		
	// create track candidate
	//genfit::Track trackCand( rep, pos, mom );
	
	// Temporary vectors
	TVectorD curPos;
	TMatrixDSym curCov;
	
	rep->get6DStateCov(state, curPos, curCov);
	
	/* Iterate over all planes and hits, try to find hits with residual
	 * lower than certain cut. TODO: define cut!
	 */
	
	std::vector<genfit::TrackPoint> tps;
	
	double numOfMissedPoints(0);
	
	for ( unsigned int i = 0; i < trackPoints.size(); ++i ) {
		
		PoolRef<TrackPoint> smallRef;
		
		double smResX(1000), smResY(1000);
		
		for ( unsigned int j = 0; j < trackPoints[i].size(); ++j ) {
	//for ( auto it : trackPoints ) {
		//for ( auto ir : it ) {
			rep->extrapolateBy(state, std::fabs( (*trackPoints[i][j]).gR[2] - curPos[2] ));
			rep->get6DStateCov(state, curPos, curCov);
			
			double cX((*trackPoints[i][j]).gR[0]);
			double cY((*trackPoints[i][j]).gR[1]);
			double cZ((*trackPoints[i][j]).gR[2]);
				
			double resX(0), resY(0), resZ(0);
				
			TVector3 curPoint(cX, cY, cZ);
				
			// get "residuals"
			resX = curPos[0]-curPoint[0];
			resY = curPos[1]-curPoint[1];
			resZ = curPos[2]-curPoint[2];
			
			if ( std::fabs(resX) < smResX && std::fabs(resY) < smResY) {
				smResX = resX;
				smResY = resY;
				smallRef = trackPoints[i][j];
			}
			
			#if 0
			std::cout << "TrackPoint = x: " << cX << ", "
					  << "y: " << cY << ", "
					  << "z: " << cZ << std::endl;
					  
			std::cout << "Current position: "
						  << "x: " << curPos[0] << ", "
						  << "y: " << curPos[1] << ", "
						  << "z: " << curPos[2] << std::endl;
			
			std::cout << "Current residual: "
						  << "x: " << resX << ", "
						  << "y: " << resY << ", "
						  << "z: " << resZ << std::endl;
			#endif
		}
		
		if ( std::fabs(smResX) < 1 && std::fabs(smResY) < 1 ) {
			genfit::TrackPoint tp = *(*smallRef).genfitTrackPoint;
			tps.push_back(tp);
		}
	}
	
	if ( tps.size() > 4 ) {
		_genfitTPs.push_back( tps );
	}
	
	tps.clear();
	
	delete rep;
	
	return true;

}

TrackCKF::ProcRes
TrackCKF::process_event(Event * evPtr) {
    
    AbstractHitHandler<TrackPoint>::process_event( evPtr );
    
	#if 0
	std::cout << "===== NEW EVENT =====" << std::endl;
	std::cout << "ECAl energy deposition: " << evPtr->ecalEdep << std::endl;
	std::cout << "HCAl energy deposition: " << evPtr->hcalEdep << std::endl;
	std::cout << "*SRD energy deposition: " << evPtr->srdEdep << std::endl;
	#endif

	TrPoints trackPoints;
	Seed seeds;
	
	double & ecal = evPtr->ecalEdep;
	double & hcal = evPtr->hcalEdep;
	
	if ( std::isnan(ecal) ) {
		ecal = 0;
	}
	
	if ( std::isnan(hcal) ) {
		hcal = 0;
	}
	
	double eDep = ecal + hcal;
	
	
	if ( std::isnan(evPtr->ecalEdep) || (evPtr->ecalEdep < 20 || evPtr->ecalEdep > 200) ) {
		seeds.clear();
		trackPoints.clear();
		_genfitTPs.clear();
		_tpMap.clear();
		return kOk;
	}
	#if 0
	if ( (evPtr->srdEdep < 1 || evPtr->srdEdep > 70) ) {
		seeds.clear();
		trackPoints.clear();
		_genfitTPs.clear();
		_tpMap.clear();
		return kOk;
	}		
	#endif
	
	//std::cout << evPtr->ecalEdep + evPtr->hcalEdep  << std::endl;
	
	_sort_hits_by_planes( trackPoints );
	
	_create_seed( trackPoints, seeds);
	
	// Process track hypothesis with seed
	for ( auto seed : seeds ) {
		_process_seed( seed, trackPoints, -11, eDep );
	}

	//double dPVal = 1.E-3;
	//int nIter(100);

	//genfit::AbsKalmanFitter * prefitter = new genfit::KalmanFitterRefTrack(nIter, dPVal);
	genfit::AbsKalmanFitter * prefitter = new genfit::DAF(false);
	
	// best track
	double smDiff(10000);
	double bestMom(0);
	double bestChi2(0);
	genfit::Track * bestTrack(nullptr);
	
	for ( unsigned int i = 0; i < _genfitTPs.size(); ++i ) {
		
		TVector3 pos(0, 0, -2100);
		TVector3 mom(0, 0, 1);

		// First point is beamoutlet position	
		double x1(0), y1(0), z1(-2100);
		double x2(seeds[i].X()), y2(seeds[i].Y()), z2(seeds[i].Z());
		
		// create vectors and calculate incline between trackpoints
		double x(0), y(0), z(0), tanX(0), tanY(0);
		
		x = ( x2 - x1 );
		y = ( y2 - y1 );
		z = ( z2 - z1 );
		
		tanX = atan2(x, z);
		tanY = atan2(y, z);

		mom.RotateX( tanX * M_PI / 180 );
		mom.RotateY( tanY * M_PI / 180);		
		
		//mom.SetMag(100);
		mom.SetMag(eDep);		
			
		genfit::AbsTrackRep * rep1 = new genfit::RKTrackRep(-11);
				
		genfit::Track * track = new genfit::Track( rep1, pos, mom );
		
		for ( unsigned int j = 0; j < _genfitTPs[i].size(); ++j ) {
			
			track->insertPoint( &_genfitTPs[i][j] );
		}	
		
		prefitter->processTrack( track );
			
		track->checkConsistency();
			
		track->determineCardinalRep();
				
		genfit::FitStatus* fit = track->getFitStatus();		
						
		genfit::MeasuredStateOnPlane stLast = track->getFittedState();
				
		if ( std::fabs(evPtr->ecalEdep - stLast.getMomMag()) < smDiff ) {
			smDiff = std::fabs(evPtr->ecalEdep - stLast.getMomMag());
			bestMom = stLast.getMomMag();
			bestChi2 = fit->getChi2() / fit->getNdf();
			bestTrack = track;
		}
					
		#if 0
		const int particle =  track->getCardinalRep()->getPDG();    
				
		std::cout << "GenFit track" << " particle PDG = " << particle
				  << " number of points = " << track->getNumPoints()
				  << " momentum = " << stLast.getMomMag()
				  << " fit converged = " << fit->isFitConverged()
				  << " fitted charge = " << fit->getCharge()
				  << " fit Chi^2 = " << fit->getChi2()
				  << " fit Ndf = " << fit->getNdf()
				  << " fit Chi^2/Ndf = " << fit->getChi2() / fit->getNdf() << std::endl;
		#endif
			
	}
	#if 1
	if (bestTrack != nullptr ) {
	
		prefitter->processTrack( bestTrack );
				
		bestTrack->checkConsistency();
		
		//genfit::EventDisplay::getInstance()->addEvent(bestTrack);
		
		for (unsigned int i = 0; i < bestTrack->getNumPoints(); ++i) {
		
			genfit::TrackPoint* tp 
					= bestTrack->getPointWithFitterInfo( i , bestTrack->getCardinalRep());
		
			genfit::AbsFitterInfo * fitterInfo 
					= tp->getFitterInfo(bestTrack->getCardinalRep());
			
			genfit::KalmanFitterInfo* kFitterInfo 
					= static_cast<genfit::KalmanFitterInfo*>(fitterInfo);

			unsigned int nMeasurements = kFitterInfo->getNumMeasurements();
			
			std::vector<genfit::AbsMeasurement*> measurements
					= tp->getRawMeasurements();
			
			for ( unsigned int i = 0; i < nMeasurements; ++i ) {
				
				DetID_t did = measurements[i]->getDetId();
				//std::cout << TDirAdapter::naming()[did] << std::endl;
				
				auto it = _histograms.find(did);
				if ( _histograms.end() == it ) {
					std::string hstName = "residuals";
					std::string hstDescription = "residuals of reconstruction";
					auto substCtx = subst_dict_for(did, hstName);
					std::string description = util::str_subst( hstDescription, substCtx );
					auto p = dir_for( did, substCtx, _overridenPath);
					p.second->cd();
					TH1F * newHst = new TH1F( p.first.c_str()
											, description.c_str()
											, 80, -10, 10 );
					it = _histograms.emplace(did, newHst).first;
				}
				
				//std::cout << TDirAdapter::naming()[did] << std::endl;
			
				const genfit::MeasurementOnPlane & residual 
						= kFitterInfo->getResidual(i);
				
				TVectorD res = residual.getState();
				TMatrixDSym covM = residual.getCov();
				
				it->second->Fill( res[0] );
				
			}
		}
	}
	#endif
	
	if ( bestMom != 0 ) {
		
		hMom->Fill ( bestMom );
			
		hMomVsChi->Fill ( bestChi2 );
			
		hMomVsECal->Fill ( ecal+hcal, bestMom );
	}


	delete prefitter;

	seeds.clear();
	trackPoints.clear();
	_genfitTPs.clear();
	_tpMap.clear();

    return kOk;
}

void
TrackCKF::finalize(){
    
    //genfit::EventDisplay::getInstance()->open();

	for( auto idHstPair : _histograms ) {
		tdirectories()[idHstPair.first]->cd();
		idHstPair.second->Write();
	}  
 
    TCanvas* c1 = new TCanvas(); 
	c1->cd(1);
    hMom->Draw();
    TCanvas* c2 = new TCanvas(); 
    c2->cd(1);
    hMomVsChi->Draw();
    TCanvas* c3 = new TCanvas(); 
    c3->cd(1);
    
}


REGISTER_HANDLER( TrackCKF, banks, ch, cfg
                , "Handler for Ideal 100 GeV path defining" ) {
    
    return new TrackCKF( ch
                       , aux::retrieve_det_selection(cfg)
                       , banks.of<Track>()
                       , cfg["geometry"].as<std::string>() );
}
}
}
#endif  // defined(GenFit_FOUND)
