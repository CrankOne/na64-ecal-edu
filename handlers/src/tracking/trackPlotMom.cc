#include "tracking/trackPlotMom.hh"

#ifdef GenFit_FOUND

#include <Track.h>
#include <MeasuredStateOnPlane.h>

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH1D.h>
#include <TH2D.h>

namespace na64dp {
namespace handlers {

TrackPlotMom::TrackPlotMom( calib::Dispatcher & cdsp
                              , const std::string & only
                              , ObjPool<Track> & bank
                              )
        : AbstractHitHandler<TrackPoint>(cdsp, only)
        , TDirAdapter(cdsp)
        , _tracksBank(bank) {

    // create histograms
//    gROOT->SetStyle("Plain");
//    gStyle->SetPalette(1);
//    gStyle->SetOptFit(1111);
    
    hMom = new TH1D("hResMom" ,"momRes",150, 0, 150);
    
    hMomVsCal = new TH2D("Momentum" ,"momVsEcalHcalCal", 150, 0, 150, 150, 0, 150);
    
    hMomVsECal = new TH1D("MomentumEcal" ,"momVsECal", 150, 0, 150);
    
    hMomVsChi = new TH1D("Chi/Ndf" ,"chiSq2", 100, 0, 20);
    
    
}

TrackPlotMom::ProcRes
TrackPlotMom::process_event(Event * evPtr) {
    
    for ( auto & track : evPtr->tracks ) {
		hMomVsCal->Fill ( (*track.second).momentum , (*track.second).edep );
					
		hMomVsChi->Fill ( (*track.second).chi2ndf);
		
		hMom->Fill ((*track.second).momentum);
		
		hMomVsECal->Fill ( (*track.second).edep );
    }
    
    return kOk;
}

void
TrackPlotMom::finalize(){
    
    TCanvas* c1 = new TCanvas(); 
	c1->cd(1);
    hMom->Draw();
    TCanvas* c2 = new TCanvas(); 
    c2->cd(1);
    hMomVsCal->Draw();
    TCanvas* c3 = new TCanvas(); 
    c3->cd(1);
    hMomVsECal->Draw();
    TCanvas* c4 = new TCanvas(); 
    c4->cd(1);
    hMomVsChi->Draw();
    
}

REGISTER_HANDLER( TrackPlotMom, banks, ch, cfg
                , "Handler to plot reconstructed momentum" ) {
    
    return new TrackPlotMom( ch
                             , aux::retrieve_det_selection(cfg)
                             , banks.of<Track>() 
                             );
}
}
}
#endif  // defined(GenFit_FOUND)
