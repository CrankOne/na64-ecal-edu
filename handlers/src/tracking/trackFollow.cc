#include "tracking/trackFollow.hh"

#ifdef GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>
#include <AbsTrackRep.h>
#include <RKTrackRep.h>

#include <AbsFitterInfo.h>
#include <KalmanFitter.h>
#include <KalmanFitterInfo.h>
#include <KalmanFittedStateOnPlane.h>
#include <DAF.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>


namespace na64dp {
namespace handlers {

TrackFollow::TrackFollow( calib::Dispatcher & cdsp
                        , const std::string & only
                        , ObjPool<Track> & bank )
        : AbstractHitHandler<TrackPoint>(cdsp, only)
		, _tracksBank(bank) {
}

bool
TrackFollow::process_hit( EventID
					    , DetID_t did
                        , TrackPoint & tp ) {
	
	_tps.emplace( did
			, _current_event().trackPoints.pool().get_ref_of(tp) );
							
	return true;
}

TrackFollow::ProcRes
TrackFollow::process_event(Event * evPtr) {
    
    _tps.clear();
    
    AbstractHitHandler<TrackPoint>::process_event( evPtr );

    return kOk;
}

REGISTER_HANDLER( TrackFollow, banks, ch, cfg
                , "Handler for Ideal 100 GeV path defining" ) {
    
    return new TrackFollow( ch
                          , aux::retrieve_det_selection(cfg)
                          , banks.of<Track>() );
}
}
}
#endif  // defined(GenFit_FOUND)
