#include "cut/CUTSBigCut.hh"

namespace na64dp {
namespace handlers {

bool
CUTSBigCut::process_hit( EventID
                           , DetID_t
                           , SADCHit & cHit ){
   

    // Check that max sample in allowable area
    if (  _catChannelFlag && 
            ( cHit.maxSample < _catSampLeft || 
              cHit.maxSample > _catSampRight ) ) {
      this->_set_hit_erase_flag(); 
    }
    // Check that max sample in allowable area
    if (  _cattimeBeginFlag && 
            ( cHit.timeBegin < _cattimeBeginLeft || 
              cHit.timeBegin > _cattimeBeginRight ) ) {
      this->_set_hit_erase_flag(); 
    }
    // Check tangens value 
    if (  _catTanFlag && 
          (  //cHit.angle > _angelUp ||
             cHit.angle < _angelLow  ) ) { 
      this->_set_hit_erase_flag(); 
    }
    return true;
}

}

REGISTER_HANDLER( CUTSBigCut, banks, ch, yamlNode
                , "Get Time Hit using rising edge " ){
    return new handlers::CUTSBigCut( ch
               , aux::retrieve_det_selection(yamlNode)
               , yamlNode["catChannelFlag"] ? yamlNode["catChannelFlag"].as<bool>() : false
               , yamlNode["catSampLeft"] ? yamlNode["catSampLeft"].as<int>() : 8
               , yamlNode["catSampRight"] ? yamlNode["catSampRight"].as<int>() : 26
               , yamlNode["cattimeBeginFlag"] ? yamlNode["cattimeBeginFlag"].as<bool>() : false
               , yamlNode["cattimeBeginLeft"] ? yamlNode["cattimeBeginLeft"].as<int>() : 8
               , yamlNode["cattimeBeginRight"] ? yamlNode["cattimeBeginRight"].as<int>() : 26 
               , yamlNode["catTanFlag"] ? yamlNode["catTanFlag"].as<bool>() : false 
               , yamlNode["angelLow"] ? yamlNode["angelLow"].as<double>() : 70
               , yamlNode["angelUp"] ? yamlNode["angelUp"].as<double>() : 90
               );

}

}

