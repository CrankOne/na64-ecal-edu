#include "calibration/calibECAL.hh"

#include "na64detID/cellID.hh"

namespace na64dp {
namespace handlers {

static CalibECAL::ECALCalib gECALCalibs[] = {
	
	// Energy deposition, tmean, tsigma
	#if 1
	{"ECAL0:0-0-0", 3.792 , 1081 , -75.3 , 6.6 } ,
	{"ECAL0:0-1-0", 3.792 , 1439 , -45.6 , 9.0 } ,
	{"ECAL0:0-2-0", 3.792 , 1367 , -51.7 , 8.4 } ,
	{"ECAL0:0-3-0", 3.792 , 1460 , -48.3 , 6.9 } ,
	{"ECAL0:0-4-0", 3.792 , 1564 , -47.9 , 6.6 } ,
	{"ECAL0:0-5-0", 3.792 , 1418 , -72.2 , 7.8 } ,
	{"ECAL0:1-0-0", 3.792 , 1328 , -48.8 , 9.6 } ,
	{"ECAL0:1-1-0", 3.792 , 1383 , -50.9 , 7.5 } ,
	{"ECAL0:1-2-0", 3.792 , 1471 , -44.4 , 8.4 } ,
	{"ECAL0:1-3-0", 3.792 , 1308 , -45.8 , 11.1 } ,
	{"ECAL0:1-4-0", 3.792 , 1490 , -45.8 , 9.0 } ,
	{"ECAL0:1-5-0", 3.792 , 1642 , -47.5 , 10.5 } ,
	{"ECAL0:2-0-0", 3.792 , 1323 , -44.8 , 9.3 } ,
	{"ECAL0:2-1-0", 3.792 , 1107 , -49.6 , 10.8 } ,
	{"ECAL0:2-2-0", 3.792 , 1278 , -49.6 , 9.0 } ,
	{"ECAL0:2-3-0", 3.792 , 1321 , -42.8 , 6.9 } ,
	{"ECAL0:2-4-0", 3.792 , 1396 , -44.0 , 7.5 } ,
	{"ECAL0:2-5-0", 3.792 , 1426 , -44.1 , 7.2 } ,
	{"ECAL0:3-0-0", 3.792 , 1387 , -44.5 , 7.5 } ,
	{"ECAL0:3-1-0", 3.792 , 1398 , -44.6 , 9.3 } ,
	{"ECAL0:3-2-0", 3.792 , 1401 , -54.5 , 9.3 } ,
	{"ECAL0:3-3-0", 3.792 , 1445 , -44.7 , 6.6 } ,
	{"ECAL0:3-4-0", 3.792 , 1428 , -45.1 , 6.3 } ,
	{"ECAL0:3-5-0", 3.792 , 1069 , -57.6 , 12.6 } ,
	{"ECAL0:4-0-0", 3.792 , 1393 , -45.2 , 10.5 } ,
	{"ECAL0:4-1-0", 3.792 , 1051 , -57.2 , 9.3 } ,
	{"ECAL0:4-2-0", 3.792 , 1265 , -43.0 , 8.4 } ,
	{"ECAL0:4-3-0", 3.792 , 1508 , -49.3 , 8.4 } ,
	{"ECAL0:4-4-0", 3.792 , 1358 , -44.8 , 6.3 } ,
	{"ECAL0:4-5-0", 3.792 , 1521 , -48.8 , 6.6 } ,
	{"ECAL0:5-0-0", 3.792 , 1323 , -67.3 , 15.0 } ,
	{"ECAL0:5-1-0", 3.792 , 1310 , -44.3 , 7.2 } ,
	{"ECAL0:5-2-0", 3.792 , 1619 , -56.6 , 13.5 } ,
	{"ECAL0:5-3-0", 3.792 , 1410 , -51.0 , 15.6 } ,
	{"ECAL0:5-4-0", 3.792 , 724 , -52.8 , 9.0 } ,
	{"ECAL0:5-5-0", 3.792 , 1253 , -70.0 , 10.5 } ,
	{"ECAL0:0-0-1", 72.055 , 2345 , -65.6 , 10.8 } ,
	{"ECAL0:0-1-1", 72.055 , 2331 , -32.2 , 5.1 } ,
	{"ECAL0:0-2-1", 72.055 , 2335 , -31.9 , 7.2 } ,
	{"ECAL0:0-3-1", 72.055 , 2156 , -38.4 , 9.3 } ,
	{"ECAL0:0-4-1", 72.055 , 2260 , -34.0 , 9.3 } ,
	{"ECAL0:0-5-1", 72.055 , 2432 , -70.6 , 10.8 } ,
	{"ECAL0:1-0-1", 72.055 , 2401 , -34.7 , 5.1 } ,
	{"ECAL0:1-1-1", 72.055 , 2347 , -65.0 , 7.8 } ,
	{"ECAL0:1-2-1", 72.055 , 2151 , -66.0 , 5.7 } ,
	{"ECAL0:1-3-1", 72.055 , 2035 , -69.1 , 6.3 } ,
	{"ECAL0:1-4-1", 72.055 , 2343 , -68.5 , 7.2 } ,
	{"ECAL0:1-5-1", 72.055 , 2235 , -24.8 , 4.8 } ,
	{"ECAL0:2-0-1", 72.055 , 2350 , -33.6 , 6.6 } ,
	{"ECAL0:2-1-1", 72.055 , 2356 , -63.2 , 5.1 } ,
	{"ECAL0:2-2-1", 72.055 , 2356 , -67.2 , 6.0 } ,
	{"ECAL0:2-3-1", 72.055 , 2324 , -67.3 , 5.4 } ,
	{"ECAL0:2-4-1", 72.055 , 2325 , -61.5 , 4.8 } ,
	{"ECAL0:2-5-1", 72.055 , 2271 , -44.3 , 6.0 } ,
	{"ECAL0:3-0-1", 72.055 , 2040 , -41.5 , 5.4 } ,
	{"ECAL0:3-1-1", 72.055 , 2185 , -59.9 , 6.6 } ,
	{"ECAL0:3-2-1", 72.055 , 2287 , -59.4 , 5.1 } ,
	{"ECAL0:3-3-1", 72.055 , 2267 , -58.5 , 5.4 } ,
	{"ECAL0:3-4-1", 72.055 , 2407 , -64.2 , 7.5 } ,
	{"ECAL0:3-5-1", 72.055 , 2190 , -42.7 , 5.4 } ,
	{"ECAL0:4-0-1", 72.055 , 2417 , -43.0 , 5.4 } ,
	{"ECAL0:4-1-1", 72.055 , 2616 , -62.0 , 6.0 } ,
	{"ECAL0:4-2-1", 72.055 , 2177 , -57.6 , 4.2 } ,
	{"ECAL0:4-3-1", 72.055 , 2292 , -60.4 , 6.0 } ,
	{"ECAL0:4-4-1", 72.055 , 2341 , -69.7 , 6.6 } ,
	{"ECAL0:4-5-1", 72.055 , 2183 , -36.8 , 5.1 } ,
	{"ECAL0:5-0-1", 72.055 , 2680 , -68.5 , 12.0 } ,
	{"ECAL0:5-1-1", 72.055 , 2028 , -47.2 , 5.7 } ,
	{"ECAL0:5-2-1", 72.055 , 2297 , -47.1 , 6.0 } ,
	{"ECAL0:5-3-1", 72.055 , 2279 , -50.0 , 5.7 } ,
	{"ECAL0:5-4-1", 72.055 , 2144 , -48.1 , 4.5 } ,
	{"ECAL0:5-5-1", 72.055 , 2219 , -71.0 , 12.0 } ,
	
	#else
	// 2018
	{"ECAL0:0-0-0", 5 , 1545 , -75.3 , 6.6 } ,
	{"ECAL0:0-1-0", 5 , 1326 , -45.6 , 9.0 } ,
	{"ECAL0:0-2-0", 5 , 1504 , -51.7 , 8.4 } ,
	{"ECAL0:0-3-0", 5 , 1350 , -48.3 , 6.9 } ,
	{"ECAL0:0-4-0", 5 , 1360 , -47.9 , 6.6 } ,
	{"ECAL0:0-5-0", 5 , 1333 , -72.2 , 7.8 } ,
	{"ECAL0:1-0-0", 5 , 1276 , -48.8 , 9.6 } ,
	{"ECAL0:1-1-0", 5 , 1487 , -50.9 , 7.5 } ,
	{"ECAL0:1-2-0", 5 , 1468 , -44.4 , 8.4 } ,
	{"ECAL0:1-3-0", 5 , 955  , -45.8 , 11.1 } ,
	{"ECAL0:1-4-0", 5 , 1347 , -45.8 , 9.0 } ,
	{"ECAL0:1-5-0", 5 , 1572 , -47.5 , 10.5 } ,
	{"ECAL0:2-0-0", 5 , 1451 , -44.8 , 9.3 } ,
	{"ECAL0:2-1-0", 5 , 968  , -49.6 , 10.8 } ,
	{"ECAL0:2-2-0", 5 , 1400 , -49.6 , 9.0 } ,
	{"ECAL0:2-3-0", 5 , 1392 , -42.8 , 6.9 } ,
	{"ECAL0:2-4-0", 5 , 1260 , -44.0 , 7.5 } ,
	{"ECAL0:2-5-0", 5 , 1538 , -44.1 , 7.2 } ,
	{"ECAL0:3-0-0", 5 , 1476 , -44.5 , 7.5 } ,
	{"ECAL0:3-1-0", 5 , 1294 , -44.6 , 9.3 } ,
	{"ECAL0:3-2-0", 5 , 1247 , -54.5 , 9.3 } ,
	{"ECAL0:3-3-0", 5 , 1358 , -44.7 , 6.6 } ,
	{"ECAL0:3-4-0", 5 , 1330 , -45.1 , 6.3 } ,
	{"ECAL0:3-5-0", 5 , 1703 , -57.6 , 12.6 } ,
	{"ECAL0:4-0-0", 5 , 1398 , -45.2 , 10.5 } ,
	{"ECAL0:4-1-0", 5 , 1198 , -57.2 , 9.3 } ,
	{"ECAL0:4-2-0", 5 , 1349 , -43.0 , 8.4 } ,
	{"ECAL0:4-3-0", 5 , 1716 , -49.3 , 8.4 } ,
	{"ECAL0:4-4-0", 5 , 1429 , -44.8 , 6.3 } ,
	{"ECAL0:4-5-0", 5 , 1295 , -48.8 , 6.6 } ,
	{"ECAL0:5-0-0", 5 , 1364 , -67.3 , 15.0 } ,
	{"ECAL0:5-1-0", 5 , 1419 , -44.3 , 7.2 } ,
	{"ECAL0:5-2-0", 5 , 1347 , -56.6 , 13.5 } ,
	{"ECAL0:5-3-0", 5 , 1246 , -51.0 , 15.6 } ,
	{"ECAL0:5-4-0", 5 , 1429, -52.8 , 9.0 } ,
	{"ECAL0:5-5-0", 5 , 1472 , -70.0 , 10.5 } ,
	{"ECAL0:0-0-1", 78 , 2373 , -65.6 , 10.8 } ,
	{"ECAL0:0-1-1", 78 , 2169 , -32.2 , 5.1 } ,
	{"ECAL0:0-2-1", 78 , 2410 , -31.9 , 7.2 } ,
	{"ECAL0:0-3-1", 78 , 2405 , -38.4 , 9.3 } ,
	{"ECAL0:0-4-1", 78 , 2163 , -34.0 , 9.3 } ,
	{"ECAL0:0-5-1", 78 , 2388 , -70.6 , 10.8 } ,
	{"ECAL0:1-0-1", 78 , 2363 , -34.7 , 5.1 } ,
	{"ECAL0:1-1-1", 78 , 2431 , -65.0 , 7.8 } ,
	{"ECAL0:1-2-1", 78 , 2066 , -66.0 , 5.7 } ,
	{"ECAL0:1-3-1", 78 , 2307 , -69.1 , 6.3 } ,
	{"ECAL0:1-4-1", 78 , 2488 , -68.5 , 7.2 } ,
	{"ECAL0:1-5-1", 78 , 2700 , -24.8 , 4.8 } ,
	{"ECAL0:2-0-1", 78 , 2739 , -33.6 , 6.6 } ,
	{"ECAL0:2-1-1", 78 , 2199 , -63.2 , 5.1 } ,
	{"ECAL0:2-2-1", 78 , 2454 , -67.2 , 6.0 } ,
	{"ECAL0:2-3-1", 78 , 2193 , -67.3 , 5.4 } ,
	{"ECAL0:2-4-1", 78 , 2832 , -61.5 , 4.8 } ,
	{"ECAL0:2-5-1", 78 , 2165 , -44.3 , 6.0 } ,
	{"ECAL0:3-0-1", 78 , 2941 , -41.5 , 5.4 } ,
	{"ECAL0:3-1-1", 78 , 2879 , -59.9 , 6.6 } ,
	{"ECAL0:3-2-1", 78 , 2436 , -59.4 , 5.1 } ,
	{"ECAL0:3-3-1", 78 , 2109 , -58.5 , 5.4 } ,
	{"ECAL0:3-4-1", 78 , 2303 , -64.2 , 7.5 } ,
	{"ECAL0:3-5-1", 78 , 1807 , -42.7 , 5.4 } ,
	{"ECAL0:4-0-1", 78 , 2374 , -43.0 , 5.4 } ,
	{"ECAL0:4-1-1", 78 , 2498 , -62.0 , 6.0 } ,
	{"ECAL0:4-2-1", 78 , 2307 , -57.6 , 4.2 } ,
	{"ECAL0:4-3-1", 78 , 2195 , -60.4 , 6.0 } ,
	{"ECAL0:4-4-1", 78 , 2482 , -69.7 , 6.6 } ,
	{"ECAL0:4-5-1", 78 , 2195 , -36.8 , 5.1 } ,
	{"ECAL0:5-0-1", 78 , 1981 , -68.5 , 12.0 } ,
	{"ECAL0:5-1-1", 78 , 2081 , -47.2 , 5.7 } ,
	{"ECAL0:5-2-1", 78 , 2317 , -47.1 , 6.0 } ,
	{"ECAL0:5-3-1", 78 , 2330 , -50.0 , 5.7 } ,
	{"ECAL0:5-4-1", 78 , 2834 , -48.1 , 4.5 } ,
	{"ECAL0:5-5-1", 78 , 2325 , -71.0 , 12.0 } ,
	#endif

};

CalibECAL::CalibECAL( calib::Dispatcher & ch
                    , const std::string & only
                    , double timeCut) 
                    : AbstractHitHandler<SADCHit>(ch, only)
				    , _timeCut(timeCut) {
						
	ch.subscribe<nameutils::DetectorNaming>(*this, "default");
}

void
CalibECAL::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<SADCHit>::handle_update(nm);

    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.ecalKinCode = nm.kin_id("ECAL").second;
    
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gECALCalibs)/sizeof(ECALCalib)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gECALCalibs[i].planeName);
        // impose placement entry into `_placements' map
        _calibs.emplace(did, gECALCalibs + i);
    }
}

bool
CalibECAL::process_hit( EventID
					  , DetID_t did
                      , SADCHit & hit ) {
	
	#if 0
	std::cout << naming()[did] << std::endl;
	std::cout << hit.maxValue << std::endl;
	std::cout << hit.maxSample << std::endl;
	#endif
	
	// Process ECAL hits and calibrate energy deposition
	
	DetID stationID(did);
	
	if ( _namingCache.ecalKinCode != stationID.kin() ) {
		return true;
	}
				
	const ECALCalib * cCalibs = _calibs[did];
	
	double bin(12.5); // bin time in ns
	
	double & masterTime = _current_event().masterTime;
	
	double bestValue(std::numeric_limits<double>::min());
	double bestSample(std::numeric_limits<int>::min());
	double smDiff(std::numeric_limits<int>::max());
		
	for ( auto & it : hit.maxima ) {
		double timeDiff = ((it.first * bin) - cCalibs->time) - masterTime;
		
		if ( std::fabs(timeDiff) < smDiff ) {
			smDiff = std::fabs(timeDiff);
			bestSample = it.first;
			bestValue = it.second;
		}
	}
	
	if ( std::fabs(smDiff) < _timeCut ) { 
	
		hit.maxValue = bestValue;
		hit.maxSample = bestSample;
		
		hit.eDep = bestValue * (cCalibs->factor / cCalibs->energy);
	} else {
		hit.eDep = 0;
	}
							   
	return true;
}

}

REGISTER_HANDLER( CalibECAL, banks, ch, yamlNode
                , "Handler for ECAL time and energy calibration" ) {
    
    return new handlers::CalibECAL( ch
								  , aux::retrieve_det_selection(yamlNode)
								  , yamlNode["timeCut"].as<double>() );
};
}
