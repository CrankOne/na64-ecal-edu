#include "plotting/hcalEcal.hh"

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH1D.h>

namespace na64dp {
namespace handlers {

HcalEcal::HcalEcal( calib::Dispatcher & cdsp
                              , const std::string & only
                              )
        : AbstractHitHandler<TrackPoint>(cdsp, only) {
    
    eDep = new TH2F("HCAL vs ECAL" ,"eDep",100, 0, 150, 100, 0, 150);
    
}

HcalEcal::ProcRes
HcalEcal::process_event(Event * evPtr) {
    
    
    double & hcal = evPtr->hcalEdep;
    double & ecal = evPtr->ecalEdep;
		
	eDep->Fill ( ecal, hcal );

    
    return kOk;
}

void
HcalEcal::finalize(){
    
    TCanvas* c1 = new TCanvas(); 
    c1->cd(1);
    eDep->Draw();   
    
}

REGISTER_HANDLER( HcalEcal, banks, ch, cfg
                , "Handler to plot Hcal vs Ecal energy deposition" ) {
    
    return new HcalEcal( ch
                             , aux::retrieve_det_selection(cfg)
                             );
}
}
}
