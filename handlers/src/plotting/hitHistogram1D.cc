#include "plotting/hitHistogram1D.hh"

namespace na64dp {

REGISTER_HANDLER( SADCHitHistogram1D
                , banks, ch, cfg
                , "Plots certain value of a SADC detector hit as 1D histogram" ) {
    return new handlers::HitHistogram1D<SADCHit>( ch
                                                , aux::retrieve_det_selection(cfg)
                                                , utils::value_getter<SADCHit>(cfg["value"].as<std::string>())
                                                , cfg["histName"].as<std::string>()
                                                , cfg["histDescr"].as<std::string>()
                                                , cfg["nBins"].as<int>()
                                                , cfg["range"][0].as<double>()
                                                , cfg["range"][1].as<double>()
                                                , cfg["path"] ? cfg["path"].as<std::string>() : ""
                                                );
}

REGISTER_HANDLER( APVHitHistogram1D
                , banks, ch, cfg
                , "Plots certain value of an APV detector hit as 1D histogram" ) {
    return new handlers::HitHistogram1D<APVHit>( ch
                                               , aux::retrieve_det_selection(cfg)
                                               , utils::value_getter<APVHit>(cfg["value"].as<std::string>())
                                               , cfg["histName"].as<std::string>()
                                               , cfg["histDescr"].as<std::string>()
                                               , cfg["nBins"].as<int>()
                                               , cfg["range"][0].as<double>()
                                               , cfg["range"][1].as<double>()
                                               , cfg["path"] ? cfg["path"].as<std::string>() : ""
                                               );
}

REGISTER_HANDLER( APVClusterHistogram1D
                , banks, ch, cfg
                , "Plots certain value of a cluster on the APV detector plane as 1D histogram" ) {
    return new handlers::HitHistogram1D<APVCluster>( ch
                                               , aux::retrieve_det_selection(cfg)
                                               , utils::value_getter<APVCluster>(cfg["value"].as<std::string>())
                                               , cfg["histName"].as<std::string>()
                                               , cfg["histDescr"].as<std::string>()
                                               , cfg["nBins"].as<int>()
                                               , cfg["range"][0].as<double>()
                                               , cfg["range"][1].as<double>()
                                               , cfg["path"] ? cfg["path"].as<std::string>() : ""
                                               );
}

REGISTER_HANDLER( CaloEDepHistogram1D
                , banks, ch, cfg
                , "Plots certain value of CaloEDep as 1D histogram" ) {
    return new handlers::HitHistogram1D<CaloEDep>( ch
                                               , aux::retrieve_det_selection(cfg)
                                               , utils::value_getter<CaloEDep>(cfg["value"].as<std::string>())
                                               , cfg["histName"].as<std::string>()
                                               , cfg["histDescr"].as<std::string>()
                                               , cfg["nBins"].as<int>()
                                               , cfg["range"][0].as<double>()
                                               , cfg["range"][1].as<double>()
                                               , cfg["path"] ? cfg["path"].as<std::string>() : ""
                                               );
}

}
