//#include "handlers/plotting/EcalAmpHists.h"
#include "plotting/SADCAmpHists.hh"

namespace na64dp {
namespace handlers {

/** \param ch Calibration handle instance
 *  \param nAmpBins Number of bins by vertical axis
 *  \param ampMin Lower bound of amplitude histogram range (vertical axis)
 *  \param ampMax Upper bound of amplitude histogram range (vertical axis)
 *  \param baseName Histogram name suffix to name the corresponding TH2F
 *  */
SADCAmpHists::SADCAmpHists( calib::Dispatcher & ch
                          , size_t nAmpBins
                          , double ampMin, double ampMax
                          , const std::string & baseName
                          ) : TDirAdapter(ch)
                            , AbstractHitHandler(ch)
                            , _ampNBins(nAmpBins)
                            , _ampRange{ampMin, ampMax}
                            , _hstBaseName(baseName) {
}

bool
SADCAmpHists::process_hit( EventID
                         , DetID_t did
                         , SADCHit & currentHit) {
    auto it = _hists.find( did );
    if( _hists.end() == it ) {
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;
        dir_for(did, histName)->cd();
        TH2F * newHst = new TH2F( histName.c_str()
                                , "waveform distributions; Channels; Amplitude"
                                , 32, 0, 32
                                , _ampNBins, _ampRange[0], _ampRange[1] );
        it = _hists.emplace( did, newHst ).first;
    }
    // fill the histogram
    for(int i = 0; i < 32; ++i){
        it->second->Fill(i, currentHit.wave[i] );
    }
    return true;
}

void
SADCAmpHists::finalize() {
    for( auto idHstPair : _hists ) {
        tdirectories()[idHstPair.first]->cd();
        idHstPair.second->Write();
    }
}

SADCAmpHists::~SADCAmpHists() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second;
    }
}

}

REGISTER_HANDLER( SADCAmpHists, banks, ch, yamlNode
                , "builds 2D histograms of SADC amplitudes" ) {
    return new handlers::SADCAmpHists( ch
                           , yamlNode["ampNBins"].as<int>()
                           , yamlNode["ampRange"][0].as<float>()
                           , yamlNode["ampRange"][1].as<float>()
                           , yamlNode["baseName"].as<std::string>()
                           );
}

}
