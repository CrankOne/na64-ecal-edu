#include "plotting/clusterChargePlot.hh"

#ifdef ROOT_FOUND

namespace na64dp {
namespace handlers {

void
ClusterChargePlot::_fill_cluster_value(const APVCluster & cluster, TH1F * hst) {
    hst->Fill( cluster.charge );
}

ClusterChargePlot::ClusterChargePlot( calib::Dispatcher & cdsp
                                  , const std::string & hstName
                                  , const std::string & hstDescr
                                  , Int_t nBins, Float_t hstMin, Float_t hstMax )
        : Cluster1DHistogram( cdsp, hstName, hstDescr, nBins, hstMin, hstMax ) {
}

}


REGISTER_HANDLER( ClusterChargePlot, banks, ch, yamlNode
                , "Produces a histogram of APV hits cluster charge distribution" ) {
    return new handlers::ClusterChargePlot( ch
                                 , yamlNode["histName"].as<std::string>()
                                 , yamlNode["histDescr"].as<std::string>()
                                 , yamlNode["nBins"].as<Int_t>()
                                 , yamlNode["range"][0].as<Float_t>()
                                 , yamlNode["range"][1].as<Float_t>() );
}

}

#endif

