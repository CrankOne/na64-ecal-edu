#include "plotting/preshowerVsRest.hh"

#include "na64detID/cellID.hh"

#include <TH2F.h>

namespace na64dp {
namespace handlers {

PreshowerVsRestECAL::PreshowerVsRestECAL( calib::Dispatcher & cdsp
                                        , const std::string & selection
                                        , const std::string & hstName
                                        , const std::string & hstTitle
                                        , size_t nBinsPreshower, double preshMin, double preshMax
                                        , size_t nBinsRest, double restMin, double restMax
                                        )
                    : AbstractHitHandler<SADCHit>(cdsp, selection)
                    , _nBinsPreshower(nBinsPreshower), _preshMin(preshMin), _preshMax(preshMax)
                    , _nBinsRest(nBinsRest), _restMin(restMin), _restMax(restMax)
                    , _hstName(hstName), _hstTitle(hstTitle)
                    , _histogram(nullptr)
                    {}

void
PreshowerVsRestECAL::handle_update( const nameutils::DetectorNaming & nm ) {
    _ecalDID = nm["ECAL"];
}

AbstractHandler::ProcRes
PreshowerVsRestECAL::process_event(Event * e) {
    _preshowerSum = _restSum = 0.;
    auto res = AbstractHitHandler<SADCHit>::process_event(e);
    if( !_histogram ) {
        _histogram = new TH2F( _hstName.c_str()
                             , _hstTitle.c_str()
                             , _nBinsRest, _restMin, _restMax
                             , _nBinsPreshower, _preshMin, _preshMax
                             );
    }
    _histogram->Fill( _restSum, _preshowerSum );
    std::cout << "xxx " << _restSum                     // XXX
              << ", " << _preshowerSum << std::endl;    // XXX
    return res;
}

bool
PreshowerVsRestECAL::process_hit( EventID
                                , DetID_t did
                                , SADCHit & currentHit) {
    CellID cid(DetID(did).payload());
    if( DetID(did).chip() != _ecalDID.chip() || DetID(did).kin() != _ecalDID.kin() )
        return true;
    assert( 0 == cid.get_z()|| 1 == cid.get_z());
    if( 0 == cid.get_z() ) {
        _preshowerSum += currentHit.eDep;
    } else {
        _restSum += currentHit.eDep;
    }
    return true;
}

void
PreshowerVsRestECAL::finalize() {
    if( _histogram ) {
        _histogram->Write();
    }
}

PreshowerVsRestECAL::~PreshowerVsRestECAL() {
    if( _histogram ) {
        delete _histogram;
    }
}

}

REGISTER_HANDLER( PreshowerVsRestECAL, banks, ch, yamlNode
                , "energy sum in preshower vs rest part of ECAL" ) {
    return new handlers::PreshowerVsRestECAL( ch
                           , aux::retrieve_det_selection(yamlNode)
                           , yamlNode["histogramName"].as<std::string>()
                           , yamlNode["histogramTitle"].as<std::string>()
                           , yamlNode["preshowerNBins"].as<int>()
                           , yamlNode["preshowerRange"][0].as<float>()
                           , yamlNode["preshowerRange"][1].as<float>()
                           , yamlNode["restNBins"].as<int>()
                           , yamlNode["restRange"][0].as<float>()
                           , yamlNode["restRange"][1].as<float>()
                           );
}

}

