# include "data/JenksClassify.hh"

namespace na64dp {

namespace handlers { }

REGISTER_HANDLER( JenksBreaksSADC
                , banks, ch, cfg
                , "Evaluates Jenks natural breaks procedure on given value"
                  " of SADC hit" ) {
    return new handlers::JenksClassify<SADCHit>( ch
                , aux::retrieve_det_selection(cfg)
                , utils::value_getter<SADCHit>(cfg["value"].as<std::string>())
                , cfg["nClasses"].as<int>()
                );
}

REGISTER_HANDLER( JenksBreaksAPV
                , banks, ch, cfg
                , "Evaluates Jenks natural breaks procedure on given value"
                  " of APV hit" ) {
    return new handlers::JenksClassify<APVHit>( ch
                , aux::retrieve_det_selection(cfg)
                , utils::value_getter<APVHit>(cfg["value"].as<std::string>())
                , cfg["nClasses"].as<int>()
                );
}

REGISTER_HANDLER( JenksBreaksAPVCluster
                , banks, ch, cfg
                , "Evaluates Jenks natural breaks procedure on given value"
                  " of APV cluster" ) {
    return new handlers::JenksClassify<APVCluster>( ch
                , aux::retrieve_det_selection(cfg)
                , utils::value_getter<APVCluster>(cfg["value"].as<std::string>())
                , cfg["nClasses"].as<int>()
                );
}

}

#if 0
REGISTER_HANDLER( JenksClassify, yamlNode
                , "performs Jenks-Fisher natural breaks optimization over a"
                  " choosen value subset for certain cell" ){
    return new JenksClassify( yamlNode["cellID"][0].as<int>()
                            , yamlNode["cellID"][1].as<int>()
                            , yamlNode["cellID"][2].as<int>()
                            , yamlNode["nClasses"].as<int>()
                            , yamlNode["value"].as<std::string>() );
} 
#endif

