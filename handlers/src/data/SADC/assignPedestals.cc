#include "data/SADC/assignPedestals.hh"

namespace na64dp {
namespace handlers {

AssignPedestals::AssignPedestals( calib::Dispatcher & cdsp
                                , const std::string & detSelection
                                , bool setNonExistingToNan )
        : AbstractHitHandler<SADCHit>(cdsp, detSelection)
        , _setNonExistingToNan(setNonExistingToNan)
        , _pedestals("default", cdsp) {
}

bool
AssignPedestals::process_hit( EventID
                            , DetID_t detID
                            , SADCHit & hit ) {
    auto it = _pedestals->find( detID );
    if( _pedestals->end() == it ) {
        // pedestals values found
        if( _setNonExistingToNan ) {
            hit.pedestals[0] = hit.pedestals[1] = std::nan("0");
        }
        return true;
    }
    // pedestals found
    const std::pair<calib::NDValue, calib::NDValue> & pp = it->second;
    hit.pedestals[0] = pp.first.mean;
    hit.pedestals[1] = pp.second.mean;
    return true;
}

}

REGISTER_HANDLER( SADCAssignPedestals, banks, dispatcher, cfg
                , "Assigns pedestals from available calibration data to SADC hit" ) {
    return new handlers::AssignPedestals( dispatcher
                                        , aux::retrieve_det_selection(cfg)
                                        , cfg["setNonExistingToNan"]
                                            ? cfg["setNonExistingToNan"].as<bool>()
                                            : false
                                        );
}

}

