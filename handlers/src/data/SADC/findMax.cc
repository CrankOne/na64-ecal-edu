#include "data/SADC/findMax.hh"

#include <limits>

namespace na64dp {
namespace handlers {

bool
SADCFindMax::process_hit( EventID
                        , DetID_t did
                        , SADCHit & hit ) {
    bool maxFound = find_maximum( hit.wave, hit.maxValue, hit.maxSample );
    if( _rmHitOnFailure && ! maxFound ) {
        _set_hit_erase_flag();
    }
    return true;
}

bool
SADCFindMax::find_maximum( const double * wf
                         , double & value
                         , double & time ) {
    int maxIdx = max_peak_idx( wf, _ampRange[0],    _ampRange[1]
                                 , _timeRange[0],   _timeRange[1] );
    if( -1 == maxIdx ) return false;
    value = wf[maxIdx];
    time = maxIdx;
    return true;
}


int *
SADCFindMax::collect_peaks( const double * wf, int * nSamples ) {
    for( int i = 1; i < 31; ++i ) {
        const double A0 = wf[i-1]
                   , A1 = wf[i]
                   , A2 = wf[i+1]
                   ;
        const bool riseOnLeft = (A1 >= A0)
                 , fallOnRight = (A1 >= A2)
                 , isPlateau = (A1 == A0 && A1 == A2)
                 , isPeak = !isPlateau && riseOnLeft && fallOnRight
                 ;
        if( isPeak ) {
            *(nSamples++) = i;
        }
    }
    return nSamples;
}

int
SADCFindMax::max_peak_idx( const double * wf
                         , const double ampMin, const double ampMax
                         , const double timeMin, const double timeMax ) {
    // Collect all the peaks in waveform
    int peaks[32];
    const int * peaksEnd = collect_peaks( wf, peaks );
    // Init the peak values
    double maxAmp = std::numeric_limits<double>::infinity();
    int maxPeakIdx = -1;
    // Iterate over all the found peaks
    for( int * cPeakIdxPtr = peaks
       ; cPeakIdxPtr != peaksEnd
       ; ++cPeakIdxPtr ) {

        const int cPeakIdx = *cPeakIdxPtr;
        assert(cPeakIdx < 32);
        const double cPeakAmp = wf[cPeakIdx];

        // if peak amplitude is out of range, skip it
        if( cPeakAmp > ampMax || cPeakAmp < ampMin )
            continue;
        if( cPeakIdx > timeMax || cPeakIdx < timeMin )
            continue;

        // initialize value if need, compare current peak value with previous
        // candidate and re-set if need
        if( -1 == maxPeakIdx || cPeakAmp > maxAmp ) {
            maxAmp = cPeakAmp;
            maxPeakIdx = cPeakIdx;
        }
    }
    return maxPeakIdx;
}

}  // namespace ::na64dp::handlers


REGISTER_HANDLER( SADCFindMax, banks, ch, cfg
                , "Locates maximum sample in SADC waveform" ) {
    return new handlers::SADCFindMax( ch
            , aux::retrieve_det_selection(cfg)
            , cfg["removeFailedHits"] ? false : cfg["removeFailedHits"].as<bool>()
            , cfg["ampRange"][0].as<double>(),  cfg["ampRange"][1].as<double>()
            , cfg["timeRange"][0].as<double>(), cfg["timeRange"][1].as<double>()
            );
}

}


