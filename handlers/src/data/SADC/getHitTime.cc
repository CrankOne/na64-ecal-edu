#include "data/SADC/getHitTime.hh"

namespace na64dp {
namespace handlers {

bool
SADCGetTimeHit::process_hit( EventID
                           , DetID_t
                           , SADCHit & cHit ){
    
    // Using cut on tang
    double tempCutOnTan = 0;
    // Value of waveform at half-maximum
    double AmpHalfWave = 0.5*cHit.maxValue,
           noise = 1,
           i_rise = -1;
    // Search rising edge
    for ( int i = cHit.maxSample; i > 1; --i ) {
      // check that adjacent value waveform differ by more than noise value
      if ( cHit.wave[i - 1] > cHit.wave[i] - noise ) { continue; } 
      // check the 0.5*A_max cross condition
      if (cHit.wave[i - 1] > AmpHalfWave ) { continue; }
      // check that the values of waveform are positive
      if( cHit.wave[i - 1] < 0 && cHit.wave[i] < 0 ) { continue; }
      // calculate intersection of the rising 
      // edge with time axis to get origin time
      i_rise = i   
             - cHit.wave[i]/( cHit.wave[i] - cHit.wave[i - 1] );
      // For cut on tang
      tempCutOnTan = cHit.wave[i] - cHit.wave[i - 1];
      // calculation of the time to value at half-maximum waveform
      if (_halfHeightFlag) {
        i_rise += AmpHalfWave/(cHit.wave[i] - cHit.wave[i - 1]);
      }
      // conversion time from channel to ns
      if (_conversTot0nsFlag) { i_rise *= CONST_CHANNEL_TO_ns; }
      // Exit loop
      break;
    }
    // Write time in hit
    cHit.timeBegin = i_rise;
    cHit.angle = ( 180 / M_PI ) * atan(tempCutOnTan);
    return true;
}

}

REGISTER_HANDLER( SADCGetTimeHit, banks, ch, yamlNode
                , "Get Time Hit using rising edge " ){
    return new handlers::SADCGetTimeHit( ch
               , aux::retrieve_det_selection(yamlNode)
               , yamlNode["conversTot0nsFlag"] ? yamlNode["conversTot0nsFlag"].as<bool>() : false
               , yamlNode["halfHeightFlag"] ? yamlNode["halfHeightFlag"].as<bool>() : false 
               );

}

}

