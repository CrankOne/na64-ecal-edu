#include "data/SADC/findMaxSimple.hh"

namespace na64dp {
namespace handlers {

bool
SADCFindMaxSimple::process_hit( EventID
                        , DetID_t did
                        , SADCHit & hit ) {
	
    int maxSampleNo = 0;
    for( int i = 1; i < 32; ++i ) {
        if( hit.wave[i] > hit.wave[maxSampleNo] ) {
            maxSampleNo = i;
        }
    }
    hit.maxValue = hit.wave[maxSampleNo];
    hit.maxSample = maxSampleNo;
   
    return true;
}

}  // namespace ::na64dp::handlers


REGISTER_HANDLER( SADCFindMaxSimple, banks, ch, cfg
                , "Locates maximum sample in SADC waveform by direct lookup"
                  " of each sample's amplitude" ) {
    return new handlers::SADCFindMaxSimple( ch
            , aux::retrieve_det_selection(cfg)
            );
}

}



