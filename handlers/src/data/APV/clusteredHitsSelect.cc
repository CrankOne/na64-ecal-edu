#include "data/APV/clusteredHitsSelect.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace handlers {

APVClusteredHitsSelect::APVClusteredHitsSelect( calib::Dispatcher & ch
        , const std::string & only
        , EvFieldTraits<APVCluster>::ValueGetter getter
        , functions::Comparator compare
        , double threshold ) : AbstractHitHandler<APVCluster>(ch, only)
                             , _getter(getter)
                             , _threshold(threshold)
                             , _compare(compare) {}

bool
APVClusteredHitsSelect::process_hit( EventID
                                   , DetID_t detId
                                   , APVCluster & c ) {
    if( !_compare( _getter(c), _threshold ) ) {
        // fails check (comparison yields `false') -- remove this cluster's
        // hits and cluster itself
        for( auto it = c.begin()
           ; c.end() != it
           ; ++it ) {
            // to find out which hit to remove, build the APV detector ID with
            // hit's wire information
            DetID wID(detId);
            wID.payload( it->second->rawData.wireNo );
            auto evMapIt = _current_event().apvHits.find( wID.id );
            if( _current_event().apvHits.end() == evMapIt ) {
                // This means that cluster refers to hit that is not indexed
                // within APV hits map. This situation shall be considered as
                // an indication of a bug.
                NA64DP_RUNTIME_ERROR( "Event integrity violated: APV cluster"
                                          " refers to non-existing hit." );
            }
            // erase
            EvFieldTraits<APVHit>::remove_from_event(
                    _current_event(), evMapIt, naming() );
        }
        // mark current cluster for removal
        this->_set_hit_erase_flag();
    }
    return true;  // continue processing
}

}

REGISTER_HANDLER( APVClusteredHitsSelect, banks, ch, cfg
                , "Removes clusters together with corresponding APV hits"
                  " from an event, following by some cluster selection criterion." ) {
    return new handlers::APVClusteredHitsSelect( ch
            , aux::retrieve_det_selection(cfg)
            , utils::value_getter<APVCluster>( cfg["value"].as<std::string>() )
            , functions::get_comparator( cfg["condition"].as<std::string>().c_str() )
            , cfg["threshold"].as<double>()
            );
}

}
