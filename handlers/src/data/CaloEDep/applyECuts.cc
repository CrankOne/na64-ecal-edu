#include "data/CaloEDep/applyECuts.hh"

namespace na64dp {
namespace handlers {

CaloEDepCuts::CaloEDepCuts( calib::Dispatcher & ch
                          , const std::string & only
                          , double minEnergy
					      , double maxEnergy
		) : AbstractHitHandler<CaloEDep>(ch, only)
		  , _minEnergy(minEnergy)
		  , _maxEnergy(maxEnergy) {}

bool
CaloEDepCuts::process_hit( EventID
			     		 , DetID_t did
                         , CaloEDep & hit ) {
	
	if ( hit.eDepMeV < _minEnergy || hit.eDepMeV > _maxEnergy ) {
		_set_event_processing_result(kDiscriminateEvent);
	}
	
	return true;
}

}

REGISTER_HANDLER( CaloEDepCuts, banks, ch, yamlNode
                , "Simple handler to apply energy cuts to calorimeters" ) {
    
    return new handlers::CaloEDepCuts( ch
								     , aux::retrieve_det_selection(yamlNode)
								     , yamlNode["EnergyRange"][0].as<double>()
									 , yamlNode["EnergyRange"][1].as<double>());
};
}
