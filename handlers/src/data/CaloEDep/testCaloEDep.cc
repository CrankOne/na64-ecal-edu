#include "data/CaloEDep/testCaloEDep.hh"

namespace na64dp {
namespace handlers {

TestCaloEDep::TestCaloEDep( calib::Dispatcher & ch
                          , const std::string & only
                          , ObjPool<CaloEDep> & bank
		) : AbstractHitHandler<CaloEDep>(ch, only)
		  , _caloInserter(bank) {}

bool
TestCaloEDep::process_hit( EventID
			     		 , DetID_t did
                         , CaloEDep & hit ) {
	
	std::cout << "Hi you there!" << std::endl;
	std::cout << naming()[did] << std::endl;
			   
	return true;
}

TestCaloEDep::ProcRes
TestCaloEDep::process_event(Event * evPtr) {
    
    for ( auto it : evPtr->sadcHits ) {
		CaloEDep calo;
    
		*_caloInserter( *evPtr, it.first, naming()) = calo;
	}
    
    AbstractHitHandler<CaloEDep>::process_event( evPtr );
    
    return kOk;
}

}

REGISTER_HANDLER( TestCaloEDep, banks, ch, yamlNode
                , "Handler for testing energy deposition in calorimeters" ) {
    
    return new handlers::TestCaloEDep( ch
								     , aux::retrieve_det_selection(yamlNode)
								     , banks.of<CaloEDep>() );
};
}
