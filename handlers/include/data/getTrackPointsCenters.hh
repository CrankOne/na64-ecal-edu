#pragma once

#include "na64dp/abstractHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Obtains clusters centers information
 *
 * Copies information of 1D cluster position to local coordinates vector.
 *
 * \note The role of this handler is not to compute the cluster centers as one
 * would probably expect, but rather to address 1D cluster center offset to a
 * particular local coordinate taking into account information of X/Y or U/V
 * coordinates precedence (e.g. 'X' comes befor 'Y' in spatial coordinates set).
 *
 * \todo Currently only support X/Y and U/V pairwise-adjoint clusters.
 * */
class GetTrackPointsCenters : public AbstractHandler {
protected:
    virtual void _process_track_point( TrackPoint & );
public:
    /// Fills the 2D track points histograms
    virtual ProcRes process_event( Event * e ) override;
};

}
}

