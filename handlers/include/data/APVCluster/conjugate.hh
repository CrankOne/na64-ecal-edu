#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief Produces track data with track points
 *
 * This handler relies on the presence of 1D cluster objects in the event
 * structure that were previously being added with clustering handler
 * (like `APVFindClusters`). Among them, the tuples (pairs, triplets, octets,
 * etc) of clusters will be found and new track point objects will be created
 * within the event structure for subsequent analysis. Only knowledge of
 * conjugated planes within a single tracking station will be used.
 *
 * \image html handlers/mm4.png
 *
 * Created track points will have their position information unitialized
 * (set to NaN).
 *
 * \note This handler does not rely on any coordinates or plane positioning
 * information
 * \todo Currently only _pairs_ of planes are supported since in NA64 there is
 * no detector stations with projection planes >2.
 * \todo "applyTo" (only detectors) is currently being ignored
 * */
class APVConjugateClusters : public AbstractHandler
                           , public util::Observable<nameutils::DetectorNaming>::iObserver {
public:
    struct NamingCache {
        DetChip_t apvChipCode;
        DetKin_t mmKinCode
               , gmKinCode
               ;
    };
protected:
    /// Caching codes accelerates clusters retreival from an event.
    NamingCache _namingCache;
    /// Ptr to current name mappings
    const nameutils::DetectorNaming * _names;

    HitsInserter<TrackPoint> _tpInserter;

    /// Updates `_namingCache` on calibration change
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
public:
    APVConjugateClusters( calib::Dispatcher & ch
                        , const std::string & only
                        , ObjPool<TrackPoint> & obTP );
    virtual AbstractHandler::ProcRes process_event( Event * ) override;
};

}
}

