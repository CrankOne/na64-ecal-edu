#include "na64dp/abstractHitHandler.hh"

#include "na64util/TDirAdapter.hh"

namespace na64dp {
namespace handlers {

/**\brief Removes ambigiuos APV clusters from an event
*
* For APV detectors occupying the same DAQ channels (multiwired, multiplexed)
* assures that clusters being present occupies the unique set of DAQ channels.
*
* Disambiguity is reslved by removing the clusters with respect to some
* criterion.
* */

class APVRmAmbigClusters : public AbstractHitHandler<APVCluster>, public TDirAdapter {

public:

    /// Internal type for clusters references sorted w.r.t. DAQ channel ID

    typedef std::multimap<NA64DP_APVWireNo_t, PoolRef<APVCluster>> ClustersByValue;

private:

    /// Keeps temporary indexes created for an event
    std::map<DetID_t, ClustersByValue> _idx;
    /// Keeps a set of clusters to be removed
    std::set<PoolRef<APVCluster>> _falseClusters;
    /// Clusters, best for wires
    std::set<size_t> _bestClusters;

protected:

    /// Removes ambiguous clusters from current event, for certain detector

    void _collect_ambiguous_clusters( DetID_t, ClustersByValue & );

public:

    APVRmAmbigClusters( calib::Dispatcher & ch
                      , const std::string & only );


    /// At the end of event processing removes clusters being marked for
    /// deletion.

    virtual AbstractHandler::ProcRes process_event( Event * ) override;
    /// Fills cluster-by channel number cache.
    virtual bool process_hit( EventID
                            , DetID_t did
                            , APVCluster & cHit ) override;
};

}
}
