#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/mm-layout.h"
#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief Find hit clusters on the APV detectors planes
 *
 * This handler discovers sets of adjacent fired wires (hits) found in the APV
 * tracker detectors, joining them into clusters. This is known to be 1D
 * clusters that further may be united into track points.
 *
 * The "sparseness" parameter corresponds to maximum distance (in wire units)
 * without charge (no wires triggered) that may be between two triggered wires.
 * Effectively, `sparseness=0` will require that each cluster is contiguous
 * (wire belonging to each cluster were triggered one-by-one, without any
 * untriggered wire), `sparseness=1` will tolerate one wire being untriggered
 * and makes adjacent triggered wires to belong to a single cluster.
 *
 * The "minimum cluster width" puts strict limit below which clusters will not
 * be taken into account. This may dramatically affect performance, so it is
 * reasonable to keep it here (and not make it the subject of a dedicated
 * handler).
 *
 * \todo `onlyDetectors` parameter (currently writes warning if detector is not known)
 * \todo optimize it; one of the slowest handlers curretly
 * */
class APVFindClusters : public AbstractHitHandler<APVHit>{
private:
    const float _minClusterWidth
              , _minHitCharge
              , _maxSparseness;

    DetKin_t _gmKinID  ///< GEMs kin ID cache
           , _mmKinID  ///< MuMegas kin ID cache
           ;
    /// MM reverse connectivity map, filled by ctr
    std::map<APVStripNo_t, APVWireNo_t> _mmRealToDaqWNo;
protected:
    /// Internal container typedef for sorted hits in a single plane
    typedef std::map<APVWireNo_t, const APVHit *> SortedHits;
    /// Hits by plane, accumulated for current event.
    std::map<DetID_t, SortedHits> _hitsCache;
    /// Cluster allocator reference
    HitsInserter<APVCluster> _clusterInserter;

    /// Upon naming change, retrieves values for MM and GM kin IDs
    virtual void handle_update(const nameutils::DetectorNaming &) override;

    /// Discovers clusters on the planes
    virtual size_t _find_clusters( Event &, DetID, const SortedHits & );
    
    /// Returns true if provided hit seems to be part of cluster candidate
    virtual bool _possibly_belongs_to_cluster( const APVCluster &
                                             , APVStripNo_t physWireNo
                                             , const APVHit & ) const;
                                             
    /// For given cluster, shall calculate its center (in wire number units)
    virtual double _get_cluster_center( const APVCluster & ) const;
    
    /// For given cluster calculate its charge weighted position
    virtual double _get_cluster_wCenter( const APVCluster & ) const;

public:
    APVFindClusters( calib::Dispatcher & ch
                   , ObjPool<APVCluster> & bank
                   , const std::string & only
                   , float clusterMinWidth=1.
                   , float clusterHitCharge=0.
                   , float sparseness=0.
                   );
    /// An interface function, performs clusterization on given event.
    virtual AbstractHandler::ProcRes process_event( Event * e ) override;
    /// Fills hits cache
    virtual bool process_hit( EventID
                            , DetID_t
                            , APVHit & ) override;
};

}
}

