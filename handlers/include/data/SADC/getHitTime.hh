#pragma once

#include "na64dp/abstractHitHandler.hh"

#define CONST_CHANNEL_TO_ns 12.5 

namespace na64dp {
namespace handlers {

/**\brief 

 * 
 * */
class SADCGetTimeHit : public AbstractHitHandler<SADCHit> {
private:
    bool _conversTot0nsFlag,
         _halfHeightFlag;
public:
    SADCGetTimeHit( calib::Dispatcher & ch
                  , const std::string & only
                  , bool conversTot0nsFlag=false
                  , bool halfHeightFlag=false

                  ) : AbstractHitHandler<SADCHit>(ch, only)
                    , _conversTot0nsFlag(conversTot0nsFlag)
                    , _halfHeightFlag(halfHeightFlag)      
                    {}
                    
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit & currentHit);
};

}
}
