#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Generates a SADC waveform function for given energy
 *
 * This handler performs a "backward" conversion, generating a waveform
 * function from SADC hit's energy. Ususally used as digitization handler for
 * MC applications.
 *
 * \todo Does no real job for so far, just testing of MC routines...
 * */
class EnergyToWf : public AbstractHitHandler<SADCHit> {
public:
    EnergyToWf( calib::Dispatcher & cdsp
              , const std::string & selection )
                            : AbstractHitHandler<SADCHit>(cdsp, selection) {}

    /// Generates a waveform for given hit (overwiriting existing)
    virtual bool process_hit( EventID eid
                            , DetID_t did_
                            , SADCHit & currentHit) override;
};

}
}

