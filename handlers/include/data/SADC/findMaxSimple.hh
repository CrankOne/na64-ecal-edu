#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Finds maximum sample in SADC waveform
 *
 * This is simple straightforward max finder. Iterates over 32 samples finding
 * the greatest amplitude and writes this value and corresponding sample number
 * to `maxValue` and `maxSample` fields of SADC hit.
 *
 * YAML config only accepts standard detector-by-name selection argument.
 * */
class SADCFindMaxSimple : public AbstractHitHandler<SADCHit> {
public:
    SADCFindMaxSimple( calib::Dispatcher &  ch
                     , const std::string & only )
                            : AbstractHitHandler<SADCHit>(ch, only) {}
    ~SADCFindMaxSimple(){}

    /// Locates maximum sample -- value and closes sample number, filling the
    /// hit's `maxValue` and `maxSample` fields.
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & hit ) override;
};

}
}

