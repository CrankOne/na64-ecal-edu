#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64event/event.hh"
#include "na64util/numerical/natBreaks.hh"
#include "na64util/str-fmt.hh"

#include <stdexcept>
#include <TArrayF.h>
#include <TFile.h>

namespace na64dp {
namespace handlers {

/**\brief Applies Jenks natural breaks classification procedure by getter func
 *
 * \todo Results output is currently not implemented (requires some work with
 * naming stuff).
 * \todo Handler is nor registered. Probably, procedure has to be the same as
 * for `HitDiscriminate` processor
 * */
template<typename HitT>
class JenksClassify : public AbstractHitHandler<HitT> {
private:
    /// Number of breaks to be applied
    const int _nClasses;
    /// Value getter to classify
    typename EvFieldTraits<HitT>::ValueGetter _getter;
    /// Container of values to be filled.
    std::vector<double> _values;
public:
    JenksClassify( calib::Dispatcher & cdsp
                 , const std::string & only
                 , typename EvFieldTraits<HitT>::ValueGetter getter
                 , int nClasses ) : AbstractHitHandler<HitT>(cdsp, only)
                                  , _nClasses(nClasses)
                                  , _getter(getter) {
        assert( getter );
    }

    virtual bool process_hit( EventID
                            , DetID_t
                            , HitT & currentEcalHit) override {
        _values.push_back( _getter(currentEcalHit) );
        return true;
    }

    virtual void finalize() override {
        using namespace na64dp::util::jenks;
        msg_debug( this->log()
                 , "Read %zu events for Jenks classifier %p"
                 , _values.size(), this );
        if( _values.size() < (size_t) _nClasses ) {
            NA64DP_RUNTIME_ERROR( "Nothing to classify." );
        }
        msg_debug( this->log()
                 , "Sorting and deduplicating values for Jenks classifier %p"
                 , this );
        ValueCountPairContainer sortedUniqueValueCounts;
        GetValueCountPairs( sortedUniqueValueCounts
                                       , &_values[0]
                                       , _values.size() );
        msg_debug( this->log()
                 , "%zu unique values retained for Jenks classifier %p"
                 , sortedUniqueValueCounts.size(), this );
        if( sortedUniqueValueCounts.size() < (size_t) _nClasses ) {
            NA64DP_RUNTIME_ERROR( "Nothing to classify." );
        }
        msg_debug( this->log()
                 , "Finding Jenks breaks for Jenks classifier %p"
                 , this );
        LimitsContainer resultingbreaksArray;
        ClassifyJenksFisherFromValueCountPairs( resultingbreaksArray
                                                           , _nClasses
                                                           , sortedUniqueValueCounts );
        TArrayF arr(resultingbreaksArray.size());
        size_t n = 0;
        for( double breakValue : resultingbreaksArray ) {
            arr[n++] = breakValue;
        }

        # if 1
        NA64DP_RUNTIME_ERROR( "TODO: Jenks classify results output" );
        # else
        TDirectory * jenksDir[3] = { (TDirectory *) gFile->Get("jenksBreaks"), NULL, NULL };
        if( !jenksDir[0] ) {
            jenksDir[0] = gFile->mkdir("jenksBreaks");
            jenksDir[1] = jenksDir[0]->mkdir("preshower");
            jenksDir[2] = jenksDir[0]->mkdir("main");
        } else {
            jenksDir[1] = (TDirectory *) jenksDir[0]->Get("preshower");
            jenksDir[2] = (TDirectory *) jenksDir[0]->Get("main");
        }
        if( _id.is_preshower() ) {
            jenksDir[1]->cd();
        } else {
            jenksDir[2]->cd();
        }
        char bf[128];
        snprintf( bf, sizeof(bf), "breaks_%s_%d_%d_%d"
                , _valueName.c_str()
                , _id.get_x(), _id.get_y(), _id.is_preshower() ? 0 : 1 );
        gDirectory->WriteObject( &arr, bf );
        # endif
    }
};

}  // namespace handlers
}  // namespace na64dp

