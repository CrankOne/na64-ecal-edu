#pragma once
#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

//extern bool (* const gComparators[4])(double, double);

/**\brief Hits template discriminator
 *
 * This is a template class with its derived implementations being registered
 * in a global registry with names: "SADCHitDiscriminate" and
 * "APVHitDiscriminate". It relies on `ValueGetter` of the `EvFieldTraits` covering
 * majority of cases of trivial hits and/or event discrimination.
 *
 * Utilizes value getter to remove certain hits from event or the entire event,
 * if value satisfies certain criteria. Criteria check is done by following
 * algorithm:
 *  1. Using choosen getter, some value is taken from hit
 *  2. Depending on string provided as an argument, the comparator will be
 *  choosen (`>=`, `>`, `<=`, `<`, and their corresponding strings `ge`, `gt`,
 *  `le`, `lt`). Value will be checked within the expression `value ? bound`
 *  (i.e. `value <= bound` if `comp` is `"<="`).
 *  3. 
 *
 * \todo Shortened form, like 'sum >= 14000'
 * */
template<typename HitT>
class HitDiscriminate : public AbstractHitHandler<HitT> {
private:
    typename EvFieldTraits<HitT>::ValueGetter _vGetter;
    bool (*_comparator)(double, double);
    double _bound;
    bool _removeHit
       , _discriminateEvent
       , _abruptHitsProcessing;
public:
    /**\brief Constructs versatile hit discriminating handler
     *
     * \param getter Value getter callback
     * \param comparator Value comparison callback
     * \param removeHit If hit will be removed from event if doesn't satisfy
     * \param discriminateEvent Event won't be propagated further by pipeline
     * \param abruptHitProcessing no hits within current event of this type will be considered by this handler
     * */
    HitDiscriminate( calib::Dispatcher & dsp
                   , typename EvFieldTraits<HitT>::ValueGetter getter
                   , bool (*comparator)(double, double)
                   , double bound
                   , bool removeHit
                   , bool discriminateEvent
                   , bool abruptHitProcessing
                   , const std::string & only
                   ) : AbstractHitHandler<HitT>(dsp, only)
                     , _vGetter( getter )
                     , _comparator(comparator)
                     , _bound(bound)
                     , _removeHit( removeHit )
                     , _discriminateEvent( discriminateEvent )
                     , _abruptHitsProcessing(abruptHitProcessing) {
        assert( getter );
        assert( comparator );
    }
   
    /// Considers a hit of certain type within the event
    virtual bool process_hit( EventID
                            , DetID_t
                            , HitT & currentHit ) {
        assert(_comparator);
        bool satisfies = _comparator( _vGetter(currentHit), _bound );
        if( satisfies ) {
            this->_set_hit_erase_flag();
            if( _discriminateEvent ) {
                // current event must not be propagated further by the pipeline
                this->_set_event_processing_result( AbstractHandler::kDiscriminateEvent );
            }
            if( _abruptHitsProcessing ) {
                // abrupt hit processing within the current event
                return false;
            }
        }
        return true;
    }
};

}
}

