#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH3.h>

namespace na64dp {
namespace handlers {

/**\brief A per-hit histogramming template handler.
 *
 * A template handler class producing 1D histograms for each detector entity of
 * certain hit. Depicts values acquired with _value getter_. The value getter
 * callbacks are described in details in corresponding
 * [section of related page](\ref HitValueGetters).
 *
 * \image html handlers/Histogram1D-example-1.png
 * \image html handlers/Histogram1D-example-2.png
 *
 * Histograms then will be stored in common TFile within corresponding
 * directories (`TDirectory`) as described in
 * [related docs of TDirAdapter](\ref TDirAdapterDetails).
 *
 * \code{.yaml}
 * - name: SADCHitHistogram1D
 *       value: sum
 *       histName: "linear-sum"
 *       histDescr: "Linear waveform sum approximation"
 *       nBins: 500
 *       range: [0, 5000]
 * \endcode
 *
 * This is the template class (as its mating two-dimensional version, the
 * `HitHistogram2D`), so its implementation provided in header while
 * template code instantiation is performed in
 * [virtual constructor](\ref VirtualCtr), in conjugated `.cc` file.
 *
 * \note For 2D generalization, see `HitHistogram2D` template.
 * \see HitHistogram2D
 * \see TDirAdapter
 * */
template<typename HitT>
class HitHistogram3D : public AbstractHitHandler<HitT>
                     , public TDirAdapter {
private:
    /// Getters for axes in 3-dimensional space
    typename EvFieldTraits<HitT>::ValueGetter _getters[3];
    /// Number of bins by correspondingaxes
    Int_t _nBins[3];
    /// Min/max boundaries of histogram, by corresponding axes
    Double_t _mins[3], _maxs[3];
    const std::string _hstBaseName  ///< Histogram base name suffix
                    , _hstDescription;  ///< Histogram common description
    /// Index of histograms by unique detector ID
    std::map<DetID_t, TH3F *> _histograms;
public:
    /**\brief Generic ctr for getter-based 
     *
     * \param ch Calibration handle instance
     * \param getter1 A value getter #1 (x-axis), retrieving certain value from the hit
     * \param getter2 A value getter #2 (y-axis), retrieving certain value from the hit
     * \param getter3 A value getter #3 (z-axis), retrieving certain value from the hit
     * \param hstBaseName A string used to build unique histogram name
     * \param hstDescription A common description for histograms created by this handler
     * \param nBins1 Number of bins within the histogram x axis
     * \param min1 Lower bound of histogram's range over x axis
     * \param max1 Upper bound of histogram's range over x axis
     * \param nBins2 Number of bins within the histogram y axis
     * \param min2 Lower bound of histogram's range over y axis
     * \param max2 Upper bound of histogram's range over y axis
     * \param nBins3 Number of bins within the histogram z axis
     * \param min3 Lower bound of histogram's range over z axis
     * \param max3 Upper bound of histogram's range over z axis
     * */
    HitHistogram3D( calib::Dispatcher & cdsp
                  , const std::string & only
                  , typename EvFieldTraits<HitT>::ValueGetter getter1
                  , typename EvFieldTraits<HitT>::ValueGetter getter2
                  , typename EvFieldTraits<HitT>::ValueGetter getter3
                  , const std::string & hstBaseName
                  , const std::string & hstDescription
                  , Int_t nBins1, Double_t min1, Double_t max1
                  , Int_t nBins2, Double_t min2, Double_t max2
                  , Int_t nBins3, Double_t min3, Double_t max3
                  ) : AbstractHitHandler<HitT>(cdsp, only)
                    , TDirAdapter(cdsp)
                    , _getters{getter1, getter2, getter3}
                    , _nBins{nBins1, nBins2, nBins3}
                    , _mins{min1, min2, min3}
                    , _maxs{max1, max2, max3}
                    , _hstBaseName(hstBaseName)
                    , _hstDescription(hstDescription) 
                    {
        assert(getter1);
        assert(getter2);
        assert(getter3);
        char axes[] = "XYZ";
        for( auto i = 0; i < 3; ++i ) {
            if( _mins[i] >= _maxs[i]
             || std::isnan(_mins[i]) || std::isinf(_mins[i])
             || std::isnan(_maxs[i]) || std::isinf(_maxs[i]) ) {
                NA64DP_RUNTIME_ERROR( "Bad value range for axis %c of"
                        " 3D histogram: [%e:%e]"
                        , axes[i]
                        , _mins[i]
                        , _maxs[i] );
            }
            if( _nBins[i] < 2 ) {
                NA64DP_RUNTIME_ERROR( "Bad bin count for axis %c of"
                        " 3D histogram: %d"
                        , axes[i], _nBins[i] );
            }
        }
    }
    
    /// Deletes histogram instances allocated on heap
    ~HitHistogram3D() {
        for(auto p : _histograms) {
            delete p.second;
        }
    }

    /// For certain detector entity, fills histogram with certain hit value
    virtual bool process_hit( EventID
                            , DetID_t did_
                            , HitT & currentHit ) override {
        DetID_t did = EvFieldTraits<HitT>::uniq_detector_id(did_);
        auto it = _histograms.find(did);
        if( _histograms.end() == it ) {
            assert( _histograms.size() < 1000 );
            // No histogram entry exists for current detector entity -- create
            // and insert one
            std::string hstName = _hstBaseName;
            dir_for(did, hstName)->cd();
            TH3F * newHst = new TH3F( hstName.c_str()
                                    , _hstDescription.c_str()
                                    , _nBins[0], _mins[0], _maxs[0]
                                    , _nBins[1], _mins[1], _maxs[1]
                                    , _nBins[2], _mins[2], _maxs[2]);
            auto ir = _histograms.emplace(did, newHst);  // `first' is an iterator
            assert( ir.second );
            it = ir.first;
        }
        // fill bin content in the histogram
        it->second->Fill( _getters[0](currentHit)
                        , _getters[1](currentHit)
                        , _getters[2](currentHit)  );
        return true;
    }

    /// Writes histogram into current TFile
     virtual void finalize() {
        for( auto idHstPair : _histograms ) {
            tdirectories()[idHstPair.first]->cd();
            idHstPair.second->Write();
        }
    }
};

}
}

 
