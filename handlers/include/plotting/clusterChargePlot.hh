#pragma once

#include "na64sw-config.h"

#ifdef ROOT_FOUND

#include "plotting/cluster1DHistogram.hh"

namespace na64dp {
namespace handlers {

/**\brief 1D histogram depicting cluster charge distribution
 *
 * Uses `Cluster1DHistogram` ancestor class to create histograms and implements
 * it value-filling method in order to populate histogram with cluster charge in
 * certain projection defined by detector plane.
 *
 * YAML config snippet:
 *
 * \code{.yaml}
 *     - name: ClusterChargePlot
 *       histName: "clusterCharge"
 *       histDescr: "APV hits clusters charge distribution"
 *       nBins: 100
 *       range: [0, 2000???] 
 * \endcode
 *
 * Resulting histogram reflects distribution of clusters of certain charge.
 * \image html handlers/apv-cluster-charge.png
 * */
class ClusterChargePlot : public Cluster1DHistogram {
protected:
    void _fill_cluster_value(const APVCluster &, TH1F *) override;
public:
    ClusterChargePlot( calib::Dispatcher &
                    , const std::string & hstName
                    , const std::string & hstDescr
                    , Int_t nBins, Float_t hstMin, Float_t hstMax );
};

}
}
#endif

