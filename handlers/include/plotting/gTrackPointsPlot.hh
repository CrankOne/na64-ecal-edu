#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

class TH2F;  // fwd

namespace na64dp {
namespace handlers {

/**\brief Puts track points on 2D histogram.
 * */
class GTrackPointsPlot : public AbstractHandler
                      , public TDirAdapter
                      , public SelectiveHitHandler
                      {
private:
    const std::string _hstBaseName
                    , _hstDescription
                    ;
    Int_t _nBins[2];
    Float_t _ranges[2][2];
    /// Histograms indexed by detector ID (unique per plane)
    std::map<DetID_t, TH2F *> _hists;
    std::string _hstPathTemplate;
protected:
    virtual bool _process_track_point( TrackPoint & tp );
public:
    GTrackPointsPlot( calib::Dispatcher & ch
                   , const std::string & hstBaseName
                   , const std::string & hstDescription
                   , Int_t nBinsX, Float_t rangeXMin, Float_t rangeXMax
                   , Int_t nBinsY, Float_t rangeYMin, Float_t rangeYMax
                   , const std::string & hstPathTemplate
                   , const std::string & selection
                   );
    /// Deletes histogram objects allocated on heap
    ~GTrackPointsPlot();
    /// Fills the 2D track points histograms
    virtual ProcRes process_event( Event * e ) override;
    /// Writes histograms
    virtual void finalize() override;
};

}
}

