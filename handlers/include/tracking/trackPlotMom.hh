#pragma once

#include "na64sw-config.h"

#ifdef GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH1D.h>
#include <TH2D.h>

namespace genfit {  // fwd decls
class AbsTrackRep;
class AbsKalmanFitter;
}

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackPlotMom : public AbstractHitHandler<TrackPoint>
                      , public TDirAdapter {

private:
    /// Track allocator reference
    ObjPool<Track> & _tracksBank;
    /// Momentum resolution
    TH1D *hMom;
    TH2D *hMomVsCal;
    TH1D *hMomVsECal;
    TH1D *hMomVsChi;
    
public:
    TrackPlotMom( calib::Dispatcher & dsp
                  , const std::string & only
                  , ObjPool<Track> & bank
                  );
                  
    virtual ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , TrackPoint & hit ) override {assert(false);}
                            
    virtual void finalize() override;
};

}
}

#endif  //  defined(GenFit_FOUND)


