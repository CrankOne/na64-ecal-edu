#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for search of tp with highest charge
 * */
class TPBestChargeAPV : public AbstractHitHandler<TrackPoint> {
public:
	typedef std::multimap<double, PoolRef<TrackPoint>> TPByCharge;

private:

	std::map<DetID_t, TPByCharge> _idx;
	
	std::set<size_t> _bestTPs;

public:
    TPBestChargeAPV( calib::Dispatcher & ch
                   , const std::string & only
                   , ObjPool<TrackPoint> & obTP );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , TrackPoint & ) override;
};

}
}
