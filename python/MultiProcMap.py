import os
import subprocess
from functools import partial
import multiprocessing

"""
A common routines to run multiple process instances of the `na64dp-pipe`
process on a single host.
"""

def _run_sub(jd, cmdTemplate):
    """
    Subprocessing callback for MultiProcMap.
    """
    cmd = []
    for n, tok in enumerate(cmdTemplate):
        if type(tok) is not str:
            raise RuntimeError( 'Argument #%d is of type %s'%(n, type(tok)) )
        cmd.append( tok.format(**jd) )
    return subprocess.call(cmd, shell=False)

class MultiProcMap(object):
    """
    Shared state representation for multiple processes.
    Initialize it with command template -- a list of strings with Python's
    `format()` fillers (`{}`) and call `run()` with iterable object describing
    values for this format. Example:

        def _tst_gen( lower, upper ):
            for i in range(lower, upper):
                yield { 'number' : '#%d'%i }

        MultiProcMap(*('echo {number}'.split())).run( _tst_gen(3, 19), nJobs=5 )

    """

    def __init__( self, *args ):
        assert(args)
        self.f = partial( _run_sub, cmdTemplate=args )

    def run(self, jobsDescriptions, nJobs=None):
        """
        Runs multiple subprocesses of same executable assuming the
        `jobsDescIterable` to be an iterable object (generator, list, etc) of
        descriptions.
        """
        pool = multiprocessing.Pool(
                multiprocessing.cpu_count() if not nJobs else nJobs )
        return pool.map( self.f, jobsDescriptions )


def _run_sub_with_ports(jd, cmdT, ports=None):
    """
    Subprocessing callback for SharedPortsMultiProc.
    """
    cmd = []
    if 'portNo' in jd:
        raise RuntimeError('Special variable "portNo" provided in job'
                ' description.')
    if ports is not None:
        myPortNo = None
        for portNo in ports.keys():
            if ports[portNo] is None:
                myPortNo = portNo
                ports[portNo] = os.getpid()
                break
        assert(myPortNo is not None)
        jd['portNo'] = myPortNo

    for n, tok in enumerate(cmdT):
        if type(tok) is not str:
            raise RuntimeError( 'Argument #%d is of type %s'%(n, type(tok)) )
        cmd.append( tok.format(**jd) )
    sc = subprocess.call(cmd, shell=False)

    if 'portNo' in jd:
        ports[jd['portNo']] = None

    return sc

class SharedPortsMultiProc(object):
    """
    Shares set of reentrant port numbers among subprocesses pool.

    def _jds( lower, upper ):
        for i in range(lower, upper):
            yield {'number' : '#%d'%i}

    MultiPipe(*('echo {number} {portNo}'.split())).run( _jds(5, 115), ports=[101, 102, 103] )
    """

    def __init__( self, *args ):
        assert(args)
        self.args = args

    def run(self, jobsDescriptions, nJobs=None, ports=None):
        """
        Runs multiple subprocesses of same executable assuming the
        `jobsDescIterable` to be an iterable object (generator, list, etc) of
        descriptions.
        """
        if ports and nJobs:
            assert( nJobs == len(ports) )
        elif ports:
            nJobs = len(ports)

        mgr = multiprocessing.Manager()
        if ports:
            portsD = mgr.dict()
            for portNo in ports:
                portsD[portNo] = None
        else:
            portsD = None
        self.f = partial( _run_sub_with_ports, cmdT=self.args, ports=portsD )

        pool = multiprocessing.Pool(
                multiprocessing.cpu_count() if not nJobs else nJobs )
        return pool.map( self.f, jobsDescriptions )
