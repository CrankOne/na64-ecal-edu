# A short script to test binary encoded event ID fields layout

# Layout
layout = {
    "event" : 30,
    "spill" : 12,
    "resrvd": 1,
    "run"   : 21,
}

sLen = 0
mskCtrl = 0
for fName, v in layout.items():
    mx = 2**v
    mask = (mx - 1) << sLen
    print( fName, 'max=%d'%mx, 'offset=%d'%sLen, 'mask=%#x'%mask, ' ({0:b})'.format(mask) )
    sLen += v
    mskCtrl |= mask
print( 'Control:', sLen, '%#x'%mskCtrl, '{0:b}'.format(mskCtrl) )


