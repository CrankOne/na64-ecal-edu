@0xa73039c41d4d74cb;

using Cxx = import "/capnp/c++.capnp";
$Cxx.namespace("capnp::na64dp");

struct SADCFittingData {
    chisq @0 :Float64;  # $\chi^2/N_{df}$
    rms @1 :Float64;  # RMS of fitting
    parameters :union {
        moyal :group {  # parameters for Moyal function fitting the waveform
            area @2 :Float64;
            maximum @3 :Float64;
            width @4 :Float64;
            errArea @5 :Float64;
            errMaximum @6 :Float64;
            errWidth @7 :Float64;
            chisq @8 :Float64;
        }
        none @9 :Void;  # XXX, when more fitting functions appear
        # ... more fitting parameters here?
    }
}

# SADC detector hit. Raw digit data is originally given present by 32 digit
# samples (of type unsigned short). Samples are generated with two sampling
# ADC each working on 40 MHz frequency. I.e. overall sampling frequency is
# 80 MHz and we need two corrsponding pedestal (zero) values.
struct SADCHit {
    detectorID @0 : UInt32;  # identifies a detector
    wave @1 :List(Float64);  # 32 samples
    sum @2 :Float64;  # sum of samples of 32 samples above
    eDep @3 :Float64; # energy deposition
    maxSample @4 :Float64;  # maximum position (0-32)
    maxValue @5 :Float64;  # maximum value
    pedestalOdd @6 :Float64;  # pedestal value for odd (1, 3...31) samples
    pedestalEven @7 :Float64;  # pedestal value for odd (0, 2...30) samples
    fittingData @8 :SADCFittingData;
}

struct APVHit {
    detectorID @0 : UInt32;  # identifies a detector and wire number
    wireNo @1 : UInt32;
    rawSample0 @2 : UInt32;
    rawSample1 @3 : UInt32;
    rawSample2 @4 : UInt32;
    channelNo @5 : UInt32;
    a02 @6 : Float64;
    a12 @7 : Float64;
    t02 @8 : Float64;
    t12 @9 : Float64;
    t02Sigma @10 : Float64;
    t12Sigma @11 : Float64;    
    averageT @12 : Float64;
    maxCharge @13 : Float64;
    time @14 : Float64;
    timeError @15 : Float64;
  
}

struct StwTDCHit {
    detectorID @0 : UInt32;  # identifies a detector
    position @01: Float64;
}

struct APVCluster {
    position @0 : Float64;
    wPosition @1 : Float64;
    charge @2 : Float64;
    sparseness @3 : Float64;
    time @4: Float64;
    timeError @5: Float64;

    hitRefs @6 : List(UInt32);
}

struct TrackClusterRef {
    detectorID @0 : UInt32;
    clusterNo @1 : UInt32;
}

struct TrackPoint {
    localR0 @0 : Float64;
    localR1 @1 : Float64;
    localR2 @2 : Float64;
    globalR0 @3 : Float64;
    globalR1 @4 : Float64;
    globalR2 @5 : Float64;

    clusterRefs @6 : List(TrackClusterRef);

    stationID @7 : UInt32;
}

struct Track {
    points @0 : List(TrackPoint);
}

struct Event {
    id @0 :UInt64;
    sadcHits @1 :List(SADCHit);
    apvHits @2 :List(APVHit);
    stwtdcHits @3 :List(StwTDCHit);
    clusters @4 :List(APVCluster);
    tracks @5 :List(Track);
}

