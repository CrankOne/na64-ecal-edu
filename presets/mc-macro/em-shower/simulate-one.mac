#               ## Em Shower Interactive Simulation Macro ##
#
# This macro will perform controlled simulation of a single event for dev/debug
# purposes. The testing fixture is a cylindric heterogeneous calorimeter
# defined in corresponding GDML document.
#                                                                     ________
#___________________________________________________________________/ Pre-init
/na64sw/setHEPRandomSeed 1337
/na64sw/physics/useList QGSP_BERT
#/na64sw/physics/useList FTFP_BERT
#/na64sw/physics/useList local
/na64sw/usePGA GPS
/na64sw/checks/useEDepByMatSteppingAction  # TODO: command will change

/geode/validate false
/geode/checkForOverlaps true
/na64sw/gdmlAuxInfo/enable export
/na64sw/gdmlAuxInfo/enable sensitive-detector
/na64sw/gdmlAuxInfo/sd/create EmCylCaloTest /na64sw-sd/calo/emTestCyl
#/na64sw/gdmlAuxInfo/sd/create StepsDump /na64sw-sd/calo/emTestDump
/na64sw/gdmlAuxInfo/enable style
/na64sw/gdmlAuxInfo/style/setDefault "surface #663333aa"  # Note: oktothorp correctly passes here
# Setup the "Calorimeter" mass world with "CylReadout" readout
/geode/useSetup CylReadout readoutWorld
#/geode/useSetup StepsDump readoutWorld
/geode/useSetup Calorimeter
/geode/useGDMLFile ../geomlib/gdml/fixtures/emshower.gdml
/na64sw/physics/createParallelPhysics
/na64sw/openROOTFile em-fixture-cyl.root
#                                                           ___________________
#_________________________________________________________/ E/M Shower Options
#/run/setCut 0.01 mm
# and/or, for certain particle
#/run/setCutForAGivenParticle e- 0.1 mm
# and/or, limit by energy (default is 990 eV)
#/cuts/setLowEdge 50 eV

# Enable/disable energy loss fluctuations
#/process/eLoss/fluct true
# Enable/disable CSDA range calculation
#/process/eLoss/CSDARange
# Enable/disable LPM effect calculation
#/process/eLoss/LPM

#/process/em/deexcitationIgnoreCut true
#/process/eLoss/UseAngularGenerator true
#/process/eLoss/UseICRU90 true
#/process/em/lowestElectronEnergy 2 eV
#/process/em/lowestMuHadEnergy 100 keV

#                                                               ______________
#_____________________________________________________________/ INITIALIZATION
/run/initialize
/na64sw/gdmlAuxInfo/style/apply
/vis/open OGL 600x600-0+0
/vis/viewer/set/autoRefresh false
/vis/verbose errors
/vis/drawVolume
/vis/viewer/set/viewpointVector -1 0 0
/vis/viewer/set/lightsVector -1 0 0
/vis/scene/add/trajectories smooth
#
#/vis/filtering/trajectories/create/attributeFilter fltr1
#/vis/filtering/trajectories/fltr1/setAttribute IKE
#/vis/filtering/trajectories/fltr1/addInterval 10 GeV 100 GeV
#
/vis/scene/add/scale   # Simple scale line
/vis/scene/add/axes    # Simple axes: x=red, y=green, z=blue.
/vis/scene/add/eventID # Drawn at end of event
/vis/viewer/set/viewpointThetaPhi 120 150

/vis/viewer/set/autoRefresh true
/vis/verbose warnings
/vis/viewer/set/projection p
/vis/viewer/set/background white
/vis/viewer/set/hiddenMarker true
#                                                              ________________
#____________________________________________________________/ PARTICLE SOURCE
/gps/verbose 2
/gps/particle e-
/gps/pos/type Plane
/gps/pos/shape Circle
/gps/pos/radius 1 mm
/gps/pos/centre 0 0 -228.75 mm  # for 150 lyrs
#/gps/pos/centre 0 0 -15.25 mm  # for 10 lyrs
/gps/ang/type beam1d	
/gps/ang/sigma_r 0.1 deg
/gps/ang/rot1 0 1 0
/gps/ang/rot2 1 0 0
/gps/ene/type Gauss
/gps/ene/sigma 0.1 MeV
/gps/ene/mono 1 GeV
#                                                                          ____
#________________________________________________________________________/ RUN
#/geometry/navigator/check_mode false
#/geometry/test/run
/run/beamOn 1
/na64sw/checks/printEdepByMaterial
/na64sw/checks/resetEDepByMatCounters
#                                                                      _______
#____________________________________________________________________/ Cleanup
/na64sw/closeROOTFile

