#include "na64mc/g4api/physicsList.hh"
#include "na64mc/g4api/processes/stepMaxLimiter.hh"
#include "na64mc/g4api/phLists/emShower.hh"

#include <G4RunManager.hh>
#include <G4ProcessManager.hh>

#include <G4EmStandardPhysics.hh>
#include <G4StoppingPhysics.hh>
#include <G4EmStandardPhysics_option1.hh>
#include <G4EmStandardPhysics_option2.hh>
#include <G4EmStandardPhysics_option3.hh>
#include <G4EmStandardPhysics_option4.hh>
#include <G4EmStandardPhysicsWVI.hh>
#include <G4EmStandardPhysicsGS.hh>
#include <G4EmStandardPhysicsSS.hh>

#include "G4DecayPhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "G4IonPhysics.hh"
#include "G4StoppingPhysics.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4NeutronTrackingCut.hh"
#include <G4HadronPhysicsQGSP_BERT.hh>

#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmLowEPPhysics.hh"

#include "G4EmDNAPhysics.hh"
#include "G4EmDNAPhysics_option2.hh"
#include "G4EmDNAPhysics_option4.hh"
#include "G4EmDNAPhysics_option6.hh"

#include "G4HadronElasticPhysics.hh"

// particles
#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"
#include "G4DNAGenericIonsManager.hh"

#include <QGSP_BERT.hh>
#include <FTFP_BERT.hh>
#include <QBBC.hh>


#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

UIModule<G4VModularPhysicsList>::UIModule( const G4String & rootPath
                                         , ModularConfig & cfg
                                         ) : GenericG4Messenger(rootPath)
                                           , _objPtr(nullptr)
                                           , _cfg(cfg) {
    cmd<G4String>( "usePhysicsList"
                 , "Creates a Geant4 physics list instance of one of predefined"
                   " types: \"local\" is the default for this library and provides"
                   " methods to add or remove various physics at a runtime."
                 , "typeName"
                 , "local"
                 , UIModule<G4VModularPhysicsList>::ui_cmd_use
                 , preinit
                 )
    .cmd<G4int>( "setVerbose"
               , "Sets verbosity of physics list factory."
               , "val"
               , UIModule<G4VModularPhysicsList>::ui_cmd_set_verbose
               , anyAppState )
    .cmd_nopar( "printAvailablePhysicsLists"
              , "Prints names of available reference phys. lists."
              , UIModule<G4VModularPhysicsList>::ui_cmd_list
              , anyAppState )
    .cmd_nopar( "printAvailablePhysicsListsEM"
              , "Prints names of available reference phys. lists' EM options."
              , UIModule<G4VModularPhysicsList>::ui_cmd_list_em
              , anyAppState )
    ;
}

void
UIModule<G4VModularPhysicsList>::ui_cmd_use( GenericG4Messenger * msgr_
                                           , const G4String & name ) {
    UIModule<G4VModularPhysicsList> & thisModule
        = dynamic_cast<UIModule<G4VModularPhysicsList> &>(*msgr_);
    if( thisModule.IsReferencePhysList(name) ) {
        thisModule._objPtr = thisModule.GetReferencePhysList(name);
    } else if( NA64SW_MC_LOCAL_PHYS_LIST_NAME == name ) {
        thisModule._objPtr = new PhysicsList( thisModule.GetRootPath() );
    } else {
        throw std::runtime_error( "Bad physics list name/identifier provided: "
                "\"" + name + "\"." );  // TODO: dedicated exception
    }
    thisModule._inUse = name;
    G4RunManager::GetRunManager()->SetUserInitialization(thisModule._objPtr);
}

void
UIModule<G4VModularPhysicsList>::ui_cmd_list( GenericG4Messenger * msgr_
                                            , const G4String & ) {
    UIModule<G4VModularPhysicsList> & thisModule
        = dynamic_cast<UIModule<G4VModularPhysicsList> &>(*msgr_);
    for( auto phLName : thisModule.AvailablePhysLists() ) {
        G4cout << " * \"" << phLName << "\"" << G4endl;
    }
    G4cout << " * \"" NA64SW_MC_LOCAL_PHYS_LIST_NAME "\"" << G4endl;
}

void
UIModule<G4VModularPhysicsList>::ui_cmd_list_em( GenericG4Messenger * msgr_
                                               , const G4String & ) {
    UIModule<G4VModularPhysicsList> & thisModule
        = dynamic_cast<UIModule<G4VModularPhysicsList> &>(*msgr_);
    for( auto phLName : thisModule.AvailablePhysListsEM() ) {
        G4cout << " * \"" << phLName << "\"" << G4endl;
    }
}

void
UIModule<G4VModularPhysicsList>::ui_cmd_set_verbose( GenericG4Messenger * msgr_
                                                   , const G4String & strExpr ) {
    UIModule<G4VModularPhysicsList> & thisModule
        = dynamic_cast<UIModule<G4VModularPhysicsList> &>(*msgr_);
    thisModule.SetVerbose( G4UIcmdWithAnInteger::GetNewIntValue(strExpr.c_str()) );
}

#if 0
G4VModularPhysicsList *
UIModuleTraits<G4VModularPhysicsList>::instantiate( const G4String & name 
                                                  , ModularConfig & msgr
                                                  ) {
    auto & thisModule = msgr.get_module<G4VModularPhysicsList>();
    if( thisModule.IsReferencePhysList(name) ) {
        return thisModule.GetReferencePhysList(name);
    } else if( NA64SW_MC_LOCAL_PHYS_LIST_NAME == name ) {
        return new PhysicsList( thisModule.GetRootPath() );
    }
    
    G4cout << "Available physics lists:" << G4endl;
    for( auto phLName : thisModule.AvailablePhysLists() ) {
        G4cout << " * \"" << phLName << "\"" << G4endl;
    }
    G4cout << " * \"" NA64SW_MC_LOCAL_PHYS_LIST_NAME "\"" << G4endl;
    G4cout << "Available EM options:" << G4endl;
    for( auto phLName : thisModule.AvailablePhysListsEM() ) {
        G4cout << " * \"" << phLName << "\"" << G4endl;
    }

    throw std::runtime_error( "Bad physics list name/identifier provided: "
            "\"" + name + "\"." );
}
#endif


PhysicsList::PhysicsList( const G4String & )
        : fEmPhysicsList(nullptr)
        , fDecayPhysics(nullptr)
        , fHadPhysicsList(nullptr) {
    #if 0
    SetVerboseLevel(1);

    // EM physics
    //fEmName = G4String("emstandard_opt4");
    //fEmPhysicsList = new G4EmStandardPhysics_option4();
    fEmName = G4String("local");
    fEmPhysicsList = new PhysListEmStandard();

    // Decay physics  
    fDecayPhysics = new G4DecayPhysics(1);

    //RegisterPhysics( new G4StoppingPhysics(1) );

    SetDefaultCutValue(.1*CLHEP::mm);
    #else
    int ver = 1;
    defaultCutValue = 0.7*CLHEP::mm;  
    SetVerboseLevel(ver);
    // EM Physics
    RegisterPhysics( fEmPhysicsList =
            // new G4EmStandardPhysics(ver)
            new G4EmStandardPhysics_option1()
        );
    // Synchroton Radiation & GN Physics
    RegisterPhysics( new G4EmExtraPhysics(ver) );
    // Decays
    RegisterPhysics( fDecayPhysics = new G4DecayPhysics(ver) );
    // Hadron Elastic scattering
    RegisterPhysics( new G4HadronElasticPhysics(ver) );
    // Hadron Physics
    //RegisterPhysics( new G4HadronPhysicsQGSP_BERT(ver));
    // Stopping Physics
    RegisterPhysics( new G4StoppingPhysics(ver) );
    // Ion Physics
    RegisterPhysics( new G4IonPhysics(ver));
    // Neutron tracking cut
    RegisterPhysics( new G4NeutronTrackingCut(ver));
    #endif
}

PhysicsList::~PhysicsList() {
    if( fEmPhysicsList ) delete fEmPhysicsList;
    if( fDecayPhysics ) delete fDecayPhysics;
    if( fHadPhysicsList ) delete fHadPhysicsList;
}

#if 1
void
PhysicsList::ConstructParticle() {
    #if 0
    G4BosonConstructor  pBosonConstructor;
    pBosonConstructor.ConstructParticle();

    G4LeptonConstructor pLeptonConstructor;
    pLeptonConstructor.ConstructParticle();

    G4MesonConstructor pMesonConstructor;
    pMesonConstructor.ConstructParticle();

    G4BaryonConstructor pBaryonConstructor;
    pBaryonConstructor.ConstructParticle();

    G4IonConstructor pIonConstructor;
    pIonConstructor.ConstructParticle();

    G4ShortLivedConstructor sLivedConstructor;
    sLivedConstructor.ConstructParticle();

    // Geant4-DNA
    G4DNAGenericIonsManager* genericIonsManager;
    genericIonsManager=G4DNAGenericIonsManager::Instance();
    genericIonsManager->GetIon("alpha++");
    genericIonsManager->GetIon("alpha+");
    genericIonsManager->GetIon("helium");
    genericIonsManager->GetIon("hydrogen");
    #else
    G4VModularPhysicsList::ConstructParticle();
    #endif
}

void
PhysicsList::ConstructProcess() {
    #if 0
    AddTransportation();
    fEmPhysicsList->ConstructProcess();
    fDecayPhysics->ConstructProcess();
    if( fHadPhysicsList ) {
        fHadPhysicsList->ConstructProcess();
    }
    add_step_max_limiter();
    #else
    G4VModularPhysicsList::ConstructProcess();
    #endif
}
#endif

void
PhysicsList::add_step_max_limiter() {
    // Step limitation seen as a process
    StepMax * stepMaxProcess = new StepMax();

    auto particleIterator = GetParticleIterator();
    particleIterator->reset();
    while( (*particleIterator)() ) {
        G4ParticleDefinition * particle = particleIterator->value();
        G4ProcessManager * pmanager = particle->GetProcessManager();

        if(stepMaxProcess->IsApplicable(*particle)) {
            pmanager->AddDiscreteProcess(stepMaxProcess);
        }
    }
}

void
PhysicsList::add_physics_ctr( const G4String& name ) {
    if( verboseLevel > -1 ) {
        log4cpp::Category & L = log4cpp::Category::getInstance(std::string("na64mc.Geant4API"));
        L << log4cpp::Priority::DEBUG
            << "PhysicsList::AddPhysicsList(\"" << name << "\")";
    }
    if (name == fEmName) return;
    if (name == "local") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new PhysListEmStandard(name);
    } else if (name == "emstandard_opt0") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmStandardPhysics();
    } else if (name == "emstandard_opt1") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmStandardPhysics_option1();
    } else if (name == "emstandard_opt2") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmStandardPhysics_option2();
    } else if (name == "emstandard_opt3") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmStandardPhysics_option3();
    } else if (name == "emstandard_opt4") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmStandardPhysics_option4();
    } else if (name == "emstandardSS") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmStandardPhysicsSS();
    } else if (name == "emstandardWVI") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmStandardPhysicsWVI();
    } else if (name == "emstandardGS") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmStandardPhysicsGS();
    } else if (name == "empenelope") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmPenelopePhysics();
    } else if (name == "emlowenergy") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmLowEPPhysics();
    } else if (name == "emlivermore") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmLivermorePhysics();
    } else if (name == "dna") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmDNAPhysics();
    } else if (name == "dna_opt2") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmDNAPhysics_option2();
    } else if (name == "dna_opt4") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmDNAPhysics_option4();
    } else if (name == "dna_opt6") {
        fEmName = name;
        delete fEmPhysicsList;
        fEmPhysicsList = new G4EmDNAPhysics_option6();
    } else if (name == "had_elastic" && !fHadPhysicsList) {
        fHadPhysicsList = new G4HadronElasticPhysics();
    } else {
        log4cpp::Category & L = log4cpp::Category::getInstance(std::string("na64mc.Geant4API"));
        L << log4cpp::Priority::ERROR
            << "PhysicsList::AddPhysicsList: \"" << name << "\""
            << " is not defined";
    }
}

}

//                                                               ______________
// ____________________________________________________________/ Physics lists
// I have copied the reference texts for these physics lists

#if 0
# define REGISTER_PHYSICS_LIST( clsName, desc )                     \
static G4VModularPhysicsList * _new_ ## clsName ## _ph_l_instance();      \
static bool _regResult_ ## clsName =                                \
    ::na64dp::VCtr::self().register_class<G4VModularPhysicsList>( # clsName, _new_ ## clsName ## _ph_l_instance, desc ); \
static G4VModularPhysicsList * _new_ ## clsName ## _ph_l_instance()

REGISTER_PHYSICS_LIST( QGSP_BERT
                     , "Like QGSP, but using Geant4 Bertini cascade for"
                       " primary protons, neutrons, pions and Kaons below"
                       " ~10GeV." ) {
    return new QGSP_BERT();
}

REGISTER_PHYSICS_LIST( FTFP_BERT
                     , "A different string model is used. The FTF model is"
                       " based on the FRITIOF description of string excitation"
                       " and fragmentation." ) {
    return new FTFP_BERT();
}

REGISTER_PHYSICS_LIST( QBBC, "QBBC standard physics list." ) {
    return new QBBC();
}

// ... TODO: other standard Geant4 physics lists

REGISTER_PHYSICS_LIST( local, "NA64SW customizable physics list." ) {
    return new mc::PhysicsList();
}
#endif

}
