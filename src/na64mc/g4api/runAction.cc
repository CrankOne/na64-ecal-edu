#include "na64mc/g4api/runAction.hh"
#include "na64mc/g4api/run.hh"

namespace na64dp {
namespace mc {

const std::string UIModuleTraits<G4UserRunAction>::command
    = "useRunAction";
const std::string UIModuleTraits<G4UserRunAction>::description
    = "Creates a Geant4 user run action instance of one of predefined"
      " types: \"na64sw\" is the default for this library.";
const std::string UIModuleTraits<G4UserRunAction>::default_
    = "na64sw";

G4UserRunAction *
UIModuleTraits<G4UserRunAction>::instantiate( const G4String & name 
                                            , ModularConfig & cfg
                                            ) {
    if( "na64sw" != name )
        throw std::runtime_error( "Currently, only \"na64sw\" run"
                " action is supported" );
    auto & thisModule = cfg.get_module<G4UserRunAction>();
    return new RunAction( thisModule.GetRootPath()
                        , thisModule.manager
                        , cfg.notifier );
}


RunAction::RunAction( const G4String & rootPath
                    , calib::Manager & calibMgrPtr
                    , Notifier & nfr )
        : GenericG4Messenger(rootPath)
        , _calibMgr(calibMgrPtr)
        , _runNo(0)
        , _mcNotifier(nfr)
        {
    dir("runAction", "Options and commands related to the run action")
        .cmd<G4String>("setPipelineConfig"
                , "Sets the config file source for post-processing."
                , "filename"
                , ui_cmd_set_postprocessing_config
                )
        .cmd<G4String>("useMonitoring"
                , "Sets the online montiroing destination."
                , "identifier"
                , ui_cmd_set_monitoring
                )
        .cmd<G4int>( "useRunNumber"
                , "Sets the run number to identify events."
                , "runNo"
                , ui_cmd_set_run_number
                )
    .end("runAction");
}

G4Run *
RunAction::GenerateRun() {
    log4cpp::Category & L = log4cpp::Category::getInstance("na64mc.Geant4API");

    if( _runConfig.empty() ) {
        L.info("Starting run #%d without event post-processing.", _runNo);
        return new BaseMCRun();
    }
    L.info("Starting run #%d with event post-processing.", _runNo);
    return new Run( _runConfig
                  , _onlineMonitDest == "quiet" ? "" : _onlineMonitDest
                  , _calibMgr
                  , _mcNotifier
                  );
}

void
RunAction::BeginOfRunAction(const G4Run * /*aRun_*/) {
    // Assemble event ID
    na64sw_EventID_t eid(0x0);
    na64sw_set_runNo(&eid, ++_runNo);
    na64sw_set_reservedBit(&eid, 1);
    // Print log message
    char bf[64];
    na64sw_eid2str(eid, bf, sizeof(bf));
    log4cpp::Category & L = log4cpp::Category::getInstance("na64mc.dataProcessing");
    L << log4cpp::Priority::INFO << "New run first event: " << bf;

    // Notify calibrations manager, if present
    _calibMgr.event_id(eid);
    // ^^^ TODO shall we?

    log4cpp::Category::getInstance("na64mc.notifications").debug(
            "Notifying of \"run started\".");
    _mcNotifier.notify(Notification::runSimulationStarted);
}

void
RunAction::EndOfRunAction(const G4Run* /*aRun_*/) {
    log4cpp::Category & L = log4cpp::Category::getInstance("na64mc.Geant4API");
    L.info("Run #%d ended.", _runNo);
    //const Run * run = static_cast<const Run*>(aRun_);
    // ...
    log4cpp::Category::getInstance("na64mc.notifications").debug(
            "Notifying of \"run ended\".");
    _mcNotifier.notify(Notification::runSimulationEnded);
}

void
RunAction::ui_cmd_set_run_number(GenericG4Messenger *, const G4String &) {
    throw std::runtime_error("TODO");
}

void
RunAction::ui_cmd_set_postprocessing_config(GenericG4Messenger *, const G4String &) {
    throw std::runtime_error("TODO");
}

void
RunAction::ui_cmd_set_monitoring(GenericG4Messenger *, const G4String &) {
    throw std::runtime_error("TODO");
}

}
}

