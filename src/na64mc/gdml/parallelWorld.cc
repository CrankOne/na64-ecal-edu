#include "na64mc/gdml/parallelWorld.hh"
#include "na64mc/g4api/detectorConstruction.hh"

#include <G4GDMLParser.hh>
#include <G4PVPlacement.hh>

#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

GDMLParallelWorld::GDMLParallelWorld( const G4String & pwName
                                    , const G4String & setupName
                                    , G4GDMLParser * gdmlParserPtr
                                    // ...
                                    )
        : G4VUserParallelWorld(pwName)
        , _setupName(setupName)
        , _gdmlParser(gdmlParserPtr)
        {}

GDMLParallelWorld::~GDMLParallelWorld() {
}

void
GDMLParallelWorld::Construct() {
    // to-be world for a parallel world
    G4VPhysicalVolume * roWorldPtr = GetWorld();
    // world ptr acquired from GDML
    G4VPhysicalVolume * gdmlGhostWorld = _gdmlParser->GetWorldVolume(_setupName);
    assert(gdmlGhostWorld);
    //G4LogicalVolume* worldLogical = ghostWorld->GetLogicalVolume();
    //assert(roWorldPtr);
    // Create parallel world for RO and put the ro-world within
    //gdmlGhostWorld->SetMotherLogical(roWorldPtr->GetLogicalVolume());
    // ^^^ this does not work
    new G4PVPlacement( nullptr  // rotation of mother frame
                 , G4ThreeVector()  // position (in rotated frame)
                 , gdmlGhostWorld->GetLogicalVolume() // current logical
                 , "parWorldPlacement-" + GetName() + "-" + _setupName  // placement name
                 , roWorldPtr->GetLogicalVolume()  // mother logical
                 , false  // not used, shall indicate "expected overlap"
                 , 0  // shoulb be set to 0 for the first volume of a given type
                 , true  // check overlaps
                 );
    log4cpp::Category & L = log4cpp::Category::getInstance(std::string("na64mc.gdml"));
    L << log4cpp::Priority::INFO
         << "Parallel world \"" << GetName() << "\" ("
         << (void*) this << ") geometry constructed.";
}

void
GDMLParallelWorld::ConstructSD() {
    // TODO:
    //      We construct SDs (bound to their volumes) using the GDML auxinfo
    //      processor, although using this method instead seems to be a more
    //      proper place. Consider redesign configuration objects to make it
    //      match the foreseen architecture of Geant4 app for better
    //      compatibility in future... Or not.
    //
    //G4cout << "Sensitive detectors of parallel world \"" << GetName() << "\" ("
    //       << (void*) this << ") constructed." << std::endl;
}

}
}
