#include "na64mc/gdml/processor.hh"

#include <G4LogicalVolume.hh>

#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

AuxInfoIndex::AuxInfoIndex() {
}

void
AuxInfoIndex::ignore_auxinfo(const std::string & nm) {
    log4cpp::Category::getInstance("na64mc.gdml")
        << log4cpp::Priority::INFO
        << "GDML aux info tag \"" << nm << "\" will be ignored silently.";
    _auxInfoIgnore.insert(nm);
}

void
AuxInfoIndex::process_auxinfo( G4LogicalVolume * volumePtr
                             , const G4GDMLAuxListType & auxInfoList ) {
    for( const G4GDMLAuxStructType & auxStruct : auxInfoList ) {
        if( _auxInfoIgnore.end() != _auxInfoIgnore.find(auxStruct.type) ) {
            log4cpp::Category::getInstance("na64mc.gdml")
                << log4cpp::Priority::DEBUG
                << "GDML aux info tag \"" << auxStruct.type << "\" ignored for volume \""
                << (volumePtr ? volumePtr->GetName() : "<none>")
                << "\".";
            continue;
        }
        auto procIt = this->find(auxStruct.type);
        if(this->end() == procIt) {
            char errbuf[256];
            snprintf( errbuf, sizeof(errbuf)
                    , "No processor found for auxiliary tag of type \"%s\""
                      " associated with volume %p (\"%s\")."
                    , auxStruct.type.c_str()
                    , volumePtr
                    , volumePtr ? volumePtr->GetName().c_str() : "null"
                    );
            G4Exception( __FUNCTION__
                   , "NA64SW002"
                   , JustWarning
                   , errbuf
                   );
            continue;
        }
        procIt->second->process_auxinfo( auxStruct, volumePtr );
    }
}

}
}
