# include "na64dp/processingInfoNw.hh"

# include <sstream>
# include <iostream>  // xxx, for cerr

# ifdef ZMQ_FOUND

namespace na64dp {

static const char gEPITag[] = "epi:";

NwPubEventProcessingInfo::NwPubEventProcessingInfo( int portNo
                                                  , size_t nMaxEvents
                                                  , const HitBanks & banks
                                                  , unsigned int refreshInterval)
                                : EvProcInfoDispatcher( nMaxEvents, refreshInterval, banks )
                                , _zCtx(1)
                                , _zPubSock(_zCtx, ZMQ_PUB) {
    std::ostringstream oss;
    oss << "tcp://*:" << portNo;
    _zPubSock.bind( oss.str().c_str() );
}

// TODO: elaborate error reporting on 0MQ failures
void
NwPubEventProcessingInfo::_update_event_processing_info() {
    // Get the short description of ongoing processing
    excerpt(_msgBuf);
    assert( _msgBuf.size() );

    // Prefix network message with 'EPS ' indicating that it is event
    // processing info data (to be filtered by subscribers)
    {
        if( ! _zPubSock.send( gEPITag, sizeof(gEPITag) - 1, ZMQ_SNDMORE ) ) {
            std::cerr << "Failed to send 0MQ msg prefix." << std::endl;
        }
        zmq::message_t message( _msgBuf.size() );
        memcpy( message.data()
              , _msgBuf.data()
              , _msgBuf.size() );
        if( ! _zPubSock.send( message, 0x0 ) ) {
            std::cerr << "Failed to send zmq msg" << std::endl;
        }
    }

    _msgBuf.clear();
}

}  // namespace na64dp

# endif

