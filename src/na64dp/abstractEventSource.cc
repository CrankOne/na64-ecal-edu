#include "na64dp/abstractEventSource.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {

AbstractEventSource::AbstractEventSource(HitBanks & banks, iEvProcInfo * epi)
        : _log(log4cpp::Category::getInstance(std::string("sources")))
        , _banks(banks)
        , _inserterSADC(_banks.of<SADCHit>())
        , _inserterAPV(_banks.of<APVHit>())
        , _inserterStwTDC(_banks.of<StwTDCHit>())
        , _epi(epi)
        {}

}  // namespace na64dp

