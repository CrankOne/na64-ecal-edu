#include "na64util/vctr.hh"
#include "na64util/str-fmt.hh"
#include "na64util/demangle.hh"

#include <cstring>

namespace na64dp {
namespace errors {

CtrNotFound::CtrNotFound( const std::string & name
                        , const std::type_info & typeInfo ) throw()
        : std::runtime_error(util::format( "No virtual constructor named \"%s\" found for subclass of %s."
                                         , name.c_str(), util::demangle_cpp(typeInfo.name()).get() ).c_str())
        , _typeInfo(typeInfo)
        {
    strncpy( _name, name.c_str(), sizeof(_name) );
}

NoCtrsOfType::NoCtrsOfType( const std::type_info & typeInfo ) throw()
        : std::runtime_error(util::format( "Unknown base type `%s' requested in virtual constructor."
                                         , util::demangle_cpp(typeInfo.name()).get()
                                         ).c_str())
        , _typeInfo(typeInfo)
        {
}

}

VCtr * VCtr::_self = nullptr;

}

