#include "na64util/numerical/langaus.hh"

#ifdef ROOT_FOUND

#include <TROOT.h>

namespace na64dp {
namespace util {

const Double_t gLandauFMax = -0.22278298;

/** This function is taken from official ROOT tutorial.
 * See, e.g.: https://root.cern/doc/v608/langaus_8C.html
 *
 * Fit parameters:
 * @param width (scale) parameter of Landau density
 * @param mp most Probable (MP, location) parameter of Landau density
 * @param area total area (integral -inf to inf, normalization constant)
 * @param sigma width (sigma) of convoluted Gaussian function
 * @param np number of convolution steps
 * @param sc convolution extends to +-sc Gaussian sigmas
 * 
 * In the Landau distribution (represented by the CERNLIB approximation),
 * the maximum is located at x=-0.22278298 (defined in `gLandauFMax` global
 * const) with the location parameter=0. This shift is corrected within this
 * function, so that the actual maximum is identical to the MP parameter.
 * */
Double_t
langaus( Double_t x, Double_t width
                   , Double_t mp
                   , Double_t area
                   , Double_t sigma
                   , Int_t np
                   , Double_t sc
                   ) {
      // Numeric constants
      const Double_t invsq2pi = 1/sqrt(2*M_PI);
      // Variables
      Double_t xx;
      Double_t mpc;
      Double_t fland;
      Double_t sum = 0.0;
      Double_t xlow,xupp;
      Double_t step;
      Double_t i;
      // MP shift correction
      mpc = mp - gLandauFMax * width;
      // Range of convolution integral
      xlow = x - sc * sigma;
      xupp = x + sc * sigma;
      step = (xupp-xlow) / np;
      // Convolution integral of Landau and Gaussian by sum
      for(i=1.0; i<=np/2; i++) {
         xx = xlow + (i-.5) * step;
         fland = TMath::Landau(xx, mpc, width) / width;
         sum += fland * TMath::Gaus(x, xx, sigma);
         xx = xupp - (i-.5) * step;
         fland = TMath::Landau(xx, mpc, width) / width;
         sum += fland * TMath::Gaus(x, xx, sigma);
      }
      return (area * step * sum * invsq2pi / sigma);
}

Double_t
langausfun(Double_t *x, Double_t *par) {
    return langaus( *x, par[0], par[1], par[2], par[3] );
}

/** Will fit provided histogram with `langaus()` function with standard ROOT
 * fitting algorithms returning function and model parameters.
 *
 * @param[in] his histogram to fit
 * @param[in] fitrange[2] lo and hi boundaries of fit range
 * @param[in] startvalues[4] reasonable start values for the fit
 * @param[in] parlimitslo[4] lower parameter limits
 * @param[in] parlimitshi[4] upper parameter limits
 * @param[in] fitOpts sets fitting options. Default is "LRQ0" (when NULL)
 * @param[out] fitparams[4] returns the final fit parameters
 * @param[out] fiterrors[4] returns the final fit errors
 * @param[out] ChiSqr returns the chi square
 * @param[out] NDF returns ndf
 * @return an instance of `TF1` representing fit function
 *
 * \note This function allocates `TF1` instance on heap. One has to `delete`
 *       it afterwards.
 * */
TF1 *
langausfit( TH1F * his
          , Double_t * fitrange
          , Double_t * startvalues
          , Double_t * parlimitslo
          , Double_t * parlimitshi
          , const char * fitOpts
          , Double_t * fitparams
          , Double_t * fiterrors
          , Double_t & chiSqr
          , Int_t & ndf ) {
   Char_t FunName[128];
   snprintf( FunName, sizeof(FunName), "Fitfcn_%s", his->GetName() );

   TF1 *ffitold = (TF1*) ROOT::GetROOT()
                        ->GetListOfFunctions()
                        ->FindObject(FunName);
   if( ffitold )
       delete ffitold;
   TF1 *ffit = new TF1(FunName, langausfun, fitrange[0], fitrange[1], 4);

   ffit->SetParameters(startvalues);
   ffit->SetParNames( "Width", "MP", "Area", "GSigma" );
   for(Int_t i=0; i<4; i++) {
      ffit->SetParLimits(i, parlimitslo[i], parlimitshi[i]);
   }

   // fit within specified range, use ParLimits, do not plot
   his->Fit(FunName, fitOpts ? fitOpts : "LRQ0");

   // Obtain output
   ffit->GetParameters(fitparams);
   for(Int_t i=0; i<4; i++)
      fiterrors[i] = ffit->GetParError(i);
   chiSqr = ffit->GetChisquare();
   ndf = ffit->GetNDF();
   return ffit;
}

}
}

#endif

