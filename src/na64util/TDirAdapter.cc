#include "na64util/TDirAdapter.hh"

#ifdef ROOT_FOUND

#include "na64detID/TBName.hh"
#include "na64util/str-fmt.hh"
#include "na64detID/TBNameErrors.hh"

#include <TFile.h>
#include <cassert>

namespace na64dp {

TDirAdapter::TDirAdapter( calib::Dispatcher & dsp ) : _names(nullptr) {
    dsp.subscribe<nameutils::DetectorNaming>(*this, "default");
}

void
TDirAdapter::handle_update( const nameutils::DetectorNaming & tbn ) {
    if( _rootDirs.empty() ) {  // first update, no recaching needed
        _names = &tbn;
        return;
    }
    NA64DP_RUNTIME_ERROR( "TODO: recache TDirAdapter dict" );
    // ^^^ if you've got this exception, then recaching of TBNames mapping
    // happened after a while. It is foreseen use case, albeit rarely needed,
    // so implementation was postponed.
}

std::map<std::string, std::string>
TDirAdapter::subst_dict_for(DetID_t did, const std::string & histName) const {
    std::map<std::string, std::string> ctx;
    naming().append_subst_dict_for(did, ctx);
    ctx["hist"] = histName;
    try {
        ctx["TBName"] = naming()[did];
    } catch( errors::IncompleteDetectorID & e ) {
        ; // pass
    }
    return ctx;
}

TDirectory *
TDirAdapter::dir_for( DetID_t did
                    , std::string & histName
                    , const std::string & overridenTemplate ) {
    std::map<std::string, std::string> ctx = subst_dict_for(did, histName);
    auto p = dir_for(did, ctx, overridenTemplate);
    histName = p.first;
    return p.second;
}

std::pair<std::string, TDirectory *>
TDirAdapter::dir_for( DetID_t did
                    , const std::map<std::string, std::string> & ctx
                    , const std::string & overridenTemplate ) {

    const std::string * strPathTmplPtr = &overridenTemplate;
    if( strPathTmplPtr->empty() ) {
        strPathTmplPtr = &naming().path_template( did );
    }

    std::string path = util::str_subst(
            *strPathTmplPtr,
            ctx );
    util::assert_str_has_no_fillers(path);
    // Get last token that typically is the histogram name and put it into
    // histName
    size_t pos = path.rfind('/');
    if( std::string::npos == pos ) {
        NA64DP_RUNTIME_ERROR( "Unable to locate last token in"
                " histogram path \"%s\".", path.c_str());
    }
    std::string dirPath = path.substr( 0, pos );
    std::string histName = path.substr( pos+1, std::string::npos );
    TDirectory * tdPtr = mkdir_p( dirPath );
    if( !tdPtr ) {
        NA64DP_RUNTIME_ERROR( "Failed to create TDirectory \"%s\" (for"
                " histogram \"%s\").", dirPath.c_str(), histName.c_str() );
    }
    _rootDirs.emplace( did, tdPtr );
    if( histName.empty() ) {
        NA64DP_RUNTIME_ERROR( "Unable to derive last token of path"
                " \"%s\".", path.c_str() );
    }
    return std::make_pair(histName, tdPtr);
}

TDirectory *
TDirAdapter::mkdir_p( const std::string & path ) {
    TDirectory * tdPtr = gFile;
    size_t prevPos = 0;
    while( std::string::npos != prevPos ) {
        size_t pos = path.find('/', prevPos);
        std::string cDirName = path.substr( prevPos, pos - prevPos );
        assert(!cDirName.empty());
        TDirectory * subDPtr = tdPtr->GetDirectory( cDirName.c_str() );
        if( !subDPtr ) {
            subDPtr = tdPtr->mkdir( cDirName.c_str(), "" );
        }
        assert(subDPtr);
        tdPtr = subDPtr;
        prevPos = std::string::npos == pos ? pos : pos + 1;
    }
    return tdPtr;
}

const nameutils::DetectorNaming &
TDirAdapter::naming() const {
    assert(_names);  // premature dereferencing
    return *_names;
}

}  // namespace na64

#endif

