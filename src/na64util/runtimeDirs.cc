#include "na64util/runtimeDirs.hh"

#include <wordexp.h>
#include <cassert>
#include <cstring>
#include <sys/stat.h>
#include <glob.h>

namespace na64dp {
namespace util {

RuntimeDirs::RuntimeDirs( const char * path_ ) {
    assert( path_ && path_[0] != '\0' );  // non-empty lookup path
    char * searchPath = strdup(path_);
    char * strtokSavePtr;
    for( char * pathEntry = strtok_r(searchPath, ":", &strtokSavePtr)
       ; NULL != pathEntry
       ; pathEntry = strtok_r(NULL, ":", &strtokSavePtr) ) {
        wordexp_t p;
        char ** w;

        assert( pathEntry[0] != '\0' );
        _mlLog << "Expanding \"" << pathEntry << "\":" << std::endl;
        wordexp( pathEntry, &p, 0 );
        w = p.we_wordv;
        for( size_t i = 0; i < p.we_wordc; ++i ) {
            struct stat dirPathStat;
            int dirStatRC = stat( w[i], &dirPathStat );
            if( -1 == dirStatRC ) {
                int dirStatErrcode = errno;
                _mlLog << "  Error accessing \""
                       << w[i] << "\": "
                       << strerror(dirStatErrcode)
                       << std::endl;
                continue;
            }
            if( ! S_ISDIR( dirPathStat.st_mode ) ) {
                _mlLog << "  Omit \""
                       << w[i] << "\": "
                       << " not a directory, skip"
                       << std::endl;
                continue;
            }
            _mlLog << "  -> " << w[i] << std::endl;
            push_back( w[i] );
        }
        wordfree(&p);
    }
}

std::vector<std::string>
RuntimeDirs::locate_files( const std::string & filePattern ) const {
    glob_t globbuf;
    globbuf.gl_offs = 0;
    bool ranOnce = false;
    for( auto dir : *this ) {
        /*int globRC =*/ glob( (dir + "/" + filePattern).c_str()
                         , GLOB_NOSORT | GLOB_MARK | ( ranOnce ? GLOB_APPEND | GLOB_DOOFFS : 0x0 ) | GLOB_TILDE | GLOB_BRACE
                         , NULL
                         , &globbuf );
        // ...
        //globbuf.gl_offs = globbuf.gl_pathc;
        ranOnce = true;
    }
    std::vector<std::string> results;
    for( size_t i = 0; i < globbuf.gl_pathc; ++i ) {
        results.push_back( globbuf.gl_pathv[i] );
    }
    return results;
    globfree( &globbuf );
}

}
}

