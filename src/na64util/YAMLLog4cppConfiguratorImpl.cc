#include "YAMLLog4cppConfiguratorImpl.hh"

#include <cassert>
//#include <iostream>

namespace na64dp {
namespace util {

// aux: returns category name from path tokens
static std::string
_cat_name_from_ptoks( const std::list<std::string> & catNames ) {
    std::string catPath;
    for( auto ptok : catNames ) {
        if( !catPath.empty() ) catPath += ".";
        catPath += ptok;
    }
    return catPath;
}

// aux function recursively creating categories from YAML node
void
YAMLLog4cppConfiguratorImpl::_configure_category_from_YAML( const YAML::Node & catCfg
                                                          , log4cpp::Category & cat
                                                          , std::list<std::string> & catNames
                                                          ) {
    log4cpp::Priority::Value priority = log4cpp::Priority::NOTSET;
    bool additivity = true;
    for( const auto & catEntry : catCfg ) {
        std::string catParName = catEntry.first.as<std::string>();
        if( "_threshold" == catParName ) {
            priority
                = log4cpp::Priority::getPriorityValue(catEntry.second.as<std::string>());
            continue;
        }
        if( "_appenders" == catParName ) {
            assert( catEntry.second.IsSequence() );  // TODO
            for( auto appenderName : catEntry.second ) {
                auto appIt = appenders.find( appenderName.as<std::string>() );
                if(appenders.end() == appIt) {
                    std::cerr << "Can not associate category \""
                        << _cat_name_from_ptoks(catNames)
                        << "\" with appender \""
                        << appenderName.as<std::string>()
                        << "\""
                        << std::endl;
                    continue;  // TODO: error on missed appender?
                }
                cat.addAppender(*(appIt->second));
                // ^^^ NOTE: there are two kinds of this method in Category.
                // Passing appender by ptr makes the category instance own the
                // appender while passing by reference makes weak association
            }
            continue;
        }
        if( "_additivity" == catParName ) {
            additivity = catEntry.second.as<bool>();
            continue;
        }
        // otherwise, the `catParName' contains the sub-category name
        catNames.push_back(catParName); {
            std::string catPath = _cat_name_from_ptoks(catNames);
            assert( ! log4cpp::Category::exists(catPath) );
            // ^^^ this is really an assertion. Category must not exist
            // before first invocation of creating `getInstance' func.
            _configure_category_from_YAML(
                      catEntry.second
                    , log4cpp::Category::getInstance(catPath)
                    , catNames
                    );
        } catNames.pop_back();
    }
    cat.setPriority(priority);
    cat.setAdditivity(additivity);
}

void
YAMLLog4cppConfiguratorImpl::doConfigure(const std::string & filename) {
    YAML::Node cfgLog;
    cfgLog = YAML::LoadFile( filename );
    doConfigure(cfgLog);
}

void
YAMLLog4cppConfiguratorImpl::doConfigure( const YAML::Node & cfg ) {
    // Instantiate all appenders
    assert( cfg["appenders"].IsMap() );  // TODO
    for( const auto appenderEntry : cfg["appenders"] ) {
        const std::string & appenderName = appenderEntry.first.as<std::string>();
        const YAML::Node & appenderCfg = appenderEntry.second;
        // priority of current appender
        log4cpp::Priority::Value priority = log4cpp::Priority::NOTSET;
        assert( appenderEntry.second.IsMap() );  // TODO
        // iterate over appender's properties and convert them into log4cpp
        // factory's parameters
        log4cpp::FactoryParams appFPs;
        std::string appenderClassName;
        log4cpp::Layout * appenderLayout = nullptr;
        for( const auto appenderParameterEntry : appenderCfg ) {
            const std::string & appenderParamName
                = appenderParameterEntry.first.as<std::string>();
            const YAML::Node & appenderParamValue
                = appenderParameterEntry.second;
            if( "_type" == appenderParamName ) {
                // has special meaning: appender class to choose
                appenderClassName = appenderParamValue.as<std::string>();
                continue;  // do not create the log4cpp's property from "type"
            }
            if( "_layout" == appenderParamName ) {
                // handle "layout" node
                std::string layoutClassName;
                log4cpp::FactoryParams lytFPs;
                // - iterate over layout parameters
                assert(appenderParamValue.IsMap());  // TODO
                for( const auto layoutParameterEntry : appenderParamValue ) {
                    if("_type" == layoutParameterEntry.first.as<std::string>() ) {
                        layoutClassName = layoutParameterEntry.second.as<std::string>();
                        continue; // do not create the log4cpp's property from "type"
                    }
                    // create log4cpp property for layout
                    lytFPs[layoutParameterEntry.first.as<std::string>()]
                        = layoutParameterEntry.second.as<std::string>();
                }
                assert(log4cpp::LayoutsFactory::getInstance().registed(
                            layoutClassName));  // TODO
                appenderLayout = log4cpp::LayoutsFactory::getInstance().create(
                        layoutClassName, lytFPs ).release();
                continue;  // do not create the log4cpp's property from "layout"
            }
            if( "_threshold" == appenderParamName ) {
                priority
                    = log4cpp::Priority::getPriorityValue(appenderEntry.second.as<std::string>());
                continue;
            }
            // TODO: support for "_filters" -- we currently do not
            //       use it, but it somehow do exist in log4cpp, as an interface
            // otherwise, it is an "ordinary" appender's parameter -- create
            // property
            appFPs[appenderParamName] = appenderParamValue.as<std::string>();
        }
        appFPs["name"] = appenderName;
        if( ! log4cpp::AppendersFactory::getInstance().registered(
                        appenderClassName)) {
            std::cerr << "Unknown appender class: \""
                << appenderClassName << "\"." << std::endl;
            continue;  // TODO: error?
        }
        // Instantiate the appender
        log4cpp::Appender * appender
            = log4cpp::AppendersFactory::getInstance()
                    .create(appenderClassName, appFPs).release();
        if( appenderLayout ) {
            appender->setLayout(appenderLayout);
        } else {
            assert(!appender->requiresLayout());  // TODO
        }
        appender->setThreshold(priority);
        appenders[appenderName] = appender;
    }
    // Iterate over the "categories"
    assert(cfg["categories"].IsMap());
    std::list<std::string> catNames;
    _configure_category_from_YAML( cfg["categories"]
                                 , log4cpp::Category::getRoot()
                                 , catNames );
}

}
}

