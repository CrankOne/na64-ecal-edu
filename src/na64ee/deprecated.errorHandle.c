# include "na64ee/errorHandle.h"

# include <stdlib.h>
# include <stdio.h>
# include <stdarg.h>

/**@file errorHandle.c
 * @brief Implements error handling mechanics for NA64EE library.
 *
 * At this file one can find the implementation of error-handling functions for
 * NA64EE library. It provides a static pointer to function that does actual
 * abort of executing process upon error. This pointer is declared in
 * corresponding header as an extern global variable and, thus, can be set to
 * user function with appropriate signature to customize behaviour on failure.
 * */

const static struct NA6EE_ErrorDescriptions {
    int code;
    char description[64];
} _static_na64eeDescriptions[] = {
    # define initialize_na64ee_description_entry( code, name, descr ) \
        { code, descr },
    for_all_NA64EE_error_codes( initialize_na64ee_description_entry )
    # undef initialize_na64ee_description_entry
    { 0x0, "<unknown error code>" }
};

const char *
na64ee_error_code_description( enum NA64EE_ErrorCode code ) {
    const struct NA6EE_ErrorDescriptions * c = _static_na64eeDescriptions;
    for( ; c->code ; ++c ) {
        if( code == c->code ) { return c->description; }
    }
    --c;
    return c->description;  /* Has to return last error code (unknown) */
}

/* Set default handler: */
void (* NA64EE_C_errorHandle)(
        enum NA64EE_ErrorCode,
        const char * )
        __attribute__ ((noreturn)) =
            NA64EE_C_default_error_handle;

void
NA64EE_C_default_error_handle(
        enum NA64EE_ErrorCode code,
        const char * details ) {
    fprintf( stderr, "NA64EE library error (#%x): %s. Details:\n\t%s\n",
             (int) code, na64ee_error_code_description(code),
             details );
    abort();
}

void
na64ee_C_error( enum NA64EE_ErrorCode code, const char * fmt, ... ) {
    char errBuffer[256];
    va_list args;
    va_start( args, fmt );
    snprintf( errBuffer, sizeof(errBuffer),
              fmt, args );
    va_end( args ); 
    (*NA64EE_C_errorHandle)( code, errBuffer );
}

