/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64ee_readout.hpp"

# ifdef SUPPORT_DDD_FORMAT

# include "na64ee_exception.hpp"

// A dirty kludge to get event header declaration. Who on Earth does the
// private declaration of public-available structs?
//# define private public
# include <DaqEventsManager.h>
# include <DaqEvent.h>
//# undef private

# include <algorithm>
# include <fstream>

namespace na64ee {

# define FULLHDRLENGTH \
    (((sizeof(CS::DaqEvent::Header::HeaderDATE_old)) > (sizeof(CS::DaqEvent::Header::HeaderDATE_36))) ? \
     ((sizeof(CS::DaqEvent::Header::HeaderDATE_old))) : ((sizeof(CS::DaqEvent::Header::HeaderDATE_36))))

bool
direct_look_up_event( std::istream & is,
               const AbsEventID & eid ) {
    uint8_t buf[FULLHDRLENGTH];
    CS::DaqEvent::Header h(buf);
    for( is.seekg( 0, is.beg );
         is;
         is.seekg( h.GetLength() - sizeof(buf), is.cur ) ) {
        // Read data into buffer associated to header:
        is.read((char *) buf, sizeof(buf));
        if( eid.runNo == h.GetRunNumber()           \
         && eid.spillNo == h.GetBurstNumber()       \
         && eid.eventNo == h.GetEventNumberInBurst() ) {
            // Found requested event:
            is.seekg( - sizeof(buf), is.cur );
            return true;
        }
    }
    return false;
}

//
// Index class and its subclasses implementation

size_t
DDDIndex::SpillDataLayout::merge( const DDDIndex::SpillDataLayout & src ) {
    if( src.chunkNo != chunkNo ) {
        na64ee_raise( mergeMismatch,
                      "Original chunk number: %u, merged: %u.",
                      src.chunkNo, chunkNo );
    }
    if( src.eventsInSpill != eventsInSpill ) {
        na64ee_raise( mergeMismatch,
                      "Original events in spill: %u, merged: %u.",
                      src.eventsInSpill, eventsInSpill );
    }
    return merge( src.eventPositions );
}

size_t
DDDIndex::SpillDataLayout::merge(
                            const std::map<EventNo_t, FilePostion_t> & src ) {
    size_t nUniq = 0;
    for( std::map<EventNo_t, FilePostion_t>::const_iterator it = src.begin();
                                                    src.end() != it; ++it ) {
        const std::pair<EventNo_t, FilePostion_t> & evlPairIt = *it;
        //auto insertionResult = eventPositions.emplace( evlPairIt.first,
        //                                               evlPairIt.second );
        auto insertionResult = eventPositions.insert( evlPairIt );
        if( insertionResult.second ) {
            ++nUniq;
        }
    }
    return nUniq;
}

std::pair<DDDIndex::EventNo_t, DDDIndex::FilePostion_t>
DDDIndex::SpillDataLayout::get_preceding_event_offset(
                                                    EventNo_t needNo ) const {
    auto uIt = eventPositions.upper_bound( needNo );
    --uIt;
    return *uIt;
}


DDDIndex::DDDIndex() {
}

DDDIndex::DDDIndex( RunNo_t rn, const RunDataLayout & rl ) {
    //_runs.emplace( rn, rl );
    _runs.insert( std::pair<RunNo_t, RunDataLayout>(rn, rl) );
}

DDDIndex::DDDIndex( const DDDIndex & orig ) : _runs( orig._runs.begin(),
                                                     orig._runs.end() ) {
}

size_t
DDDIndex::merge( const DDDIndex & src ) {
    size_t nUniq = 0;
    for( std::map<RunNo_t, RunDataLayout>::const_iterator it = src._runs.begin();
                                            src._runs.end() != it; ++it ) {
        nUniq += merge( it->first, it->second );
    }
    return nUniq;
}

size_t
DDDIndex::merge( RunNo_t rn, const RunDataLayout & lstRef ) {
    size_t nUniq = 0;
    auto targetRunIt = _runs.find( rn );
    if( _runs.end() == targetRunIt) {
        // Entire run not indexed --- just insert entire copy.
        //targetRunIt = _runs.emplace(rn, lstRef).first;
        targetRunIt = _runs.insert( std::pair<RunNo_t, RunDataLayout>(rn, lstRef) ).first;
        // Count number of entries:
        # if __cplusplus > 199711L
        std::for_each( lstRef.begin(), lstRef.end(),
                [ & nUniq ] ( std::remove_reference<decltype(lstRef)>::type::value_type v ) {
                    nUniq += v.second.eventPositions.size();
                } );
        # else
        for( auto it = lstRef.begin(); lstRef.end() != it; ++it ) {
            nUniq += it->second.eventPositions.size();
        }
        # endif
        return nUniq;
    }
    //for( const auto & spillPairIt : lstRef )
    for( auto it = lstRef.begin(); it != lstRef.end(); ++it ) {
        auto & spillPairIt = *it;
        const auto & spillNo = spillPairIt.first;
        auto targetSpillIt = targetRunIt->second.find( spillNo );
        if( targetRunIt->second.end() == targetSpillIt ) {
            // New spill introduced --- just insert a copy.
            //targetRunIt->second.emplace( spillNo, spillPairIt.second );
            targetRunIt->second.insert( std::pair<SpillNo_t, SpillDataLayout>(spillNo, spillPairIt.second) );
            nUniq += spillPairIt.second.eventPositions.size();
        } else {
            // Additional info submitted for existing spill. Merge it.
            nUniq += targetSpillIt->second.merge( spillPairIt.second );
        }
    }
    return nUniq;
}

void
DDDIndex::write( std::ostream & ) const {
}

/** Performs sequential reading of events in given stream. Will fill the list
 * instance with spill (called burst in DDD) information.
 *
 * Accepts a "duty" parameter meaning that only each N event will be cached.
 * One can safely set it to 0, however it will lead to large indexing objects.
 *
 * Default chank number value affects nothing except the initial value of all
 * the chunkNo fields in SpillDataLayout instances. There is no way to validate
 * this number from inside this function.
 *
 * Does not initialize sentry to stream begin --- reading will be continued
 * from current sentry position.
 * */
void
DDDIndex::index_chunk( std::istream & is,
                       RunDataLayout & lst,
                       size_t nDuty,
                       RunNo_t expectedRunNo,
                       ChunkNo_t defaultChunkNo,
                       FILE * dbgStream ) {
    uint8_t buf[FULLHDRLENGTH];
    CS::DaqEvent::Header h(buf);
    size_t nEventsRead = 0,
           cPosition = is.tellg();
    SpillNo_t lastSpill = 0;
    SpillDataLayout csl;  // current spill layout
    csl.chunkNo = defaultChunkNo;
    EventNo_t eventsInSpill = 0;
    bool spillNoInitialized = false;
    for( ; is;
           eventsInSpill++,
           nEventsRead++,
           is.seekg( h.GetLength() - sizeof(buf), is.cur ) ) {
        cPosition = is.tellg();
        // Read data into buffer associated to header:
        is.read((char *) buf, sizeof(buf));
        if( !spillNoInitialized ) {
            lastSpill = h.GetBurstNumber();
            spillNoInitialized = true;
        }
        // New event will be cached in cases:
        //  - 0 == nEventsRead%nDuty
        //  - lastSpill != <current spill #> (new spill start)
        //  - target list is empty
        if( lastSpill != h.GetBurstNumber() ) {
            if( !csl.eventPositions.empty() ) {
                csl.eventsInSpill = eventsInSpill - 1;
                //lst.emplace( lastSpill, csl );
                lst.insert( RunDataLayout::value_type(lastSpill, csl) );
                if( dbgStream ) {
                    fprintf( dbgStream, "New spill %d (%d memorized with %zu "
                            "entries indexing %u events).\n",
                            h.GetBurstNumber(),
                            lastSpill,
                            csl.eventPositions.size(),
                            csl.eventsInSpill );
                }
                lastSpill = h.GetBurstNumber();
                eventsInSpill = 0;
            } else if( dbgStream ) {
                fprintf( dbgStream, "Empty spill ignored.\n" );
            }
            csl.eventPositions.clear();
        }
        if( 0 == nEventsRead
         || 0 == nDuty
         || !(nEventsRead%nDuty) ) {
            if( expectedRunNo != h.GetRunNumber() ) {
                na64ee_raise( badRunNumber, "Expected run number %u while "
                    "%zu-th event belongs to run %d.",
                    expectedRunNo, nEventsRead, h.GetRunNumber() );
            }
            if( dbgStream ) {
                fprintf( dbgStream, "Cached event %zu (%u in burst)"
                         " by offset %#010lx.\n",
                         nEventsRead, h.GetEventNumberInBurst(), cPosition );
            }
            //csl.eventPositions.emplace( h.GetEventNumberInBurst(), cPosition );
            csl.eventPositions.insert( std::pair<RunNo_t, FilePostion_t>(
                                               h.GetEventNumberInBurst(), cPosition ) );
        }
    }
}

void
DDDIndex::print_to_ostream( std::ostream & os ) const {
    //for( const auto & runIt : _runs )
    for( auto it = _runs.begin(); _runs.end() != it; ++it ) {
        const auto & runIt = *it;
        os << "Run #" << runIt.first << ":" << std::endl;
        //for( const auto & spillIt : runIt.second )
        for( auto iit = runIt.second.begin(); runIt.second.end() != iit; ++iit ) {
            const auto & spillIt = *iit;
            const SpillDataLayout & csl = spillIt.second;
            os << "  spill #" << spillIt.first
               << " stored in chunk " << (unsigned short) csl.chunkNo
               << " has " << csl.eventPositions.size()
               << " entries, indexing " << csl.eventsInSpill
               << " events, starting from " << csl.eventPositions.begin()->first
               << std::endl;
               // note that this information does not reflect the fact that
               // spills are eventaully written not from first event, but
               // starting from some number after. However, the number of
               // events in spill is correct and refers to actual number of
               // events written in spill.
        }
    }
}

///TODO: pretty print option should strip the
/// spaces.
void
DDDIndex::to_JSON_str( std::ostream & os/*, bool prettyPrint*/ ) const {
    size_t ncol;  // aux
    char dlm[3] = {' ', ' ', ' '};  // aux
    os << "{ \"runs\" : [";
    //for( const auto & runIt : _runs )
    for( auto it = _runs.begin(); _runs.end() != it; ++it ) {
        const auto & runIt = *it;
        os << dlm[0] << std::endl
           << std::string(4, ' ') << "{"
           << std::endl;
        os << std::string(8, ' ') << "\"runNo\" : " << runIt.first << "," << std::endl
           << std::string(8, ' ') << "\"spills\" : [";
        //for( const auto & spillIt : runIt.second )
        for( auto iit = runIt.second.begin(); runIt.second.end() != iit; ++iit ) {
            const auto & spillIt = *iit;
            const SpillDataLayout & csl = spillIt.second;
            os << dlm[1] << std::endl
               << std::string(12, ' ') << "{" << std::endl
               << std::string(16, ' ') << "\"spillNo\" : " << spillIt.first
                                                           << "," << std::endl
               << std::string(16, ' ') << "\"nEvents\" : " << csl.eventsInSpill
                                                           << "," << std::endl
               << std::string(16, ' ') << "\"chunkNo\" : " << (int) csl.chunkNo
                                                           << "," << std::endl
               << std::string(16, ' ') << "\"entries\" : ["
               ;
            ncol = 0;
            //for( const auto & evpIt : csl.eventPositions )
            for( auto iiit = csl.eventPositions.begin(); csl.eventPositions.end() != iiit; ++iiit ) {
                const auto & evpIt = *iiit;
                if( ncol ) {
                    os << dlm[2] << " ";
                }
                if( !(ncol%4) ) {
                    os << std::endl
                       << std::string(20, ' ');
                       ;
                }
                os << "[" << evpIt.first << ", "
                   //<< std::hex << "\""
                   << evpIt.second
                   //<< "\"" << std::dec
                   << "]"
                   ;
                ++ncol;
                dlm[2] = ',';
            }
            os << std::endl
               << std::string(16, ' ') << "]" << std::endl
               << std::string(12, ' ') << "}"
               ;
            dlm[1] = ',';
        }
        os << std::endl
           << std::string(8, ' ') << "]" << std::endl
           << std::string(4, ' ') << "}"
           ;
        dlm[0] = ',';
    }
    os << std::endl << "] }"
       << std::endl;
}

//TODO
//void
//DDDIndex::from_JSON_str( std::istream & is ) {
//    //...
//}

DDDIndex::FilePostion_t
DDDIndex::get_event_stream_pos( std::istream & is, EventNumericID eid_,
                                FILE * dbgStream ) const {
    UEventID eid;
    eid.numeric = eid_;
    // Reset stream first:
    is.clear();  // clear EOF flag
    //is.seekg( 0, is.beg );
    // Get nearest event id:
    auto runIdxContainer = _runs.find( eid.layout.runNo );
    if( _runs.end() == runIdxContainer ) {
        na64ee_raise( eventNotFound, "Event ID suggests run number %u which "
            "is not found in index %p.", eid.layout.runNo, this );
    }
    const RunDataLayout & spillMap = runIdxContainer->second;
    RunDataLayout::const_iterator spillIt;
    SpillNo_t sno = eid.layout.hasChunkNo ? eid.layout.spillNo : 
                    eid.chunklessLayout.spillNo ;
    {
        spillIt = spillMap.find( sno );
        if( spillMap.end() == spillIt ) {
            na64ee_raise( eventNotFound, "Event ID suggests spill number %u "
                "which is not found in index %p indexing run %u.",
                sno, this, eid.layout.runNo );
        }
    }
    EventNo_t evno = eid.layout.hasChunkNo ? eid.layout.eventNo :
                     eid.chunklessLayout.eventNo ;
    auto naeraestPrecPosPair = spillIt
                               ->second.get_preceding_event_offset( evno );
    if( dbgStream ) {
        fprintf( dbgStream,
                 "Got nearest ev #%u\n",
                  naeraestPrecPosPair.first );
    }
    // If event number matches perfectly, just return cached position:
    if( evno == naeraestPrecPosPair.first ) {
        return naeraestPrecPosPair.second;
    }
    // Set sentry to nearest preceding event and read headers until requested
    // event number will not be reached.
    is.seekg( naeraestPrecPosPair.second, is.beg );
    {
        uint8_t buf[FULLHDRLENGTH];
        CS::DaqEvent::Header h(buf);
        do {
            is.read((char *) buf, sizeof(buf));
            if( evno == h.GetEventNumberInBurst() ) {
                is.seekg( - sizeof(buf), is.cur );
                return is.tellg();
            } else if(dbgStream) {
                fprintf( dbgStream, "Omitting ev #%u\n",
                    (EventNo_t) h.GetEventNumberInBurst() );
            }
            is.seekg( h.GetLength() - sizeof(buf), is.cur );
        } while( is && h.GetEventNumberInBurst() < evno
                    && h.GetBurstNumber() == sno
                    && h.GetRunNumber() == eid.layout.runNo );
        if( dbgStream ) {
            fprintf( stdout, "Traversal loop exit conditions: "
                             "%d < %d && %d == %d && %d == %d "
                             "violated.\n",
                        (int) h.GetEventNumberInBurst(), (int) evno,
                        (int) h.GetBurstNumber(), (int) sno,
                        (int) h.GetRunNumber(), (int) eid.layout.runNo );
        }
    }
    # if __STDC_VERSION__ >= 201112L
    # pragma GCC diagnostic push
    # pragma GCC diagnostic ignored "-Wformat"
    # pragma GCC diagnostic ignored "-Wformat-extra-args"
    # endif
    na64ee_raise( eventNotFound, "Event ID %" fmt_EVENTID
                                 " not found in stream.", eid.numeric );
    # if __STDC_VERSION__ >= 201112L
    # pragma GCC diagnostic pop
    # endif
}


/* NOTE:
 * I was concerned about `legitimate' conversion for pos_type and asked people
 * at stackoverlow for details:
 *  http://stackoverflow.com/questions/41257958/how-one-can-safely-serialize-stdbasic-istreamcharpos-type
 * Apparently there is nothing useful in this struct besides just an offset
 * value that can be safely (de)serialized from within std::streamoff type.
 */

size_t
DDDIndex::serialized_size() const {
    const size_t nBytesPerPosT = sizeof( std::streamoff );
    size_t nBytes = 2*sizeof(uint32_t);
    //for( auto & rp : _runs )
    for( auto it = _runs.begin(); _runs.end() != it; ++it ) {
        auto & rp = *it;
        nBytes += sizeof( RunNo_t );
        nBytes += sizeof( uint32_t );  // for number of spills in run
        //for( auto & sl : rp.second )
        for( auto iit = rp.second.begin(); iit != rp.second.end(); ++iit ) {
            auto & sl = *iit;
            nBytes += sizeof( SpillNo_t )
                    + sizeof( ChunkNo_t )
                    + sizeof( EventNo_t )
                    + sizeof( uint32_t )  // for number of entries
                    ;
            nBytes += (nBytesPerPosT +
                            sizeof(EventNo_t))*sl.second.eventPositions.size();
        }
    }
    return nBytes;
}


size_t
DDDIndex::serialize( uint8_t * bytes, size_t bytesLength ) const {
    uint32_t & nBytesUsed = *reinterpret_cast<uint32_t *>(bytes);
    nBytesUsed = sizeof( uint32_t );
    uint8_t * c = bytes + sizeof(uint32_t);
    # define wrt_ssq( type, value )                                     \
    nBytesUsed += sizeof(type);                                         \
    if( nBytesUsed > bytesLength ) {                                    \
        na64ee_raise( overflow,                                         \
                    "Serialization buffer has insufficient length." );  \
    }                                                                   \
    *reinterpret_cast<type *>(c) = value;                               \
    c += sizeof(type);
    wrt_ssq( uint32_t, (uint32_t) _runs.size() );
    //for( auto & rp : _runs )
    for( auto it = _runs.begin(); _runs.end() != it; ++it ) {
        auto & rp = *it;
        wrt_ssq( RunNo_t, rp.first );
        wrt_ssq( uint32_t, rp.second.size() );
        //for( auto & sl : rp.second )
        for( auto iit = rp.second.begin(); rp.second.end() != iit; ++iit ) {
            auto & sl = *iit;
            wrt_ssq( SpillNo_t, sl.first );
            wrt_ssq( ChunkNo_t, sl.second.chunkNo );
            wrt_ssq( EventNo_t, sl.second.eventsInSpill );
            wrt_ssq( uint32_t,  ((uint32_t) sl.second.eventPositions.size()) );
            //for( auto & entry : sl.second.eventPositions )
            for( auto iiit = sl.second.eventPositions.begin();
                               sl.second.eventPositions.end() != iiit; ++iiit ) {
                const auto & entry = *iiit;
                wrt_ssq( EventNo_t, entry.first );
                wrt_ssq( std::streamoff, ((std::streamoff) entry.second) );
            }
        }
    }
    return nBytesUsed;
    # undef wrt_ssq
}

DDDIndex *
DDDIndex::deserialize( const uint8_t * bytes ) {
    DDDIndex * instancePtr = new DDDIndex();
    DDDIndex & idx = *instancePtr;

    const uint32_t & bytesLength = *reinterpret_cast<const uint32_t *>(bytes);
    const uint8_t * c = bytes + sizeof(uint32_t);
    size_t nBytesUsed = sizeof(uint32_t);

    # define _rdv_ssq( type, value )                                        \
    nBytesUsed += sizeof(type);                                             \
    if( nBytesUsed > bytesLength ) {                                        \
        na64ee_raise( underflow,                                            \
                        "Serialization buffer has insufficient length." );  \
    }

    # define rdv_ssq( type, value ) _rdv_ssq(type, value)                   \
    const type & value = *reinterpret_cast<const type *>( c );              \
    c += sizeof(type);

    # define rev_ssq( type, value ) _rdv_ssq(type, value)                   \
    value = *reinterpret_cast<const type *>( c );                           \
    c += sizeof(type);

    rdv_ssq( uint32_t, nRuns );
    for( uint32_t nRun = 0; nRun < nRuns; ++nRun ) {
        rdv_ssq( RunNo_t, runNo );
        rdv_ssq( uint32_t, nSpills );
        RunDataLayout runLt;
        for( uint32_t nSpill = 0; nSpill < nSpills; ++nSpill ) {
            rdv_ssq( SpillNo_t, spillNo );
            SpillDataLayout spl;
            rev_ssq( ChunkNo_t, spl.chunkNo );
            rev_ssq( EventNo_t, spl.eventsInSpill );
            rdv_ssq( uint32_t,  nEntries );
            for( uint32_t nEvent = 0; nEvent < nEntries; ++nEvent ) {
                rdv_ssq( EventNo_t,         evInSpNo );
                rdv_ssq( std::streamoff,    offsetVal );
                //spl.eventPositions.emplace( evInSpNo, offsetVal );
                spl.eventPositions.insert( std::pair<SpillNo_t, FilePostion_t>(evInSpNo, offsetVal) );
            }
            //runLt.emplace( spillNo, spl );
            runLt.insert( RunDataLayout::value_type(spillNo, spl) );
        }
        //idx._runs.emplace( runNo, runLt );
        idx._runs.insert( std::pair<RunNo_t, RunDataLayout>(runNo, runLt) );
    }

    return instancePtr;
}

# if 0
/** This function performs direct reading of certain event pointed out by ID.
 * It is not recommended for practical usage since it will traverse the entire
 * file reading headers of each event until required one will be reached.
 *
 * The serialized DDD-events has headers describing event size, so there is no
 * need to read out entire event to figure out its size. Even so, the reading
 * all the headers of events preceeding to interesting one can be quite
 * time-consuming operation so one has to utilize advanced techniques.
 * */
void read_event(
        std::istream &,
        const EventID &,
        CS::DaqEvent & ) {
    // ... TODO
}
# endif

std::ostream &
operator<<(std::ostream & os, const ChunkID & sid) {
    os << "{" << "run#" << (int) sid.runNo << ", chunk#"
                        << (int) sid.chunkNo << "}";
    return os;
}

std::vector<uint8_t>
read_one_new_raw_event( const std::string & filePath,
                        const DDDIndex & idx,
                        NA64_UEventID eid ) {
    std::ifstream is( filePath );
    na64ee::DDDIndex::FilePostion_t evPos
                          = idx.get_event_stream_pos( is, eid.numeric );
    if( !is ) {
        na64ee_raise( fileNotFound, "Unable to open file \"%s\".",
                filePath.c_str() );
    }
    is.seekg( evPos, is.beg );
    CS::DaqEvent * evPtr;
    try {
        evPtr = new CS::DaqEvent( is );
    } catch( CS::DaqEvent::ExceptionEndOfStream & e ) {
        # if __STDC_VERSION__ >= 201112L
        # pragma GCC diagnostic push
        # pragma GCC diagnostic ignored "-Wformat"
        # pragma GCC diagnostic ignored "-Wformat-extra-args"
        # endif
        na64ee_raise( fileNotFound, "Unable to find event " fmt_EVENTID " in \"%s\": %s.",
                eid.numeric,
                filePath.c_str(),
                e.what() );
        # if __STDC_VERSION__ >= 201112L
        # pragma GCC diagnostic pop
        # endif
    }
    return std::vector<uint8_t>(  (unsigned char *) evPtr->GetBuffer(),
                                 ((unsigned char *) evPtr->GetBuffer()) + evPtr->GetLength());
}

}  // namespace na64ee

# endif  // SUPPORT_DDD_FORMAT

