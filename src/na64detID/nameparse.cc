#include <regex>
#include <iostream>

namespace na64dp {
namespace nameutils {

const std::regex gNameRx("^([A-Z]+)(\\d{1,2})?[:-]?([^\\d\\W]?[\\w-]*)?$");
const int gNameGroupNo = 1
        , gStatNumGroupNo = 2
        , gPayloadGroupNo = 3
        ;

bool
tokenize_name_str( const std::string & str
                 , std::string & detName
                 , std::string & number
                 , std::string & payload ) {
    std::smatch m;
    if( ! std::regex_match(str, m, gNameRx) ) {
        return false;
    }
    detName = m[gNameGroupNo].str();
    number = m[gStatNumGroupNo].str();
    payload = m[gPayloadGroupNo].str();
    return true;
}


}
}

