#include "na64detID/kins.hh"
#include "na64util/str-fmt.hh"
#include "na64detID/TBNameErrors.hh"

#include <log4cpp/Category.hh>

#include <utility>  

namespace na64dp {
namespace nameutils {

KinFeaturesTable::KinID
KinFeaturesTable::kin_id( const std::string & name ) const {
    auto it = _kinIDs.find(name);
    if( _kinIDs.end() == it ) {
        NA64DP_RUNTIME_ERROR( "Unknown detector kin \"%s\""
                            , name.c_str() );
    }
    return it->second;
}

const KinFeaturesTable::Features &
KinFeaturesTable::kin_features( KinID kid ) const {
    auto it = _kinTraits.find( kid );
    if( _kinTraits.end() == it ) {
        NA64DP_RUNTIME_ERROR( "Unknown chip(=%#x)+kin(=%#x) IDs pair."
                            , kid.first
                            , kid.second );
    }
    return it->second;
}

const KinFeaturesTable::Features &
KinFeaturesTable::kin_features( const std::string & name ) const {
    auto kid = kin_id( name );
    return kin_features( kid );
}

KinFeaturesTable::Features &
KinFeaturesTable::define_kin( const std::string & kinName
                            , DetKin_t kinID_
                            , DetChip_t chipID
                            , const std::string & nameFormat
                            , const std::string & kinDescription
                            , const std::string & histsTPath
                            ) {
    auto & L = log4cpp::Category::getInstance("detID");
    if( kinID_ > aux::gKinIDMax || 0 == kinID_ ) {
        L.error( "Provided kin ID %#x (> %#x) or =0 for \"%s\" is out of range."
                  , kinID_, aux::gKinIDMax, kinName.c_str() );
        throw errors::IDIsOutOfRange( "KinID is out of range."
                                    , kinID_, aux::gKinIDMax );
    }
    KinID kinID = std::make_pair(chipID, kinID_);
    auto nameIR = _kinIDs.emplace(kinName, kinID);
    if( !nameIR.second ) {
        L.error( "Duplicating kin name \"%s\"."
                  , kinName.c_str() );
        throw errors::NameIsNotUniq( "Kin name is not unique."
                                   , kinName.c_str() );
    }
    auto kinIR = _kinTraits.emplace( kinID
                                   , Features(kinName) );
    if( !kinIR.second ) {
        _kinIDs.erase( nameIR.first );
        throw errors::IDIsNotUniq( "Non-unique kin ID.", kinID_ );
    }
    Features & fts = kinIR.first->second;
    fts.description = kinDescription;
    fts.nameFormat = nameFormat;
    fts.pathFormat = histsTPath;

    L.debug( "New detector kin \"%s\" on chip (%#x) is defined"
             " with ID %#x."
           , kinName.c_str(), chipID, kinID_ );
    return fts;
}

void
KinFeaturesTable::clear() {
    _kinTraits.clear();
    _kinIDs.clear();
}

}
}

