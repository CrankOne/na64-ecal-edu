#include "na64event/fields/APV.hh"

#include "na64event/hitTraits.hh"
#include "na64dp/abstractEventSource.hh"
#include "na64detID/TBName.hh"
#include "na64event/serialize.hh"

// Detector-specific IDs
#include "na64detID/wireID.hh"
#include "na64util/str-fmt.hh"

#include <cmath>

namespace na64dp {

HitsMap<APVHit> &
EvFieldTraits<APVHit>::map( Event & e ) {
    return e.apvHits;
}

std::vector<HitsMap<APVHit>*>
EvFieldTraits<APVHit>::maps_for( Event & e
                               , DetID /*unused*/
                               , const nameutils::DetectorNaming & ) {
    std::vector<HitsMap<APVHit>*> r;
    r.push_back( &e.apvHits );
    return r;
}

void
EvFieldTraits<APVHit>::remove_from_event( Event & e
                                        , HitsMap<APVHit>::Iterator it
                                        , const nameutils::DetectorNaming & nm ) {
    // Remember detector ID as `it' will not be valid after erasing it from
    // main map
    DetID did(it.offsetIt->first);
    // Erase entry from each subsidiary map
    for( auto subMapPtr : maps_for(e, did, nm) ) {
        subMapPtr->container().erase( did );
    }
}

HitsInserter<APVHit> &
EvFieldTraits<APVHit>::inserter( AbstractEventSource & src ) {
    return src._inserterAPV;
}

#if 0
void
EvFieldTraits<APVHit>::append_subst_dict_for( DetID did,
        std::map<std::string, std::string> & m,
        const nameutils::DetectorNaming & nm ) {
    char bf[64];
    const std::string kinName = nm.kin_name(did);
    if( "MM" == kinName || "GM" == kinName ) {
        if( did.is_payload_set() ) {
            WireID wID(did.payload());
            snprintf(bf, sizeof(bf), "%d", wID.wire_no() );
            m["wireNo"] = bf;
            snprintf(bf, sizeof(bf), "%c", WireID::proj_label(wID.proj()) );
            m["proj"] = bf;
        }
    } else {
        NA64DP_RUNTIME_ERROR( "Unknwon APV detector kin: \"%s\""
                            , kinName.c_str() );
    }
}
#endif

void
EvFieldTraits<APVHit>::reset_hit( APVHit & hit ) {
    hit.rawData.wireNo = std::numeric_limits<uint16_t>::max();
    hit.rawData.samples[0]
        = hit.rawData.samples[1]
        = hit.rawData.samples[2]
        = std::numeric_limits<uint32_t>::max();
    hit.averageT = 0;
    hit.maxCharge = 0;
    hit.a02 = hit.a12 = 0;
    hit.t02 = hit.t12 = 0;
    hit.t02sigma = hit.t12sigma = 0;
    hit.time = hit.timeError = 0;
}

DetID_t
EvFieldTraits<APVHit>::uniq_detector_id(DetID did) {
    // Keep chip, kin, station number and projection id -- only wire number is
    // not set
    WireID wID( did.payload() );
    assert( wID.proj_is_set() );
    assert( wID.proj() != WireID::kUnknown );
    wID.unset_wire_no();
    did.payload(wID.id);
    return did.id;
}

//
// Getters
/////////

static double _apv_ratio02_getter( const APVHit & h ) { return h.a02; }
static double _apv_ratio12_getter( const APVHit & h ) { return h.a12; }
static double _apv_wireno_getter( const APVHit & h ) { return h.rawData.wireNo; }
static double _apv_averageT_getter( const APVHit & h ) { return h.averageT; }
static double _apv_maxCharge_getter( const APVHit & h ) { return h.maxCharge; }
static double _apv_time_getter( const APVHit & h ) { return h.time; }
static double _apv_timeError_getter( const APVHit & h ) { return h.timeError; }
// temp
static double _apv_timeRise_getter( const APVHit & h ) { return h.timeRise; }

// ...

static EvFieldTraits<APVHit>::GetterEntry _getters[] = {
    { _apv_ratio02_getter, "aRatio02", "Hit samples ratio value a0/a2" },
    { _apv_ratio12_getter, "aRatio12", "Hit samples ratio value a1/a2" },
    { _apv_wireno_getter, "daqWireNo", "DAQ wire number" },
    { _apv_averageT_getter, "hitRiseTime", "Mean rise time of hit" },
    { _apv_maxCharge_getter, "hitCharge", "Maximal sample of hit" },
    { _apv_time_getter, "hitTime", "Time of APV hit" },
    { _apv_timeError_getter, "hitTimeError", "Time error of APV hit" },
    //
    { _apv_timeRise_getter, "hitTimeRise", "Rise time of APV hit" },
    // ...
    { nullptr, nullptr, nullptr }
};
EvFieldTraits<APVHit>::GetterEntry * EvFieldTraits<APVHit>::getters
    = _getters;

//
// Serialization
///////////////

#if defined(CapnProto_FOUND) && CapnProto_FOUND
void
EvFieldTraits<APVHit>::pack( const APVHit & hit
                           , PackedType::Builder bHit
                           , serialization::StatePacking & S ) {
    // write APV hit specific fields
    bHit.setWireNo( hit.rawData.wireNo );
    bHit.setRawSample0( hit.rawData.samples[0] );
    bHit.setRawSample1( hit.rawData.samples[1] );
    bHit.setRawSample2( hit.rawData.samples[2] );
    bHit.setAverageT ( hit.averageT );
	bHit.setMaxCharge ( hit.maxCharge );
    bHit.setChannelNo( hit.channelNo );
    bHit.setA02( hit.a02 );
    bHit.setA12( hit.a12 );
    bHit.setT02( hit.t02 );
    bHit.setT12( hit.t12 );
    bHit.setT02Sigma( hit.t02sigma );    
    bHit.setT12Sigma( hit.t12sigma );
    bHit.setTime( hit.time );    
    bHit.setTimeError( hit.timeError ); 
    // ...

    // put the number of current serialized hit into map for subsequent usage
    auto ir = S.apvHits.emplace( hit.channelNo
                               , S.apvHits.size() );
    assert( ir.second );  // channel no is unique
}

void
EvFieldTraits<APVHit>::unpack( PackedType::Reader rHit
                             , APVHit & hit
                             , serialization::StateUnpacking & S) {
    hit.rawData.wireNo = rHit.getWireNo();
    hit.rawData.samples[0] = rHit.getRawSample0();
    hit.rawData.samples[1] = rHit.getRawSample1();
    hit.rawData.samples[2] = rHit.getRawSample2();
    hit.channelNo = rHit.getChannelNo();
    hit.averageT = rHit.getAverageT();
    hit.maxCharge = rHit.getMaxCharge();
    hit.a02 = rHit.getA02();
    hit.a12 = rHit.getA12();
    hit.t02 = rHit.getT02();
    hit.t12 = rHit.getT12();
    hit.t02sigma = rHit.getT02Sigma();
    hit.t12sigma = rHit.getT12Sigma();
    hit.time = rHit.getTime();
    hit.timeError = rHit.getTimeError();
    // ...
    // put the reference to the current hit within a map
    auto ir = S.apvHits.emplace( S.apvHits.size()
                               , S.banks.of<APVHit>().get_ref_of( &hit )
                               );
    assert( ir.second );  // element is unique
}
#endif

}

