#include "na64event/fields/StwTDC.hh"

#include "na64event/hitTraits.hh"
#include "na64dp/abstractEventSource.hh"
#include "na64detID/TBName.hh"
#include "na64event/serialize.hh"
#include "na64util/str-fmt.hh"

// Detector-specific IDs
#include "na64detID/wireID.hh"
#include "na64detID/cellID.hh"

#include <cmath>

namespace na64dp {

HitsMap<StwTDCHit> &
EvFieldTraits<StwTDCHit>::map( Event & e ) { return e.stwtdcHits; }

std::vector<HitsMap<StwTDCHit>*>
EvFieldTraits<StwTDCHit>::maps_for( Event & e
                                  , DetID
                                  , const nameutils::DetectorNaming & ) {
    std::vector<HitsMap<StwTDCHit>*> r;
    r.push_back( &e.stwtdcHits );
    return r;
}

void
EvFieldTraits<StwTDCHit>::remove_from_event( Event & e
                                           , HitsMap<StwTDCHit>::Iterator it
                                           , const nameutils::DetectorNaming & nm ) {
    // Remember detector ID as `it' will not be valid after erasing it from
    // main map
    DetID did(it.offsetIt->first);
    // Erase entry from each subsidiary map
    for( auto subMapPtr : maps_for(e, did, nm) ) {
        subMapPtr->container().erase( did );
    }
}

HitsInserter<StwTDCHit> &
EvFieldTraits<StwTDCHit>::inserter( AbstractEventSource & src ) {
    return src._inserterStwTDC;
}

void
EvFieldTraits<StwTDCHit>::reset_hit( StwTDCHit & hit ) {
	hit.rawData.wireNo = std::numeric_limits<uint16_t>::max();
	hit.rawData.time   = std::numeric_limits<uint32_t>::max();
    hit.position       = std::nan("0");
}

DetID_t
EvFieldTraits<StwTDCHit>::uniq_detector_id(DetID did) {
    return did.id;
}

/// StwTDC position getter
static double _swttdc_position_getter( const StwTDCHit & h ) { return h.position; }

//
// Getters
/////////

static EvFieldTraits<StwTDCHit>::GetterEntry _getters[] = {
    { _swttdc_position_getter, "position", "Hit position" },
    // ...
    { nullptr, nullptr, nullptr }
};
EvFieldTraits<StwTDCHit>::GetterEntry * EvFieldTraits<StwTDCHit>::getters
    = _getters;


//
// Serialization
///////////////

#if defined(CapnProto_FOUND) && CapnProto_FOUND
void
EvFieldTraits<StwTDCHit>::pack( const StwTDCHit & hit
                              , PackedType::Builder bHit
                              , serialization::StatePacking & ) {
    // write StwTDC hit specific fields

    bHit.setPosition( hit.position );
    
    // ...
}

void
EvFieldTraits<StwTDCHit>::unpack( PackedType::Reader rHit
                                , StwTDCHit & hit
                                , serialization::StateUnpacking & state ) {

    hit.position = rHit.getPosition();
}

#endif

}
