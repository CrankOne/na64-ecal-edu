#include "na64event/fields/APVCluster.hh"
#include "na64event/serialize.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {

APVClustersIndex &
EvFieldTraits<APVCluster>::map(Event & e) {
    return e.apvClusters;
}

std::vector<APVClustersIndex*>
EvFieldTraits<APVCluster>::maps_for( Event & e
                                   , DetID
                                   , const nameutils::DetectorNaming & ) {
    std::vector<APVClustersIndex*> r;
    r.push_back( &e.apvClusters );
    return r;
}

void
EvFieldTraits<APVCluster>::remove_from_event( Event & e
                                            , APVClustersIndex::Iterator it
                                            , const nameutils::DetectorNaming & nm ) {
	#if 1
	std::pair< DetID_t, PoolRef<APVCluster> > pr = *it;
	// Remove from subsidiary maps
	for( auto subMapPtr : maps_for(e, DetID(it.offsetIt->first), nm) ) {
		// Locate all the clusters corresponding to detector
		auto er = subMapPtr->container().equal_range(pr.first);
		// From these clusters, find and remove entry for certain
		// cluster using by-value comparison (note that we do stop
		// on first match -- doubtful)
		// TODO: relies only on offsets leading to certain error in case
		// of mixing pools
		for( auto it = er.first; it != er.second; ++it ) {
			if( it->second != pr.second.offset_id() ) continue;
			subMapPtr->container().erase(it);
			break;
		}
	}
	#else  // TODO: XXX, wrong implem
    // Remember detector ID as `it' will not be valid after erasing it from
    // main map
    DetID did(it.offsetIt->first);
    // Erase entry from each subsidiary map
    for( auto subMapPtr : maps_for(e, did, nm) ) {
        subMapPtr->container().erase( did );
    }
    #endif
}

void
EvFieldTraits<APVCluster>::reset_hit( APVCluster & c ) {
    c.clear();
    c.position = std::nan("0");
    c.charge = 0.;
    c.time = 0;
    c.timeError = 0;
    c.wPosition = 0.;
    c.sparseness = 0.;
}

static double _apv_cluster_position_getter( const APVCluster & c ) { return c.position; }
static double _apv_cluster_wposition_getter( const APVCluster & c ) { return c.wPosition; }
static double _apv_cluster_charge_getter( const APVCluster & c ) { return c.charge; }
static double _apv_cluster_time_getter( const APVCluster & c ) { return c.time; }
static double _apv_cluster_time_error_getter( const APVCluster & c ) { return c.timeError; }
static double _apv_cluster_sparseness_getter( const APVCluster & c ) { return c.sparseness; }
static double _apv_cluster_width_getter( const APVCluster & c ) { return c.size(); }
static double _apv_cluster_goodness_getter( const APVCluster & c ) {
    return c.size()*c.charge/(c.sparseness + 1);
}
// ...

//
// Getters
/////////

static EvFieldTraits<APVCluster>::GetterEntry _getters[] = {
    { _apv_cluster_position_getter,  "position",   "center" },
    { _apv_cluster_wposition_getter, "wPosition",  "weighted center" },
    { _apv_cluster_charge_getter,    "charge",     "integrated charge coefficient" },
    { _apv_cluster_time_getter,   "time",    "time" },
    { _apv_cluster_time_error_getter,   "timeError",    "Error of time" },
    { _apv_cluster_sparseness_getter,"sparseness", "sparseness coefficient" },
    { _apv_cluster_width_getter,     "width",      "width (in number of wires units)" },
    { _apv_cluster_goodness_getter,  "goodness",
        "goodness coefficient =width*charge/(sparseness+1)"  },
    // ...
    {nullptr, nullptr, nullptr}
};

EvFieldTraits<APVCluster>::GetterEntry * EvFieldTraits<APVCluster>::getters = _getters;

//
// Serialization
///////////////

#if defined(CapnProto_FOUND) && CapnProto_FOUND
void
EvFieldTraits<APVCluster>::pack( const APVCluster & c
                               , PackedType::Builder bC
                               , serialization::StatePacking & state ) {
    // Assure that if there are hits within current cluster, there are hits map
    assert( state.apvHits.empty() || ! c.empty() );
    // Write APV cluster specific fields
    bC.setPosition( c.position );
    bC.setWPosition( c.wPosition );
    bC.setCharge( c.charge );
    bC.setSparseness( c.sparseness );
    bC.setTime ( c.time );
    bC.setTimeError ( c.timeError );
    // Pipulate hit references with hit numbers (relies on insertion
    // order written in storedHitsIdx)
    auto bHitRef = bC.initHitRefs( c.size() );
    size_t nHit = 0;
    for( auto cRefPair : c ) {
        // Find APV hit associated with cluster among APV hits that have been
        // serializaed already
        auto hitOffsetIt = state.apvHits.find( cRefPair.first );
        if( state.apvHits.end() == hitOffsetIt ) {
            NA64DP_RUNTIME_ERROR( "Invalid serialization state:"
                    " failed to address cluster's APV hit." );
        }
        bHitRef.set( nHit, hitOffsetIt->second );
        ++nHit;
    }
    // ...

    auto ir = state.apvClusters.emplace( state.banks.of<APVCluster>().get_ref_of( &c )
                                       , state.apvClusters.size() );
    assert( ir.second );
}

void
EvFieldTraits<APVCluster>::unpack( PackedType::Reader rC
                                 , APVCluster & c
                                 , serialization::StateUnpacking & state ) {
    c.position = rC.getPosition();
    c.wPosition = rC.getWPosition();
    c.charge = rC.getCharge();
    c.sparseness = rC.getSparseness();
    c.time = rC.getTime();
    c.timeError = rC.getTimeError();
    // Populate cluster with hit references (relies on hits previosuly being
    // de-serialized with hit unpacking and transmitted hire by the state)
    auto rHitRefs = rC.getHitRefs();
    for( size_t i = 0; i < rHitRefs.size(); ++i ) {
        size_t rHitRef = rHitRefs[i];
        auto it = state.apvHits.find(rHitRef);
        if( state.apvHits.end() == it ) {
            NA64DP_RUNTIME_ERROR( "Can not bind cluster with APV hit by"
                    " stored reference: unknown wire number in"
                    " de-serialization state." );
        }
        auto ir = c.emplace( it->second->channelNo
                           , it->second );
        if( ! ir.second ) {
            NA64DP_RUNTIME_ERROR( "Can not bind cluster with APV hit by"
                    " stored reference: repeatative wire number in"
                    " de-serialization state." );
        }
    }
    // ...

    auto ir = state.apvClusters.emplace( state.apvClusters.size()
                                       , state.banks.of<APVCluster>().get_ref_of( c ) );
    assert( ir.second );
}
#endif

}

