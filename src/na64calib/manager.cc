#include "na64calib/manager.hh"
#include "na64util/str-fmt.hh"

// create_calibration_handle() shall know about all the existing handle
// subclasses. TODO: refactor it for VCtr
//#include "na64calib/YAMLCalibHandle.hh"

#include <fstream>

namespace na64dp {
namespace calib {

void
iCalibRunIndex::UpdatesList::enqueue_update( Dispatcher::CIDataID key
                                           , const std::string & loaderName
                                           , EventID calibEventID ) {
    auto it = find(key);
    CIUpdateID updEntry{loaderName, calibEventID};
    if( end() == it ) {
        auto ir = emplace( key, std::vector<CIUpdateID>() );
        assert(ir.second);
        it = ir.first;
    }
    it->second.push_back( updEntry );
}


Manager::Manager() {
}

Manager::~Manager() {
}

bool
Manager::_collect_updates( iCalibRunIndex::UpdatesList & updates
                         , EventID newEventID ) {
    for( auto indexPtr : _ciIndexes ) {
        indexPtr->updates(_cEventID, newEventID, updates);
    }
    _log.debug( "Collected %d calibration info update types."
              , updates.size() );
    return !updates.empty();
}

void
Manager::_apply_updates( const iCalibRunIndex::UpdatesList & updates ) {
    // load updates if they are relevant (at least one observable exists)
    for( auto updateEntry : updates ) {
        Dispatcher::CIDataID dataTypeID = updateEntry.first;
        if( !observable_exists(dataTypeID) ) continue;
        // For relevant updates, iterate over specified loader+runID pair
        // using first one available
        bool updateLoaded = false;
        for( auto & updateSpec : updateEntry.second ) {
            const std::string & loaderName = updateSpec.loaderName;
            auto loaderIt = _ciLoaders.find(loaderName);
            if( _ciLoaders.end() == loaderIt ) {
                msg_debug( _log, "No loader \"%s\" to load"
                        " calibration data %s for event #%s."
                        , loaderName.c_str()
                        , util::calib_id_to_str(dataTypeID).c_str()
                        , updateSpec.eventID.to_str().c_str() );
                continue;
            }
            iCalibDataLoader & loader = *(loaderIt->second);
            if( !loader.has( updateSpec.eventID
                           , dataTypeID ) ) {
                msg_debug( _log, "Loader \"%s\" can not provide"
                        " calibration data %s for event #%s."
                        , loaderName.c_str()
                        , util::calib_id_to_str(dataTypeID).c_str()
                        , updateSpec.eventID.to_str().c_str() );
                continue;
            }
            loader.load_data( updateSpec.eventID
                            , dataTypeID
                            , *this );
            msg_debug( _log, "Loader \"%s\" used to load"
                     " calibration data %s for event #%s."
                     , loaderName.c_str()
                     , util::calib_id_to_str(dataTypeID).c_str()
                     , updateSpec.eventID.to_str().c_str() );
            updateLoaded = true;
            break;
        }
        if( ! updateLoaded ) {
            // Generate report on the failed calibration loading
            char errBf[1024];
            size_t bytesUsed = snprintf( errBf
                    , sizeof(errBf)
                    , "Calibration update failure details: info type %s"
                      " loaders tried: "
                    , util::calib_id_to_str(dataTypeID).c_str() );
            for( auto & updateSpec : updateEntry.second ) {
                if( bytesUsed >= sizeof(errBf) ) break;
                //const std::string & loaderName = updateSpec.loaderName;
                bytesUsed += snprintf( errBf
                        , sizeof(errBf) - bytesUsed
                        , "%s (%s), "
                        , updateSpec.loaderName.c_str()
                        , updateSpec.eventID.to_str().c_str() );
            }
            _log.error( errBf );
            NA64DP_RUNTIME_ERROR( "None of the loaders provided"
                    " update for calibration %s"
                    , util::calib_id_to_str(dataTypeID).c_str() );
        }
    }
}

void
Manager::event_id( EventID newEventID ) {
    // collect updates
    iCalibRunIndex::UpdatesList updates;
    if( _collect_updates(updates, newEventID) ) {
        _apply_updates(updates);
    }
    _cEventID = newEventID;
}

void
Manager::add_loader( const std::string & loaderName
                   , iCalibDataLoader * loader ) {
    auto ir = _ciLoaders.emplace( loaderName, loader );
    if( !ir.second ) {
        throw std::runtime_error( "Duplicating loader name." );
    }
}

void
Manager::add_run_index( iCalibRunIndex * runIndex ) {
    _ciIndexes.push_back( runIndex );
}

}  // namespace ::na64dp::calib
}  // namespace na64dp

