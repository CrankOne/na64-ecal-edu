#include "na64calib/dispatcher.hh"

namespace na64dp {

namespace errors {

NoCalibrationInfoEntry::NoCalibrationInfoEntry( const std::string & nm
                                              , const std::type_info & ti ) throw() 
    : std::runtime_error(
            util::format( "Calibration info %s not found."
                        , util::calib_id_to_str(calib::Dispatcher::CIDataID(ti, nm)).c_str()
                        ).c_str() )
    , _ti(ti)
    , _name(nm) {}

PrematureDereferencing::PrematureDereferencing( const std::string & nm
                                              , const std::type_info & ti ) throw() 
    : std::runtime_error(
            util::format( "Calibration info %s entry is not yet available."
                         , util::calib_id_to_str(calib::Dispatcher::CIDataID(ti, nm)).c_str()
                         ).c_str() )
        , _ti(ti)
        , _name(nm) {}

}
}

