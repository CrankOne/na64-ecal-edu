/**\brief Geode client application
 *
 * Uses CURL to perform client-side operations with the remote Geode API.
 *
 * TODO: purpose for this application is fuzzy for now. We are in the process
 * of testing the Geode and many architectural features are unclear. Geant4
 * XercesC is able to fetch data from remote by its own, and for the tracking
 * we have to finish the full TGeo conversion cycle first with all the Genfit
 * integration.
 * */

#include "na64app/app.hh"

namespace geode {

struct Config {
    /// Geode remote address to communicate with
    std::string remote;
};

}  // namespace geode

static void
usage_info( const char * appName
          , std::ostream & os
          , const ApplicationConfig & appCfg
          , const char * subcommand
          ) {
    if( !subcommand ) {
        os << "Geode command-line client application." << std::endl
           << "Usage:" << std::endl
           << "  $ " << appName
           << "[-h,--help]"
           << "[build|submit|receive] OPTIONS"
           << std::endl;
        os << "where:" << std::endl
           << "  -h,--help makes app to print this message and exit." << std::endl
           << "  [build|submit|receive] ..." << std::endl;
    }
    // TODO ...
}

int
main(int argc, char * argv[]) {
    CURL * curl;
    // ...
}

