#include "na64app/app.hh"
#include "na64dp/abstractHandler.hh"
#include "na64detID/TBName.hh"
#include "na64calib/SADC/pedestals.hh"
#include "na64calib/loader-generic.hh"
#include "na64calib/config-yaml.hh"
#include "na64dp/abstractEventSource.hh"
#include "na64util/runtimeDirs.hh"

#include <fcntl.h>
#include <dlfcn.h>

namespace na64dp {
namespace util {

void
list_registered( std::ostream & os
               , const char * type ) {
    if( (!type) || !strcmp( "handlers", type ) ) {
        os << "handlers (existing event data handler entities):" << std::endl;
        try {
            for( const auto & p : na64dp::VCtr::self().registry<na64dp::AbstractHandler>() ) {
                os << " * " << p.first << " -- " << p.second.second << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
        if( type ) return;
    }
    if( (!type) || !strcmp( "sources", type ) ) {
        os << "sources (existing event data source format codecs):" << std::endl;
        try {
            for( const auto & p : na64dp::VCtr::self().registry<na64dp::AbstractEventSource>() ) {
                os << " * " << p.first << " -- " << p.second.second << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
        if( type ) return;
    }
    if( (!type) || !strcmp( "getters-SADC", type ) ) {
        os << "getters-SADC (getters for for SADC hits):" << std::endl;
        try {
            for( auto c = na64dp::EvFieldTraits<na64dp::SADCHit>::getters
               ; c->getter
               ; ++c ) {
                os << " * " << c->name << " -- " << c->description << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
        if( type ) return;
    }
    if( (!type) || !strcmp( "getters-APV", type ) ) {
        os << "getters-APV (getters for APV hits):" << std::endl;
        try {
            for( auto c = na64dp::EvFieldTraits<na64dp::APVHit>::getters
               ; c->getter
               ; ++c ) {
                os << " * " << c->name << " -- " << c->description << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
        if( type ) return;
    }
    if( (!type) || !strcmp( "getters-clusters", type ) ) {
        os << "getters-clusters (getters for APV Clusters):" << std::endl;
        try {
            for( auto c = na64dp::EvFieldTraits<na64dp::APVCluster>::getters
               ; c->getter
               ; ++c ) {
                os << " * " << c->name << " -- " << c->description << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
        if( type ) return;
    }
    if( (!type) || !strcmp( "detector-id-definitions", type ) ) {
        os << "detector-id-definitions (Grammar entities for detector selection):" << std::endl;
        try {
            for( auto ge = na64dp::utils::gDetIDGetters
               ; ge->name
               ; ++ge ) {
                if( '+' == ge->name[0] ) continue;  // omit external resolvers
                os << " * " << ge->name + 1 << " -- " << ge->description << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
    }
}


void
load_modules( const std::set<std::string> & modules_
            , const char * paths ) {
    std::set<std::string> modules;
    std::ostringstream mlLog;
    if( ! paths ) paths = ".";

    na64dp::util::RuntimeDirs rd( paths );
    for( auto mdl_ : modules_ ) {
        struct stat mfStat;
        int mfStatRC = stat( mdl_.c_str(), &mfStat );
        if( -1 != mfStatRC && S_ISREG( mfStat.st_mode ) )  {
            modules.insert(mdl_);
            continue;
        }

        std::vector<std::string> modulePaths = rd.locate_files( "{lib,}" + mdl_ + "{.so,}" );
        if( modulePaths.empty() ) {
            std::cerr << "Error: unable"
                         " to find module \"" << mdl_ << "\"." << std::endl;
            continue;
        }
        if( modulePaths.size() > 1 ) {
            std::cerr << "Warning: few eponymous candidates found for module \""
                << mdl_ << "\" (first takes precedence): ";
            for( auto mfe : modulePaths ) {
                std::cerr << mfe << ", ";
            }
            std::cerr << std::endl;
        }

        modules.insert(*modulePaths.begin());
    }

    for( auto mf : modules ) {
        void * libPtr = dlopen(mf.c_str(), RTLD_NOW | RTLD_GLOBAL);
        if( !libPtr ) {
            std::cerr << "Failed to load shared object (module): \"" << mf << "\": "
                      << dlerror() << std::endl;
        } else {
            std::cout << "Module \"" << mf << "\" has been loaded." << std::endl;
        }
    }
}


void
setup_default_calibration_indexes(
        std::unordered_map<std::string, na64dp::calib::YAMLCalibInfoCtr> & ctrs,
        na64dp::calib::GenericLoader & genericLoader,
        na64dp::calib::Manager & mgr ) {
    {   // Naming calibrations
        using na64dp::nameutils::DetectorNaming;
        // - manually allocate the simple YAML data index instance
        //   parameterised with `DetectorNaming` type
        auto namesIndex = new na64dp::calib::SimpleYAMLDataIndex<DetectorNaming>(
                na64dp::calib::mappings_from_yaml_API02);
        // - create the "constructor entry" structure to be called by config
        //   parser
        auto namesKey = na64dp::calib::Dispatcher::info_id<DetectorNaming>("default");
        na64dp::calib::YAMLCalibInfoCtr nmCtr { namesKey
                                              , namesIndex
                                              , nullptr
                                              };
        // - emplace "constructor entry" to "constructor entries list"
        ctrs.emplace( "names", nmCtr );
        // - emplace the data index entry into generic constructor; do it
        //   manually as externally-created objects won't be automatically
        //   bound to generic loader
        genericLoader.add_data_index( namesKey, namesIndex );
    }

    {   // Pedestals CSV parsers
        using na64dp::calib::Pedestals;
        // - manually instantiate the pedestals data index
        auto pedestalsIndex = new na64dp::calib::PedestalsCSVDataIndex( mgr );
        // - create the "constructor entry" structure
        auto pedestalsKey = na64dp::calib::Dispatcher::info_id<Pedestals>("default");
        na64dp::calib::YAMLCalibInfoCtr pdsCtr { pedestalsKey
                                               , pedestalsIndex
                                               , nullptr
                                               };
        ctrs.emplace( "SADCPedestals", pdsCtr );
        // - manually bound created data index with generic loader
        genericLoader.add_data_index( pedestalsKey, pedestalsIndex );
    }
}

}
}

